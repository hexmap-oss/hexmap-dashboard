FROM node:15.14.0-buster-slim

WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install -qs

# add app
COPY . ./

# build app
RUN yarn build

CMD public
