/* 
 * src/components/sidebar/Sidebar.tsx
 */
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import { useTheme } from '@material-ui/core/styles';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import Box from '@material-ui/core/Box';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import BallotIcon from '@material-ui/icons/Ballot';
import BarChartIcon from '@material-ui/icons/BarChart';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import FaceIcon from '@material-ui/icons/Face';
import FlagIcon from '@material-ui/icons/Flag';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
// import PublicIcon from '@material-ui/icons/Public';
import SettingsIcon from '@material-ui/icons/Settings';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import SyncAltIcon from '@material-ui/icons/SyncAlt';

import { selectItems } from './sidebarSlice';

const icons: {[key: string]: JSX.Element} = {
    SupervisorAccount: <SupervisorAccountIcon />,
    Ballot: <BallotIcon />,
    Flag: <FlagIcon />,
    BarChart: <BarChartIcon />,
    SyncAlt: <SyncAltIcon />,
    Face: <FaceIcon />,
    // Public: <PublicIcon />,
    GpsFixed: <GpsFixedIcon />,
    CloudDownload: <CloudDownloadIcon />
};


interface SidebarProps {
    classes: ClassNameMap;
    handleClose: () => void;
    isOpen: boolean;
};

// Creates the sliding dashboard sidebar with state controlled by global app hook
const Sidebar = ({classes, handleClose, isOpen}: SidebarProps) => {
    const theme = useTheme();
    const items = useSelector(selectItems);

    return (
        <Drawer
            variant='permanent'
            open={isOpen}
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: isOpen,
                [classes.drawerClose]: !isOpen
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: isOpen,
                    [classes.drawerClose]: !isOpen,
                })
            }}
        >
            <div className={classes.toolbar}>
                <IconButton onClick={handleClose}>
                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
            </div>
            <Divider />
            <List>
                {items.map((item, index) => (
                <ListItem key={`menu-item-${index}`} component={Link} to={item.path}>
                    <ListItemIcon>{icons[item.iconName]}</ListItemIcon>
                    <ListItemText className={classes.sidebarItemText} primary={item.text} />
                </ListItem>
                ))}
            </List>
            
            <Box className={classes.drawerFooter}>
                <Divider />
                
                <List>
                    <ListItem component={Link} to={'/survey-settings'}>
                        <ListItemIcon><SettingsIcon /></ListItemIcon>
                        <ListItemText className={classes.sidebarItemText} primary={'Settings'} />
                    </ListItem>
                </List>
            </Box>
        </Drawer>
    );
};


export default Sidebar;
