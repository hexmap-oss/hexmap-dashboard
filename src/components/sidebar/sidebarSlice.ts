/* 
 * src/components/sidebar/sidebarSlice.ts
 */
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

import SidebarItem from './sidebarTypes';


interface SidebarState {
    items: Array<SidebarItem>
};

const initialState: SidebarState = {
    items: [
        {
            iconName: 'SupervisorAccount',
            text: 'Dashboard Users',
            textId: 'app.sidebar.items.organizationUserPermissions',
            path: '/dashboard-users',
            userLevel: 0
        },
        {
            iconName: 'Ballot',
            text: 'Survey Wizard',
            textId: 'app.sidebar.pages.surveyWizard',
            path: '/survey-wizard',
            userLevel: 0
        },
        {
            iconName: 'Flag',
            text: 'Prompts',
            textId: 'app.sidebar.pages.prompts',
            path: '/prompts-wizard',
            userLevel: 0
        },
        {
            iconName: 'BarChart',
            text: 'Metrics',
            textId: 'app.sidebar.pages.metrics',
            path: '/metrics',
            userLevel: 1
        },
        // {
        //     iconName: 'Public',
        //     text: 'Survey Map',
        //     textId: 'app.sidebar.pages.surveyMap',
        //     path: '/survey-map',
        //     userLevel: 1
        // },
        {
            iconName: 'Face',
            text: 'Participants',
            textId: 'app.sidebar.pages.participants',
            path: '/participant-users',
            userLevel: 1
        },
        {
            iconName: 'GpsFixed',
            text: 'GPS Points',
            textId: 'app.sidebar.pages.gpsPoints',
            path: '/participant-map',
            userLevel: 2
        },
        {
            iconName: 'CloudDownload',
            text: 'Data Management',
            textId: 'app.sidebar.pages.dataManagement',
            path: '/data-management',
            userLevel: 1
        },
        {
            iconName: 'SyncAlt',
            text: 'OD Map',
            textId: 'app.sidebar.pages.odmap',
            path: '/od-map',
            userLevel: 1
        },        
    ]
};


export const sidebarSlice = createSlice({
    name: 'sidebar',
    initialState,
    reducers: {
        
    },
});

export const selectItems = (state: RootState) => state.sidebar.items;

export default sidebarSlice.reducer;

