/* 
 * src/components/sidebar/sidebarTypes.ts
 */

export default interface SidebarItem {
    iconName: string,
    text: string,
    textId: string,
    path: string,
    userLevel: number,
};
