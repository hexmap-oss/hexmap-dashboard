/* 
 * src/components/appBar/customAppBar.tsx
 */
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';

import { DRAWER_WIDTH } from '../../themes/base';
import { clearCachedUser } from '../../app/browserStorage';
import { logoutUser, selectUserAccessToken } from '../../views/login/loginSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '90%',
            margin: 'auto',
            paddingTop: theme.spacing(4),
        },
        appBar: {
            backgroundColor: theme.palette.primary.dark,
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen
            }),
            zIndex: theme.zIndex.drawer + 1
        },
        appBarShift: {
            marginLeft: DRAWER_WIDTH,
            width: `calc(100% - ${DRAWER_WIDTH}px)`,
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen
            })
        },
        appBarTitle: {
            flexGrow: 1,
            color: theme.palette.common.white,
            textDecoration: 'none'
        },
        menuButton: {
            marginRight: 36,
        },
        hide: {
            display: 'none'
        },        
    })
);


interface CustomAppBarProps {
    drawerIsOpen: boolean;
    onDrawerOpen: () => void;
};

const CustomAppBar = ({drawerIsOpen, onDrawerOpen}: CustomAppBarProps) => {
    const classes = useStyles();
    const [redirectToLogin, setRedirectToLogin] = useState(false);
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const accessToken = useSelector(selectUserAccessToken);
    // const isAuthed = useSelector(selectIsSuccess);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!accessToken) {
            setRedirectToLogin(true);
        }
    }, [accessToken]);


    const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleProfileMenuClose = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(null);
    };

    const handleLogOut = () => {
        clearCachedUser();
        dispatch(logoutUser());
        setRedirectToLogin(true);
    };

    if (redirectToLogin) {
        return (
            <Redirect to='/login' />
        );
    }
    return (
        <AppBar
            position='fixed'
            className={clsx(classes.appBar, {
                [classes.appBarShift]: drawerIsOpen,
            })}
        >
            <Toolbar>
                <IconButton
                    color='inherit'
                    aria-label='open drawer'
                    onClick={onDrawerOpen}
                    edge='start'
                    className={clsx(classes.menuButton, {
                        [classes.hide]: drawerIsOpen,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <Typography
                    variant='h6'
                    className={classes.appBarTitle}
                    component={Link}
                    to={'/'}
                    noWrap
                >
                    HexMap Dashboard
                </Typography>					

                {/* Profile icon menu */}
                <Box>
                    <IconButton
                        aria-label='account of current user'
                        aria-controls='menu-appbar'
                        aria-haspopup='true'
                        onClick={handleProfileMenuOpen}
                        color='inherit'
                    >
                        <AccountCircle />
                    </IconButton>
                    <Menu
                        id='menu-appbar'
                        getContentAnchorEl={null}
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right'
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right'
                        }}
                        open={open}
                        onClose={handleProfileMenuClose}
                        keepMounted
                    >
                        <MenuItem component={Link} to={'/organization-settings'}>
                            Organization
                        </MenuItem>
                        <MenuItem component={Link} to={'/user-settings'}>
                            My Account
                        </MenuItem>
                        <Divider />
                        <MenuItem onClick={handleLogOut}>
                            <b>Log Out</b>
                        </MenuItem>                        
                    </Menu>
                </Box>
            </Toolbar>
        </AppBar>        
    );
}

export default CustomAppBar;