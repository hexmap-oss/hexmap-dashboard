/* 
 * src/components/questionBuilder/cards/CheckboxEdit.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import { CheckboxOption, TextFieldChangedEvent } from '../questionBuilderTypes';
import OptionsBullets from './shared/OptionsBullets';
import AddRemoveChoicesButtons from './shared/AddRemoveChoicesButtons';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        options: {
            paddingLeft: 16
        }
    })
);


interface CheckboxEditFormProps {
    options: CheckboxOption[];
    setOptions: (options: CheckboxOption[]) => void;
};

const CheckboxEditForm = (props: CheckboxEditFormProps) => {
    const classes = useStyles();

    const handleAddOption = () => {
        props.setOptions([...props.options, '']);
    };

    const handleDeleteOption = () => {
        props.setOptions(props.options.slice(0, -1));
    };

    const handleEditOption = (event: TextFieldChangedEvent, optionIndex: number) => {
        const newOptions = [...props.options];
        newOptions[optionIndex] = event.target.value
        props.setOptions(newOptions);
    };

    return (
        <>
        <Box display='flex' justifyContent='center'>
            <AddRemoveChoicesButtons
                onAddOption={handleAddOption}
                onDeleteOption={handleDeleteOption}
            />
        </Box>

        <Box className={classes.options}>
            <OptionsBullets
                options={props.options}
                onEditOption={handleEditOption}
            />
        </Box>
        </>
    );
};

export default CheckboxEditForm;
