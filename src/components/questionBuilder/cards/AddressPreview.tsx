/* 
 * src/components/questionBuilder/cards/AddressPreview.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import RoomIcon from '@material-ui/icons/Room';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: 16
        },
        icon: {
            fontSize: 52,
            marginBottom: 20,
        },
        caption: {
            fontSize: 10,
            textTransform: 'uppercase',
            color: theme.palette.grey[700]
        }
    })
);


interface AddressPreviewFormProps {
    title: string;
};

const AddressPreviewForm = (props: AddressPreviewFormProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <RoomIcon className={classes.icon} />
            <Typography className={classes.caption} variant='caption'>
                User indicates location with map marker in mobile app
            </Typography>
        </Box>
    );
};

export default AddressPreviewForm;
