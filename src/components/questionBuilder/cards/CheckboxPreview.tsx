/* 
 * src/components/questionBuilder/cards/CheckboxPreview.tsx
 */
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

import { CheckboxClickedEvent, CheckboxOption } from '../questionBuilderTypes';
import { makeDashedId } from '../../../app/utils';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center'
        },
        formControl: {
            margin: theme.spacing(1),
        },
        input: {
            position: 'relative',
        },
    })
);

interface CheckboxPreviewFormProps {
    title: string;
    options: CheckboxOption[];
    setOptions: (options: CheckboxOption[]) => void;
};

const CheckboxEditForm = (props: CheckboxPreviewFormProps) => {
    const classes = useStyles();
    const formId = makeDashedId(props.title);
    const [selected, setSelected] = useState<number[]>([]);

    const handleChange = (event: CheckboxClickedEvent, optionIndex: number) => {
        const newSelected = [...selected];
        const index = selected.indexOf(optionIndex);
        if (index > -1) {
            newSelected.splice(index, 1);  // unselect existing
        } else {
            newSelected.push(optionIndex); // select checkbox
        }
        setSelected(newSelected);
    };

    return (
        <Box className={classes.root}>
            <FormControl component='fieldset' className={classes.formControl}>
                <FormGroup>
                {props.options.map((option, index) => {
                    let name = makeDashedId(option),
                        isChecked = selected.indexOf(index) > -1;
                    return (
                        <FormControlLabel
                            className={classes.input}
                            key={`${formId}-${index}`}
                            control={<Checkbox checked={isChecked} size={'small'} onChange={(event) => handleChange(event, index)} name={name} />}
                            label={option}
                        />
                    );
                })}
                </FormGroup>
        </FormControl>
      </Box>
    );
};

export default CheckboxEditForm;
