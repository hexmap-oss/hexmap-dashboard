/* 
 * src/components/questionBuilder/cards/Dropdown.tsx
 */
import { useState } from 'react';

import DropdownEditForm from './DropdownEdit';
import DropdownPreviewForm from './DropdownPreview';
import Template from './Template';
import { DropdownOption, QuestionTypes, StackQuestionProps } from '../questionBuilderTypes';


const DropdownQuestion = (props: StackQuestionProps) => {
    const [selected, setSelected] = useState('');
    const [options, setOptions] = useState<DropdownOption[]>(props.options ? props.options : []);

    const PreviewForm = (
        <DropdownPreviewForm
            title={props.title}
            options={options}
            selected={selected}
            setSelected={setSelected}
        />
    );

    const EditForm = (
        <DropdownEditForm
            options={options}
            setOptions={setOptions}
        />
    );

    return (
        <Template
            id={props.id}
            type={QuestionTypes.dropdown}
            title={props.title}
            fieldName={props.fieldName}
            options={options}
            deleteQuestion={props.deleteQuestion}
            saveQuestion={props.saveQuestion}
            EditForm={EditForm}
            PreviewForm={PreviewForm}
            hardcodedId={props.hardcodedId}
            disabled={props.disabled}
        />
    );
}

export default DropdownQuestion;
