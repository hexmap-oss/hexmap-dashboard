/* 
 * src/components/questionBuilder/cards/TextBoxPreview.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',
            padding: 12,
        },
        textField: {
            width: 400,
            maxWidth: '60%'
        }
    })
);

interface TextBoxPreviewFormProps {
    title: string;
};

const TextBoxPreviewForm = (props: TextBoxPreviewFormProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <TextField
                className={classes.textField}
                label='Text Response'
                multiline
                rows={4}
                defaultValue={''}
                variant='outlined'
                InputLabelProps={{
                    shrink: true,
                }}                
            />            
        </Box>
    );
};

export default TextBoxPreviewForm;
