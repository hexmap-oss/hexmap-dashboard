/* 
 * src/components/questionBuilder/cards/AddressEdit.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Image from 'material-ui-image';

import MarkerImage from './addressMarker.png';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center'
        }
    })
);


interface AddressEditFormProps {};

const AddressEditForm = (props: AddressEditFormProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Image
                src={MarkerImage}
                animationDuration={250}
                style={{
                    paddingTop: 'none',
                    height: '250px',
                    width: '250px',
                }}
            />
        </Box>
    );
};

export default AddressEditForm;
