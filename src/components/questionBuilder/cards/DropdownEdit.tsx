/* 
 * src/components/questionBuilder/cards/DropdownEdit.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import AddRemoveChoicesButtons from './shared/AddRemoveChoicesButtons';
import OptionsBullets from './shared/OptionsBullets';
import { DropdownOption, TextFieldChangedEvent } from '../questionBuilderTypes';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        buttonGroup: {
            display: 'flex',
            justifyContent: 'center'
        },
        options: {
            paddingLeft: 16
        }
    })
);


interface DropdownEditFormProps {
    options: DropdownOption[];
    setOptions: (options: DropdownOption[]) => void;
}

const DropdownEditForm = (props: DropdownEditFormProps) => {
    const classes = useStyles();

    const handleAddOption = () => {
        props.setOptions([...props.options, '']);
    };

    const handleDeleteOption = () => {
        props.setOptions(props.options.slice(0, -   1));
    };

    const handleEditOption = (event: TextFieldChangedEvent, optionIndex: number) => {
        const newOptions = [...props.options];
        newOptions[optionIndex] = event.target.value as string;
        props.setOptions(newOptions);
    };

    return (
        <>
        <Box className={classes.buttonGroup}>
            <AddRemoveChoicesButtons
                onAddOption={handleAddOption}
                onDeleteOption={handleDeleteOption}
            />
        </Box>

        <Box className={classes.options}>
            <OptionsBullets
                options={props.options}
                onEditOption={handleEditOption}
            />
        </Box>
        </>
    );
}

export default DropdownEditForm;
