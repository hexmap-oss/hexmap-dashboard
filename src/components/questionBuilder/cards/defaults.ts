/* 
 * src/components/questionBuilder/defaults.tsx
 */
import { QuestionTypes, StackQuestion } from '../questionBuilderTypes';

const dropdownQuestion = {
    id: -1,
    type: QuestionTypes.dropdown,
    title: 'Dropdown Selection',
    fieldName: '',
    options: ['Choice 1', 'Choice2']
} as StackQuestion;

const checkboxQuestion = {
    id: -1,
    type: QuestionTypes.checkbox,
    title: 'Checkbox Selection',
    fieldName: '',
    options: ['Choice 1', 'Choice2']
} as StackQuestion;

const numberQuestion = {
    id: -1,
    type: QuestionTypes.number,
    title: 'Number Selection',
    fieldName: '',
} as StackQuestion;

const addressQuestion = {
    id: -1,
    type: QuestionTypes.address,
    title: 'Address Selection',
    fieldName: '',
} as StackQuestion;

const textBoxQuestion = {
    id: -1,
    type: QuestionTypes.textBox,
    title: 'Text Box Selection',
    fieldName: '',
} as StackQuestion;

const defaults: {[key: string]: StackQuestion} = {
    dropdown: dropdownQuestion,
    checkbox: checkboxQuestion,
    number: numberQuestion,
    address: addressQuestion,
    textBox: textBoxQuestion,
};

export default defaults;
