/* 
 * src/components/questionBuilder/cards/Checkboxes.tsx
 */
import { useState } from 'react';

import CheckboxEditForm from './CheckboxEdit';
import CheckboxPreviewForm from './CheckboxPreview';
import Template from './Template';
import { CheckboxOption, QuestionTypes, StackQuestionProps } from '../questionBuilderTypes';


const CheckboxQuestion = (props: StackQuestionProps) => {
    const [options, setOptions] = useState<CheckboxOption[]>(props.options ? props.options : []);

    const PreviewForm = (
        <CheckboxPreviewForm 
            title={props.title}
            options={options}
            setOptions={setOptions}      
        />
    );

    const EditForm = (
        <CheckboxEditForm
            options={options}
            setOptions={setOptions}        
        />
    );

    return (
        <Template
            id={props.id}
            type={QuestionTypes.checkbox}
            title={props.title}
            fieldName={props.fieldName}
            options={options}
            deleteQuestion={props.deleteQuestion}
            saveQuestion={props.saveQuestion}
            EditForm={EditForm}
            PreviewForm={PreviewForm}
            hardcodedId={props.hardcodedId}
            disabled={props.disabled}
        />
    );
}

export default CheckboxQuestion;