/* 
 * src/components/questionBuilder/cards/shared/OptionsBullets.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField'

import { DropdownOption, CheckboxOption, TextFieldChangedEvent } from '../../questionBuilderTypes';

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        optionBullet: {
            color: theme.palette.primary.main,
            fontSize: 20,
            marginRight: 10,
        }
    })
);


interface OptionsBulletsProps {
    options: DropdownOption[] | CheckboxOption[];
    onEditOption: (event: TextFieldChangedEvent, index: number) => void;
};

const OptionsBullets = ({options, onEditOption}: OptionsBulletsProps) => {
    const classes = useStyles();

    return (
        <div>
            {options.map((option, index) => (
                <Box key={'editable-option-' + index}>
                    <label className={classes.optionBullet}>•</label>
                    <TextField
                        id={'text-field-' + index}
                        type={'text'}
                        value={option}
                        onChange={
                            (event: TextFieldChangedEvent) => onEditOption(event, index)}
                    />
                </Box>
            ))}
        </div>
    );
}

export default OptionsBullets;
