/* 
 * src/components/questionBuilder/cards/shared/AddRemoveChoicesButtons.tsx
 */
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        button: {
            minWidth: 44,
            marginRight: 8,
        },
        add: {
            color: theme.palette.common.white,
            backgroundColor: theme.palette.success.light,
            '&:hover': {
                backgroundColor: theme.palette.success.main,
            }
        },
        delete: {
            color: theme.palette.common.white,
            backgroundColor: theme.palette.error.light,
            '&:hover': {
                backgroundColor: theme.palette.error.main,
            }            
        }        
    })
);

interface AddRemoveChoicesButtonsProps {
    onAddOption: () => void;
    onDeleteOption: () => void;
};


const AddRemoveChoicesButtons = ({onAddOption, onDeleteOption}: AddRemoveChoicesButtonsProps) => {
    const classes = useStyles();

    return (
        <>
            <Button 
                className={clsx(classes.button, classes.add)}
                onClick={onAddOption}
                variant='contained'
                // color='secondary'
                size='small'
            >
                <AddIcon />
            </Button>
            <Button
                className={clsx(classes.button, classes.delete)}
                onClick={onDeleteOption}
                variant='contained'
                // color='secondary'
                size='small'
            >
                <RemoveIcon />
            </Button>
        </>  
    );
}

export default AddRemoveChoicesButtons;
