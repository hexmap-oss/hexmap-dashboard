/* 
 * src/components/questionBuilder/cards/shared/MissingFieldNameAlertDialog.tsx
 */
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        acceptButton: {
            color: theme.palette.info.main,
        },
    })
);

interface MissingFieldAlertDialogProps {
    isOpen: boolean;
    setOpen: (isOpen: boolean) => void;
};

const MissingFieldAlertDialog = ({isOpen, setOpen}: MissingFieldAlertDialogProps) => {
    const classes = useStyles();

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Dialog
                open={isOpen}
                onClose={handleClose}
                aria-labelledby='alert-dialog-title'
                aria-describedby='alert-dialog-description'
            >
                <DialogTitle id='alert-dialog-title'>
                    {'CSV Field Name required'}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id='alert-dialog-description'>
                        {`The CSV Field Name provides a column name for the exported data.
                          All typed letters are converted to lowercase and spaces are replaced with dashes.`}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button className={classes.acceptButton} onClick={handleClose} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default MissingFieldAlertDialog;
