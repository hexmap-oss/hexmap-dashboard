/* 
 * src/components/questionBuilder/cards/Address.tsx
 */
import { QuestionTypes, StackQuestionProps } from '../questionBuilderTypes';
import AddressEditForm from './AddressEdit';
import AddressPreviewForm from './AddressPreview';
import Template from './Template';



const AddressQuestion = (props: StackQuestionProps) => {
    const PreviewForm = (
        <AddressPreviewForm title={props.title} />
    );

    const EditForm = (
        <AddressEditForm />
    );

    return (
        <Template
            id={props.id}
            type={QuestionTypes.address}
            title={props.title}
            fieldName={props.fieldName}
            deleteQuestion={props.deleteQuestion}
            saveQuestion={props.saveQuestion}
            EditForm={EditForm}
            PreviewForm={PreviewForm}
            hardcodedId={props.hardcodedId}
            disabled={props.disabled}
        />
    );
}

export default AddressQuestion;
