/* 
 * src/components/questionBuilder/cards/Number.tsx
 */
import { QuestionTypes, StackQuestionProps } from '../questionBuilderTypes';
import NumberEditForm from './NumberEdit';
import NumberPreviewForm from './NumberPreview';
import Template from './Template';


const NumberQuestion = (props: StackQuestionProps) => {
    const PreviewForm = (
        <NumberPreviewForm title={props.title} />
    );

    const EditForm = (
        <NumberEditForm />
    );

    return (
        <Template
            id={props.id}
            type={QuestionTypes.number}
            title={props.title}
            fieldName={props.fieldName}
            deleteQuestion={props.deleteQuestion}
            saveQuestion={props.saveQuestion}            
            EditForm={EditForm}
            PreviewForm={PreviewForm}
            hardcodedId={props.hardcodedId}
            disabled={props.disabled}
        />
    );
}

export default NumberQuestion;
