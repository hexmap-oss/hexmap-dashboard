
/* 
 * src/components/questionBuilder/cards/DropdownPreview.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import { DropdownOption } from '../questionBuilderTypes';
import { makeDashedId } from '../../../app/utils';


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: '100%',
        },
        input: {
            margin: 'auto',
            maxWidth: 300,
            position: 'relative',
            width: '50%',
        },
        inputControl: {
            width: '100%',
            fontSize: 14,
            paddingTop: theme.spacing(0.5),

            '& .MuiSelect-select': {
                padding: theme.spacing(1)
            }
        }
    })
);


interface DropdownPreviewFormProps {
    selected: string;
    setSelected: (value: string) => void;
    title: string;
    options: DropdownOption[];
}

const DropdownPreviewForm = (props: DropdownPreviewFormProps) => {
    const classes = useStyles();
    const formId = makeDashedId(props.title);

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        props.setSelected(event.target.value as string);
    };

    return (
        <FormControl className={classes.formControl}>
            <Box className={classes.input}>
                <Select
                    className={classes.inputControl}
                    id={formId}
                    value={props.selected}
                    onChange={handleChange}
                >
                {props.options.map((option, index) => (
                    <MenuItem key={`menu-item-${index}`} value={option}>{option}</MenuItem>
                ))}
                </Select>
            </Box>
        </FormControl>
    );
}

export default DropdownPreviewForm;
