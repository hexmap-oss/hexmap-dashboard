/* 
 * src/components/questionBuilder/cards/Dropdown.tsx
 */
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import MissingFieldNameAlert from './shared/MissingFieldNameAlertDialog';
import { 
    ButtonClickedEvent,
    StackQuestionProps,
    TextFieldChangedEvent
} from '../questionBuilderTypes';
import EditableTitle from '../editableTitle';
import { makeDashedId } from '../../../app/utils';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginTop: 5,
            marginBottom: 14,
        },
        rootHardcodedOutline: {
            borderColor: theme.palette.warning.main,
            borderWidth: 1,
            borderStyle: 'solid',
        },
        cardContent: {
            paddingBottom: 8,
        },
        title: {
            textAlign: 'center',
        },
        fieldNameInput: {
            fontSize: 14,
        },        
        controlBar: {
            paddingTop: 20,
            position: 'relative',
        },
        buttonGroup: {
            position: 'absolute',
            bottom: 0,
            right: 0
        },
        editButton: {
            float: 'right'
        },
        deleteButton: {
            float: 'right',
            color: theme.palette.error.main,
        }
    }),
);


interface TemplateProps extends StackQuestionProps {
    EditForm: any;
    PreviewForm: any;
    disabled?: boolean;
}


const Template = (props: TemplateProps) => {
    const classes = useStyles();
    const [isEditing, setIsEditing] = useState(false);
    const [title, setTitle] = useState(props.title);
    const [fieldName, setFieldName] = useState(props.fieldName);
    const [isFieldNameAlertOpen, setFieldNameAlertOpen] = useState(false);

    useEffect(() => {
        setTitle(props.title);
    }, [props.title]);

    const _createQuestion = () => {
        const question = {
            id: props.id,
            type: props.type,
            title,
            fieldName,
        } as StackQuestionProps;
        if (props.options) {
            question.options = props.options;
        }
        return question;
    }

    const _questionIsValid = (question: StackQuestionProps) => {
        if (question.fieldName === '') {
            setFieldNameAlertOpen(true);
            return false;
        }
        return true;
    };

    const handleEditClick = (event: ButtonClickedEvent) => {
        setIsEditing(!isEditing);
    };

    const handleSetFieldName = (event: TextFieldChangedEvent) => {
        const fieldName = makeDashedId(event.target.value);
        setFieldName(fieldName);
    }

    const handleSaveClick = (event: ButtonClickedEvent) => {
        const question = _createQuestion();
        if (_questionIsValid(question)) {
            props.saveQuestion(question);
            setIsEditing(!isEditing);
        }
    };

    const handleDeleteClick = (event: ButtonClickedEvent) => {
        const question = _createQuestion();
        props.deleteQuestion(question);
    }

    const isHardcoded = props.hardcodedId && props.hardcodedId >= 100;
    return (
        <Card className={clsx(classes.root, isHardcoded ? classes.rootHardcodedOutline : undefined )} elevation={2}>
            <CardContent className={classes.cardContent}>
                <Grid container>
                    <Grid item xs={12}>
                        <Box className={classes.title}>
                            <EditableTitle
                                isEditing={isEditing}
                                text={props.title}
                                setText={setTitle}
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={12}>
                    {isEditing === false
                        ? props.PreviewForm
                        : props.EditForm
                    }
                    </Grid>

                    <Grid item xs={12}>
                        { !props.disabled && !isHardcoded ? (
                        <Box className={classes.controlBar}>
                            {isEditing === true ? (
                            <Box>
                                <form className={classes.root} noValidate>
                                    <TextField
                                        value={fieldName}
                                        onChange={handleSetFieldName}
                                        label='CSV field name'
                                        InputProps={{
                                            classes: {
                                                input: classes.fieldNameInput
                                            }
                                        }}
                                        required
                                    />
                                </form>
                            </Box>
                            ) : undefined}

                            {isEditing === false ? (
                            <Box className={classes.buttonGroup}>
                                <Button
                                    className={classes.editButton}
                                    onClick={handleEditClick}
                                >
                                    Edit
                                </Button>
                            </Box>
                            ) : (
                            <Box className={classes.buttonGroup}>
                                <Button
                                    className={classes.editButton}
                                    onClick={handleSaveClick}
                                >
                                    Save
                                </Button>

                                <Button
                                    className={classes.deleteButton}
                                    onClick={handleDeleteClick}
                                >
                                    Delete
                                </Button>               
                            </Box>
                            )}
                        </Box>
                        )
                        : undefined}
                    </Grid>
                </Grid>
            </CardContent>

            <MissingFieldNameAlert
                isOpen={isFieldNameAlertOpen}
                setOpen={setFieldNameAlertOpen}
            />
        </Card>
    );
}

export default Template;