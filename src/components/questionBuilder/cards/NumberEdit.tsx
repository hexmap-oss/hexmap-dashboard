/* 
 * src/components/questionBuilder/cards/NumberEdit.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',
            padding: 8,
        },
    })
);

interface NumberEditFormProps {};

const NumberEditForm = (props: NumberEditFormProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <TextField
                label='Number input'
                type='number'
                InputLabelProps={{
                    shrink: true,
                }}
            >
            </TextField>
        </Box>
    );
};

export default NumberEditForm;
