/* 
 * src/components/questionBuilder/cards/TextBox.tsx
 */
import TextBoxEditForm from './TextBoxEdit';
import TextBoxPreviewForm from './TextBoxPreview';
import Template from './Template';
import { QuestionTypes, StackQuestionProps } from '../questionBuilderTypes';


const TextBoxQuestion = (props: StackQuestionProps) => {
    const PreviewForm = (
        <TextBoxPreviewForm title={props.title} />
    );

    const EditForm = (
        <TextBoxEditForm />
    );

    return (
        <Template
            id={props.id}
            type={QuestionTypes.textBox}
            title={props.title}
            fieldName={props.fieldName}
            deleteQuestion={props.deleteQuestion}
            saveQuestion={props.saveQuestion}
            EditForm={EditForm}
            PreviewForm={PreviewForm}
            hardcodedId={props.hardcodedId}
            disabled={props.disabled}
        />
    );
}

export default TextBoxQuestion;
