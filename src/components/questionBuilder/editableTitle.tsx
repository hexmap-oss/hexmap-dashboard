/* 
 * src/components/questionBuilder/editableTitle.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        editing: {
            width: '100%',
        },
        textFieldInput: {
            fontSize: 20,
            fontWeight: 500,
            lineHeight: 1.6,
        }
    }),
);

interface EditableTitleProps {
    isEditing: boolean;
    text: string;
    setText: (text: string) => void;
}

const EditableTitle = (props: EditableTitleProps) => {
    const classes = useStyles();

    const handleOnChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        props.setText(event.target.value as string);
    };

    return (
        <div>
            {props.isEditing ? (
                <Box display='flex' p={1}>
                    <Box p={1} flexGrow={1}>
                        <TextField
                            className={classes.editing}
                            id='editable-title-edit'
                            defaultValue={props.text}
                            onChange={handleOnChange}
                            InputProps={{
                                classes: {
                                    input: classes.textFieldInput
                                }
                            }}
                        />
                    </Box>
                    <Box p={1}>
                        <EditIcon />
                    </Box>
                </Box>
            ) : (
                <Typography variant='h6'>{props.text}</Typography>
            )}
        </div>
    );
}

export default EditableTitle;
