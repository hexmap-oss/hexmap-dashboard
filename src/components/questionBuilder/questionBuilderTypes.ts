/* 
 * src/components/questionBuilder/questionBuilderTypes.ts
 */
import AddressQuestion from './cards/Address';
import CheckboxQuestion from './cards/Checkbox';
import DropdownQuestion from './cards/Dropdown';
import NumberQuestion from './cards/Number';
import TextBoxQuestion from './cards/TextBox';

export type TextFieldChangedEvent = React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>;
export type ButtonClickedEvent = React.MouseEvent<HTMLButtonElement, MouseEvent>;
export type CheckboxClickedEvent = React.ChangeEvent<HTMLInputElement>;

export enum QuestionTypes {
    dropdown = 'DROPDOWN',
    checkbox = 'CHECKBOX',
    number   = 'NUMBER',
    address  = 'ADDRESS',
    textBox  = 'TEXTBOX',
};

export const QuestionTypeMap: {[key in QuestionTypes]: React.FC<StackQuestionProps>} = {
    [QuestionTypes.dropdown]: DropdownQuestion,
    [QuestionTypes.checkbox]: CheckboxQuestion,
    [QuestionTypes.number]: NumberQuestion,
    [QuestionTypes.address]: AddressQuestion,
    [QuestionTypes.textBox]: TextBoxQuestion,
};

// TODO: build field types into API instead of mysterious integer codes
export const JSONQuestionTypeLookup: { [key: number]: QuestionTypes } =  {
    1: QuestionTypes.dropdown,
    2: QuestionTypes.checkbox,
    3: QuestionTypes.number,
    4: QuestionTypes.address,
    5: QuestionTypes.textBox,
    // TODO: remove hardcoded questions from survey builder
    100: QuestionTypes.dropdown,  // gender
    101: QuestionTypes.dropdown,  // age
    103: QuestionTypes.textBox,   // email,
    104: QuestionTypes.dropdown,  // member_type (occupation)
    105: QuestionTypes.address,   // location_home
    106: QuestionTypes.address,   // location_study
    107: QuestionTypes.address,   // location_work
    108: QuestionTypes.checkbox,  // travel_mode_study
    109: QuestionTypes.checkbox,  // travel_model_alt_study
    110: QuestionTypes.checkbox,  // travel_mode_work
    111: QuestionTypes.checkbox,  // travel_mode_alt_work
};

// survey builder types
export type DropdownOption = string;
export type CheckboxOption = string;

export type StackQuestion = {
    id: number
    type: keyof typeof QuestionTypeMap
    title: string
    fieldName: string
    options?: DropdownOption[] | CheckboxOption[]
    hardcodedId?: number
    disabled?: boolean
};

export interface StackQuestionProps extends StackQuestion {
    deleteQuestion: (question: StackQuestion) => void;
    saveQuestion: (question: StackQuestion) => void;    
}

export type MoveStackQuestion = {
    fromIndex: number
    toIndex: number
};

export type DragObject = {
    type: string
    question: string
};

export const FormTypes = {
    CARD: 'card',
    BUTTON: 'button'
};
