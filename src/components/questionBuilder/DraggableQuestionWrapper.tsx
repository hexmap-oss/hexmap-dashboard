/* 
 * src/components/questionBuilder/DraggableQuestionWrapper.tsx
 *
 * based on https://react-dnd.github.io/react-dnd/examples/sortable/cancel-on-drop-outside
 */
import { useRef } from 'react';
import { useDrag, useDrop, DropTargetMonitor } from 'react-dnd';
import { XYCoord } from 'dnd-core';


const ItemTypes = {
    CARD: 'card',
}

const style = {
    cursor: 'move',
}

interface DragItem {
    index: number
    id: string
    type: string
  }

interface DraggableQuestionWrapperProps {
    children?: React.ReactNode;
    id: string;
    index: number;
    moveCard: (dragIndex: number, hoverIndex: number) => void;
    disabled: boolean;
}

const DraggableQuestionWrapper = ({ id, children, index, moveCard, disabled }: DraggableQuestionWrapperProps) => {
    const ref = useRef<HTMLDivElement>(null);
    const [{ handlerId }, drop] = useDrop({
        accept: ItemTypes.CARD,
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            }
        },
        hover(item: DragItem, monitor: DropTargetMonitor) {
            if (!ref.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index

            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return
            }

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect()

            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

            // Determine mouse position
            const clientOffset = monitor.getClientOffset()

            // Get pixels to the top
            const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }

            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }

            // Time to actually perform the action
            moveCard(dragIndex, hoverIndex)

            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        },
    })

    const [{ isDragging }, drag] = useDrag({
        type: ItemTypes.CARD,
        item: { index, id, type: ItemTypes.CARD } as DragItem,
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    if (disabled) {
        return <div>{children}</div>;
    }    

    const opacity = isDragging ? 0.5 : 1;
    drag(drop(ref));
    return (
        <div ref={ref} style={{ ...style, opacity }} data-handler-id={handlerId}>
            {children ? children : undefined}
        </div>
    )
}

export default DraggableQuestionWrapper;
