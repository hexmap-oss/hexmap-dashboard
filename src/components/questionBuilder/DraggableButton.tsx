/* 
 * src/components/questionBuilder/draggableButton.tsx
 */
import { useDrag } from 'react-dnd';
import { SvgIconTypeMap } from '@material-ui/core';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
import Button from '@material-ui/core/Button';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import { Icon as MdiIcon } from '@mdi/react';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';

import { DragObject, FormTypes } from './questionBuilderTypes';


interface DraggableButtonProps {
    isDisabled?: boolean;
    text: string;
    Icon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
    MdiIcon?: string;
    classes?: ClassNameMap;
    draggedItem: DragObject;
}

const DraggableButton = (props: DraggableButtonProps) => {
    let buttonIcon = props.Icon ? (
        <props.Icon className={props.classes?.buttonIcon} /> 
    ) : undefined
    if (!buttonIcon && props.MdiIcon) {
        buttonIcon = <MdiIcon className={props.classes?.buttonIcon} path={props.MdiIcon} size={0.9} />
    }

    const [{ isDragging }, dragRef] = useDrag({
        type: FormTypes.BUTTON,
        item: () => {
            return props.draggedItem
        },
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging()
        }),
    })

    const opacity = isDragging ? 0.5 : 1
    return (
        <Button
            variant='contained'
            disableRipple={true}
            className={props.classes?.button}
            ref={dragRef}
            style={{
                opacity: opacity,
            }}
            color='primary'
            disabled={props.isDisabled}
        >
            <DragIndicatorIcon className={props.classes?.buttonIcon} />
            {buttonIcon}
            {props.text}
        </Button>
    );
};


export default DraggableButton;
