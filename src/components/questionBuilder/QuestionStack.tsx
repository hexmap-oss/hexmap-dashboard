/* 
 * src/components/surveyBuilder/QuestionStack.tsx
 */
import clsx from 'clsx';
import { useMemo, useRef, useCallback } from 'react';
import { ConnectDropTarget } from 'react-dnd';
import { DropTarget, useDrop } from 'react-dnd';
import { useDispatch } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { lightBlue } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import DraggableQuestionWrapper from './DraggableQuestionWrapper'
import { StackQuestion, FormTypes, DragObject, MoveStackQuestion, QuestionTypeMap } from './questionBuilderTypes';
import defaultQuestions from './cards/defaults';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: 'transparent',
            padding: 16
        },
        rootHover: {
            backgroundColor: lightBlue[50],
        },
        placeholder: {
            backgroundColor: theme.palette.common.white,
            borderColor: theme.palette.grey[300],
            borderStyle: 'dashed',
            borderWidth: 1,
            padding: 10,
            height: 100,
            textAlign: 'center'
        },
        placeholderTitle: {
            color: theme.palette.grey[600],
            fontSize: 18,
            marginTop: 5,
            fontWeight: 'bold'
        },
        placeholderText: {
            color: theme.palette.grey[600],
            fontSize: 14,
            marginTop: 14
        }        
    }),
);

export interface QuestionStackProps {
    questions: StackQuestion[];
    disabled: boolean;
    addQuestion: (question: StackQuestion) => void;
    deleteQuestion: (index: number) => void;
    moveQuestion: (payload: MoveStackQuestion) => void;
    saveQuestion: (question: StackQuestion) => void;

    connectDropTarget: ConnectDropTarget;
};


const QuestionStack = (props: QuestionStackProps) => {
    const classes = useStyles();
    const stackRef = useRef(null);
    const dispatch = useDispatch();
    const {
        questions,
        addQuestion,
        deleteQuestion,
        moveQuestion,
        saveQuestion,
        connectDropTarget
    } = props;

    // handle button drop
    const [{isOver}, dropRef] = useDrop(() => ({
        accept: FormTypes.BUTTON,
        drop: (item: DragObject) => handleAddQuestion(item.question),
        collect: (monitor) => ({
            isOver: !!monitor.isOver()
        })
    }), [questions]);

    // add new question when buttons are dragged to stack
    const handleAddQuestion = (questionType: string) => {
        const newQuestion = { ...defaultQuestions[questionType] };
        const existingMaxId = Math.max.apply(
            Math,
            questions.map(q => q.id)
        );
        newQuestion.id = existingMaxId + 1;
        dispatch(addQuestion(newQuestion));
    }

    // move existing questions within stack
    const handleMoveQuestion = useCallback(
        (fromIndex: number, toIndex: number) => {
            dispatch(moveQuestion({fromIndex, toIndex} as MoveStackQuestion));
        },
        [dispatch, moveQuestion]
    );

    const handleDeleteQuestion = useCallback(
        (question: StackQuestion) => {
            const questionData = questions.filter((q) => q.id === question.id)[0];
            const index = questions.indexOf(questionData);
            dispatch(deleteQuestion(index));
        },
        [deleteQuestion, dispatch, questions]
    );

    // save new state to question state
    const handleSaveQuestion = (question: StackQuestion) => {
        dispatch(saveQuestion(question));
    }

    // memoize the stack's elements to attempt to limit re-writes when dragging
    // question buttons to form. There is a slight lag due to the css background color
    // of the stack changing on hover when dragging a button. This could be snakeoil.`    
    const memoizedStack = useMemo(() => {
        return questions.map((question: StackQuestion, index) => {
            const Question = QuestionTypeMap[question.type];

            return (
                <DraggableQuestionWrapper
                    key={question.id}
                    id={`${question.id}`}
                    index={index}
                    moveCard={handleMoveQuestion}
                    disabled={props.disabled}
                >
                    <Question
                        id={question.id}
                        type={question.type}
                        title={question.title}
                        fieldName={question.fieldName}
                        options={question.options}
                        deleteQuestion={handleDeleteQuestion}
                        saveQuestion={handleSaveQuestion}
                        hardcodedId={question.hardcodedId}
                        disabled={props.disabled}
                    />
                </DraggableQuestionWrapper>
            );
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps 
    }, [props.disabled, questions]);

    const placeholderQuestion = (
        <Paper className={classes.placeholder} elevation={1}>
            <Box>
                <Typography className={classes.placeholderTitle}>Question Placeholder</Typography>
            </Box>
            <Typography className={classes.placeholderText}>Drop your first question here to get started.</Typography>
        </Paper>        
    );

    connectDropTarget(stackRef);
    return (
        <div 
            className={isOver
                ? clsx(classes.root, classes.rootHover)
                : classes.root
            } 
            ref={dropRef}
        >
            <div ref={stackRef}>
                {questions.length > 0 ? memoizedStack : placeholderQuestion}
            </div>
        </div>
    );
}

export default DropTarget(FormTypes.CARD, {}, (connect) => ({
    connectDropTarget: connect.dropTarget(),
}))(QuestionStack);
