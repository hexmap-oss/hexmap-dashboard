/* 
 * src/components/usersTable/UsersTableStyles.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


export const useTableStyles = makeStyles((theme: Theme) =>
    createStyles({
        table: {
            minWidth: 750
        },
        tableRowMultiSelect: {
            // '&.MuiTableRow-hover:hover': {
            //     backgroundColor: 'inherit',
            // },
            '&.Mui-selected, &.Mui-selected:hover': {
                backgroundColor: 'inherit',
                // '& > .MuiTableCell-root': {
                //     color: theme.palette.common.white
                // }
            }            
        },
        tableRowSingleSelect: {
            '&.MuiTableRow-hover:hover': {
                backgroundColor: 'inherit',
            },            
            '&.Mui-selected, &.Mui-selected:hover': {
                backgroundColor: theme.palette.primary.light,
                '& > .MuiTableCell-root': {
                    color: theme.palette.grey[800]
                }
            }            
        },
        visuallyHidden: {
            border: 0,
            clip: 'rect(0, 0, 0, 0)',
            height: 1,
            margin: -1,
            overflow: 'hidden',
            padding: 0,
            position: 'absolute',
            top: 20,
            width: 1,
        }
    })
);