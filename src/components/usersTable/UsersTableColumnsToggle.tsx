/* 
 * src/components/usersTable/UsersTableColumnsToggle.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

import { UsersTableColumn } from './usersTableTypes';



const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',

            [theme.breakpoints.down('sm')]: {
                display: 'none',
            },
        },
        checkbox: {
            borderRadius: 16,
            backgroundColor: theme.palette.primary.light,
            height: 36,
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.light,
            marginRight: 20,
            marginTop: 4,
            padding: '0 10px 0 5px',
            WebkitTransition: 'all 0.2s',

            '&:hover': {
                backgroundColor: theme.palette.primary.light,
                borderColor: theme.palette.primary.main,
            }
        },
    })
);



interface ColumnsToggleProps {
    columns: UsersTableColumn[];
    onToggleColumnVisibility: (col: string, isChecked: boolean) => void;
}


const ColumnsToggle = (props: ColumnsToggleProps) => {
    const classes = useStyles();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        props.onToggleColumnVisibility(event.target.name, event.target.checked);
    }

    return(
        <FormGroup row className={classes.root}>
        {props.columns.map((col: UsersTableColumn, idx: number) => (
            <FormControlLabel
                className={classes.checkbox}
                control={
                    <Checkbox
                        checked={col.visible}
                        // labelStyle={clsx(col.visible ? classes.checkboxTextActive : undefined)}
                        checkedIcon={<CheckCircleOutline />}
                        icon={<RadioButtonUnchecked />}
                        onChange={handleChange}
                        name={col.label}
                        color={col.visible ? 'primary' : 'secondary'}
                    />
                }
                key={idx}
                label={col.label}
            />                
        ))}
        </FormGroup>
    );
}

export default ColumnsToggle;
