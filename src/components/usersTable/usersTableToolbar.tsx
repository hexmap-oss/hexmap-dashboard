/* 
 * src/components/usersTable/UsersTableToolbar.tsx
 */
import clsx from 'clsx';
import { createStyles, makeStyles, lighten, Theme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(1),
        },
        highlightDefault: {
            color: theme.palette.primary.main,
            backgroundColor: lighten(theme.palette.primary.light, 0.85)
        },
        highlightDelete: {
            color: theme.palette.error.main,
            backgroundColor: lighten(theme.palette.error.light, 0.85)
        },
        title: {
            flex: '1 1 100%',
        }
    }
));

interface UsersTableToolbarProps {
    numSelected: number;
    buttonAriaLabel?: string;
    buttonTooltipTitle: string;
    colorScheme?: string;
    onButtonClick?: (event: React.MouseEvent) => void;
    tableTitle?: string;
    ActionIcon?: typeof SvgIcon;
}

const UsersTableToolbar = (props: UsersTableToolbarProps) => {
    const classes = useStyles();
    const { 
        buttonAriaLabel,
        buttonTooltipTitle,
        colorScheme,
        onButtonClick,
        numSelected,
        ActionIcon
    } = props;

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlightDefault]: numSelected > 0,
                [classes.highlightDelete]: numSelected > 0 && colorScheme === 'delete',
            })}
        >
            { numSelected > 0
                ? <Typography className={classes.title} variant='subtitle1' component='div'>
                    {numSelected} selected
                </Typography>
                : <Typography className={classes.title} variant='h6' id={'table-title'} component='div'>
                    {props.tableTitle}
                </Typography>
            }

            { numSelected > 0 
                ? <Tooltip title={buttonTooltipTitle}>
                    {/* 'delete users' */}
                    <IconButton
                        aria-label={buttonAriaLabel}
                        onClick={onButtonClick}
                    >
                        {ActionIcon ? <ActionIcon /> : <DeleteIcon />}
                    </IconButton>
                </Tooltip>
                : undefined 
            }
        </Toolbar>
    );
}


export default UsersTableToolbar;
