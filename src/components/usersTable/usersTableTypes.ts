/* 
 * src/components/usersTable/UsersTableTypes.ts
 */

export type Order = 'asc' | 'desc';


export interface UsersTableColumn {
    rowKey: keyof UsersTableRow;
    label: string;
    date?: boolean;
    disablePadding?: boolean;
    numeric?: boolean;
    visible?: boolean;
}

export interface UsersTableRow {
    id: number;
    selectable: boolean;
}

