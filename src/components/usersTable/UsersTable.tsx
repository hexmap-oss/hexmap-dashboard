/* 
 * src/components/usersTable/UsersTable.tsx
 */
import React, { useState } from 'react';
import format from 'date-fns/format';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import PublicIcon from '@material-ui/icons/Public';

import UsersTableHead from './usersTableHead';
import UsersTableToolbar from './usersTableToolbar';
import { useTableStyles } from './usersTableStyles';
import { Order, UsersTableColumn, UsersTableRow } from './usersTableTypes';
import { useMemo } from 'react';
import ColumnsToggle from './UsersTableColumnsToggle';


function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function formatCellValue(column: UsersTableColumn, row: UsersTableRow) {
    let cellValue: any = row[column.rowKey];
    if (column.date) {
        return format(parseInt(cellValue), 'yyyy-mm-dd HH:mm:ss');
    }
    if (typeof cellValue === 'boolean') {
        return String(cellValue);
    }
    return cellValue;    
}

interface UsersTableProps {
    title?: string;
    colorScheme?: string;
    columns: UsersTableColumn[];
    getComparator: (order: Order, orderBy: keyof UsersTableRow) =>
        (a: UsersTableRow, b: UsersTableRow) => number;
    onChangePage?: (newPage: number) => void;
    onChangeRowsPerPage?: (rowsPerPage: number) => void;
    onRowClick?: (rowIndex: number) => void;
    onSort?: (order: Order, orderBy: keyof UsersTableRow) => void;
    onToolbarButtonClick: (rows: UsersTableRow[]) => void;
    onToggleColumnVisibility?: (column: string, isVisible: boolean) => void;
    page?: number;
    rows: UsersTableRow[];
    rowsPerPage?: number;
    sorting?: Map<string, string | number>;
    toolbar?: JSX.Element;
    toolbarButtonAriaLabel: string;
    toolbarButtonTooltipTitle: string;
    totalRows?: number;
    selectMany?: boolean;
    disableSelectAll?: boolean;
};

const UsersTable = (props: UsersTableProps) => {
    const classes = useTableStyles();
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState<keyof UsersTableRow>('id');
    const [selected, setSelected] = useState<number[]>([]);
    const [_page, setPage] = useState(0);
    const [_rowsPerPage, setRowsPerPage] = useState(8);

    const page = props.page ? props.page : _page;
    const rowsPerPage = props.rowsPerPage ? props.rowsPerPage : _rowsPerPage;
    const totalRows = props.totalRows ? props.totalRows : props.rows.length;
    const isSelected = (rowId: number) => selected.indexOf(rowId) !== -1;


    const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof UsersTableRow) => {
        const isAsc = orderBy === property && order === 'asc';
        if (props.onSort) {
            props.onSort(order, orderBy);
        } else {
            setOrder(isAsc ? 'desc' : 'asc');
            setOrderBy(property);
        }
    };

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelectedRows = props.rows.map((r) => r.id);
            setSelected(newSelectedRows);
            return;
        }
        setSelected([]);
    };

    const handleRowClick = (event: React.MouseEvent<unknown>, rowId: number) => {
        const selectedIndex = selected.indexOf(rowId);
        if (props.selectMany) {
            let newSelected: number[] = [];

            if (selectedIndex === -1) {
                newSelected = newSelected.concat(selected, rowId);
            } else if (selectedIndex === 0) {
                newSelected = newSelected.concat(selected.slice(1));
            } else if (selectedIndex === selected.length - 1) {
                newSelected.concat(selected.slice(0, -1));
            } else if (selectedIndex > 0) {
                newSelected = newSelected.concat(
                    selected.slice(0, selectedIndex),
                    selected.slice(selectedIndex + 1),
                );
            }
            setSelected(newSelected);
        } else {
            setSelected([rowId]);
        }

        if (props.onRowClick) {
            props.onRowClick(rowId);
        }
    };

    const handleToolbarButtonClick = (event: React.MouseEvent) => {
        const selectedRows = props.rows.filter(row => isSelected(row.id));
        props.onToolbarButtonClick(selectedRows);
    };

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        if (props.onChangePage) {
            props.onChangePage(newPage);
        } else {
            setPage(newPage);
        }
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const rowsPerPage = parseInt(event.target.value, 10);
        if (props.onChangeRowsPerPage) {
            props.onChangeRowsPerPage(rowsPerPage);
        } else {
            setRowsPerPage(rowsPerPage);
            setPage(0);
        }
    };

    const rows = useMemo(() => {
        // setSelected([]);  // unselect all old rows
        return props.page
            ? stableSort(props.rows, props.getComparator(order, orderBy))
            : stableSort(props.rows, props.getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    }, [props, order, orderBy, page, rowsPerPage]);

    const rowClassName = props.selectMany ? classes.tableRowMultiSelect: classes.tableRowSingleSelect;
    return (
        <>
            { props.toolbar !== undefined
              ? props.toolbar
              : (
                <UsersTableToolbar
                    buttonAriaLabel={props.toolbarButtonAriaLabel}
                    buttonTooltipTitle={props.toolbarButtonTooltipTitle}
                    colorScheme={props.colorScheme}
                    numSelected={selected.length}
                    onButtonClick={handleToolbarButtonClick}
                    tableTitle={props.title}
                    ActionIcon={PublicIcon}
                />
              )
            }
            
            { props.onToggleColumnVisibility
                ? (
                    <ColumnsToggle
                        columns={props.columns}
                        onToggleColumnVisibility={props.onToggleColumnVisibility}
                    />                    
                )
                : undefined
            }

            <TableContainer>
                <Table
                    className={classes.table}
                    aria-labelledby='tableTitle'
                    aria-label='users table'
                >
                    <UsersTableHead
                        classes={classes}
                        columns={props.columns}
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={props.rows.length}
                        selectMany={props.selectMany}
                        disableSelectAll={props.disableSelectAll}
                    />


                    <TableBody>
                        {rows.map((row, rowIndex) => {
                                const isItemSelected = isSelected(row.id);
                                const labelId = `users-table-checkbox-${rowIndex}`;

                                return(
                                    <TableRow
                                        hover
                                        className={rowClassName}
                                        onClick={(event: React.MouseEvent<unknown>) => {
                                            if (row.selectable) handleRowClick(event, row.id)
                                        }}
                                        role='checkbox'
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={`row-${row.id}`}
                                        selected={isItemSelected}
                                        style={{
                                            whiteSpace: 'nowrap'
                                        }}
                                    >
                                        {/* Show checkboxes on multiple row selectable tables */}
                                        {props.selectMany
                                            ? row.selectable 
                                                ? <TableCell padding='checkbox'>
                                                    <Checkbox
                                                        checked={isItemSelected}
                                                        inputProps={{ 'aria-labelledby': labelId }}
                                                        color='primary'
                                                    />
                                                </TableCell>
                                                : <TableCell padding='checkbox' />
                                            : undefined
                                        }

                                        {/* Format table rows */}
                                        {props.columns
                                            .filter(column => column.visible !== false)
                                            .map((column, columnIndex) => {
                                                return (
                                                    <TableCell
                                                        align={column.numeric ? 'right': 'left'}
                                                        key={`cell-${rowIndex}-${columnIndex}`}
                                                    >
                                                        {formatCellValue(column, row)}
                                                    </TableCell>
                                                );
                                            })
                                        }
                                    </TableRow>
                                );
                            })}

                        {/* Display empty rows to fill expected table size */}
                        {/* {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )} */}
                    </TableBody>
                </Table>
            </TableContainer>

            <TablePagination
                rowsPerPageOptions={[8, 10, 20, 50]}
                component='div'
                count={totalRows}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    );
}

export default UsersTable;
