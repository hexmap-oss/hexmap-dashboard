/* 
 * src/components/usersTable/UsersTableHead.tsx
 */
import Checkbox from '@material-ui/core/Checkbox';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';

import { useTableStyles } from './usersTableStyles';
import { Order, UsersTableColumn, UsersTableRow } from './usersTableTypes';


interface UsersTableHeadProps {
    classes: ReturnType<typeof useTableStyles>;
    columns: UsersTableColumn[];
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof UsersTableRow) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
    selectMany?: boolean;
    disableSelectAll?: boolean;
};


const UsersTableHead = (props: UsersTableHeadProps) => {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

    const createSortHandler = (property: keyof UsersTableRow) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                { props.selectMany
                    ? <TableCell padding='checkbox'>
                        { !props.disableSelectAll 
                            ? <Checkbox
                                indeterminate={numSelected > 0 && numSelected < rowCount}
                                checked={rowCount > 0 && numSelected === rowCount}
                                onChange={onSelectAllClick}
                                inputProps={{ 'aria-label': 'select all desserts' }}
                            />
                            : undefined
                        }
                    </TableCell>
                    : undefined
                }

                {props.columns
                    .filter((column) => column.visible !== false)
                    .map((column) => (
                    <TableCell
                        key={column.rowKey}
                        align={column.numeric ? 'right': 'left'}
                        padding={column.disablePadding ? 'none': 'normal'}
                        sortDirection={orderBy === column.rowKey ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === column.rowKey}
                            direction={orderBy === column.rowKey ? order : 'asc'}
                            onClick={createSortHandler(column.rowKey)}
                        >
                            <b>{column.label}</b>
                            {orderBy === column.rowKey ? (
                                <span className={classes.visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>          
                ))}
            </TableRow>
        </TableHead>
    );
}


export default UsersTableHead;
