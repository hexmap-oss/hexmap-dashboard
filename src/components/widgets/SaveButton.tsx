/* 
 * src/components/widgets/SaveButton.tsx
 */
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import CheckIcon from '@material-ui/icons/Check';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

interface SaveButtonProps {
    className?: string;
    isEdited: boolean;
    isSaving: boolean;
    onClick: () => void;
}

const SaveButton = (props: SaveButtonProps) => {
    const buttonIcon = () => {
        if (props.isSaving) {
            return <CircularProgress />
        } else if (props.isEdited) {
            return <MoreHorizIcon />
        } else {
            return <CheckIcon />
        }
    }

    return (
        <>
            <Button
                className={props.className}
                variant='contained'
                color='primary'
                size='small'
                startIcon={buttonIcon()}
                onClick={props.onClick}
            >
            Save
            </Button>
        </>
    );
}

export default SaveButton;
