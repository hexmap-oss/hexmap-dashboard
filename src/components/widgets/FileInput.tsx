/* 
 * src/components/widgets/FileInput.tsx
 */
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import PublishIcon from '@material-ui/icons/Publish';
import { Typography } from '@material-ui/core';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        inputGroup: {
            borderCollapse: 'separate',
            position: 'relative',
            display: 'table',

        },
        inputLabel: {
            display: 'table-cell',
            padding: '5px 12px',
            border: '1px solid #ccc',
            borderRadius: 4,
            borderRight: 0,
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
            fontSize: 14,
            fontWeight: 400,
            lineHeight: 1,
            backgroundColor: '#eee',
            whiteSpace: 'nowrap',
            width: '1%',
        },
        inputIcon: {
            height: 16,
            width: 16,
            verticalAlign: 'middle',
        },
        labelText: {
            fontSize: 14,
            display: 'inline-block',
            verticalAlign: 'middle'
        },
        input: {
            display: 'table-cell',
            height: 34,
            padding: '6px 12px',
            border: '1px solid #ccc',
            width: '100%',
            borderRadius: 4,
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0
        }
    })
);

interface FileInputProps {
    className?: string;
    labelText: string;
}

const FileInput = (props: FileInputProps) => {
    const classes = useStyles();

    return (
        <Box className={clsx(classes.inputGroup, props.className)}>
            <span className={classes.inputLabel}>
                <PublishIcon className={classes.inputIcon} />
                <Typography className={classes.labelText}>{props.labelText}</Typography>
            </span>
            <input className={classes.input} type='file' />
        </Box>
    );
};

export default FileInput;
