/* 
 * src/components/widgets/ColorLegend.tsx
 */
import interpolate from 'color-interpolate';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { Paper } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        title: {
            textAlign: 'left',
            marginBottom: 8,
            fontWeight: 'bold',
            fontSize:'large'
        },
        scaleContainer: {
            margin: 0,
            padding: 0,
            float: 'left',
            listStyleType: 'none'
        },
        scale: {
            display: 'flex',
            float: 'left',
            listStyleType: 'none',
            marginBottom: 6,
            textAlign: 'center',
            width: 50,
        },
        scaleItemLabel: {
            display: 'block',
            float: 'left',
            height: 15,
            width: 50,
        },
        description: {
            textAlign: 'center',
            fontSize:'small',
            display: 'flex',
            flexWrap: 'wrap',
            marginRight:'-1rem',
            marginLeft: '1rem',
            fontWeight: 'bold',
        },
        legendPaper: {
            background: 'aliceblue',
            width: 'inherit',
            height: '10vh',
            maxHeight: '10vh',
            bottom:'0px'

        },
    })
);



const ColorLegend = () => {
    const classes = useStyles();
    const palette = interpolate(['blue', 'green', 'red']);

    const range = (min: number, max: number, numBreaks: number) => {
        // if only one argument supplied then return random number between 1 and argument
        if (max === undefined) {
            max = min;
            min = 0;
        }
        const breakIndexes = [...Array(numBreaks).keys()];
        return breakIndexes.map(x => (x + 1) * (max - min) / (numBreaks));
    }

    const interpolateColor = (interpolateValue: number) => {
        return palette(interpolateValue);
    }

    return (
        <div >
            <Paper className={classes.legendPaper}>
                <div className={classes.title}>Legend</div>
                <div className={classes.scaleContainer}>
                    <Grid container className={classes.scaleContainer}>
                        <Grid item xs = {3} className={classes.description}>
                            <div className={classes.description}>Num. Trips</div>
                        </Grid>
                        <Grid item xs = {3}>
                            <ul className={classes.scale}>
                                { range(0, 1, 6).map(idx => {
                                    const colors = interpolateColor(idx);
                                    const itemLabel = Math.round(idx *100) / 100
                                    console.log(idx, colors)
                                    return (
                                        <li key = {idx}>
                                            <span className = {classes.scaleItemLabel} style={{backgroundColor: colors}}></span>
                                            { itemLabel }
                                        </li>
                                    );
                                }) }
                            </ul>
                        </Grid>
                    </Grid>
                </div>      
            </Paper>      
        </div>
    );
}

export default ColorLegend;
