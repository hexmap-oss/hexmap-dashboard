/* 
 * src/components/widgets/InviteCodeWidget.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            position: 'relative',
        },
        searchInput: {
            borderWidth: 2,
            borderColor: 'transparent',
            borderBottomColor: theme.palette.primary.main,
            borderStyle: 'solid',
            fontSize: 14,
            fontWeight: 600,
            height: 50,
            paddingLeft: 40,
            width: '100%',

            '&:focus': {
                boxShadow: '0 5px 20px 0 rgba(0,0,0,0.15)',
                borderColor: theme.palette.primary.main,
                outline: 0,
                transition: 'all 200ms ease'
            }
        },
        searchInputIcon: {
            position: 'absolute',
            top: 13,
            left: 10,
            color: theme.palette.grey[500],
            fontSize: 24
        }
    })
);


interface SearchInputProps {
    searchText: string,
    onChange: (searchText: string) => void
}

const SearchInput = (props: SearchInputProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <input
                type="text"
                className={classes.searchInput}
                // placeholder={this.props.intl.formatMessage({id: this.props.searchHelpTextId})}
                value={props.searchText}
                onChange={(e) => props.onChange(e.target.value)} 
            />
            <div className={classes.searchInputIcon}>
                <SearchIcon />
            </div>
        </Box>
    );
};

export default SearchInput;
