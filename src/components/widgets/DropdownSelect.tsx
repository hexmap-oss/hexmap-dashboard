/* 
 * src/components/widgets/DropdownSelect.tsx
 */
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import { capitalize } from '../../app/utils';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        }
    })
);


interface DropdownSelectProps {
    label: string;
    className?: string;
    options: string[];
    value: string;
    onChange: (value: string) => void;
}

const DropdownSelect = (props: DropdownSelectProps) => {
    const classes = useStyles();
    const lowerLabel = props.label.toLocaleLowerCase()

    const handleChangeValue = (event: React.ChangeEvent<{ value: unknown }>) => {
        props.onChange(event.target.value as string);
    }
    
    return (
        <FormControl className={clsx(classes.formControl, props.className)}>
            <InputLabel id={lowerLabel + '-select-label'}>{props.label}</InputLabel>
            <Select
                labelId={lowerLabel + '-select-label'}
                id={lowerLabel + '-select'}
                value={props.value}
                onChange={handleChangeValue}
                displayEmpty
            >
            {props.options.map((option: string, index: number) => (
                <MenuItem key={index} value={option}>{capitalize(option)}</MenuItem>
            ))}
            </Select>
        </FormControl>
    );
};

export default DropdownSelect;
