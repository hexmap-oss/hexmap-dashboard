/* 
 * src/component/widgets/GridItemAlignedInput.tsx
 *
 * Displays a responsive full-width Grid item with aligned labels and TextFields
 * Content: ~2/3 page width, aligned left. Used in SettingsPanel cards.
 */
import React from 'react';
import { InputProps } from '@material-ui/core/Input';
import { FilledInputProps } from '@material-ui/core/FilledInput';
import { OutlinedInputProps } from '@material-ui/core/OutlinedInput';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { Typography } from '@material-ui/core';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        label: {
            display: 'flex',
            justifyContent: 'center'
        },
        labelText: {
            fontWeight: 700,
            fontSize: 14
        },
        icon: {
            '& svg': {
                width: '0.9em',
                height: '0.9em',
            }
        }        
    })
);


interface GridItemAlignedInputProps {
    label: string,
    value: string | number,
    type?: string,
    Icon: JSX.Element,
    InputProps?: Partial<InputProps> | Partial<FilledInputProps> | Partial<OutlinedInputProps> | undefined,
    onChange: (event: string | number) => void,
}

const GridItemAlignedInput = (props: GridItemAlignedInputProps) => {
    const classes = useStyles();

    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (props.type === 'number') {
            const value = event.target.value;
            // DANGER!: why does this work to evaluate numbers vs text?
            if (value.length > 0) {
                props.onChange(value);
            }
        } else {
            props.onChange(event.target.value);
        }
    }

    return (
        <>
            <Grid item className={classes.label} xs={4}>
                <Typography className={classes.labelText}>{props.label}</Typography>
            </Grid>
            <Grid item xs={8} sm={6} md={5}>
                {/* Input with icon offset by Grid */}
                <Grid container spacing={4} alignItems='flex-end'>
                    <Grid className={classes.icon} item xs={1}>
                        {props.Icon}
                    </Grid>
                    <Grid item xs>
                        <Box style={{width: '100%'}}>
                            <TextField
                                id='input-with-icon-grid'
                                value={props.value}
                                type={props.type}
                                InputProps={props.InputProps}
                                onChange={handleOnChange}
                                fullWidth={true}
                            />
                        </Box>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};


export default GridItemAlignedInput;
