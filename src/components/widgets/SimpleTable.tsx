/* 
 * src/components/widgets/SimpleTable.tsx
 */
import clsx from 'clsx';
import { ReactNode } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        table: {},
        headerCell: {
            fontWeight: 600,
        }
    })
);

type tableHeader = {
    key: string
    label: ReactNode
}

type tableRow = {
    [key: string]: any
};

interface SimpleTableProps {
    className?: string;
    headers: tableHeader[];
    rows: tableRow[];
}

const SimpleTable = (props: SimpleTableProps) => {
    const classes = useStyles();

    return (
        <Box className={clsx(classes.root, props.className)}>
            <TableContainer>
                <Table
                    className={classes.table}
                    aria-label='simple table'
                >
                    <TableHead>
                        <TableRow>
                            <TableCell key={'id'}></TableCell>
                            { props.headers.map(header => (
                                <TableCell className={classes.headerCell} key={header.key} align='right'>{header.label}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    { props.rows.map((row: tableRow, rowIdx: number) => {
                        return (
                            <TableRow key={rowIdx}>
                                <TableCell align='right'>{row['id']}</TableCell>
                                { props.headers.map((header, headerIdx: number) => {
                                    const cellKey = `${rowIdx}-${headerIdx}`;
                                    return (
                                        <TableCell align='right' key={cellKey}>{row[header.key]}</TableCell>
                                    );
                                })}
                            </TableRow>
                        );
                    })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
};

export default SimpleTable;
