/* 
 * src/components/widgets/DateRangeButtonSelect.tsx
 */
import clsx from 'clsx';
import moment from 'moment';
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import DateRangeButton from './DateRangeButton';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        group: {
            paddingLeft: 16,
            paddingRight: 16,
        }
    }),
);

interface DateRangeButtonSelectProps {
    className?: string,
    setDates: (start: number, end: number) => void,
};

const DateRangeButtonSelect = (props: DateRangeButtonSelectProps) => {
    const classes = useStyles();
    const [activeButtonId, setActiveButtonId] = useState('days-7');
    const buttonNums = [60, 30, 7];
    const buttonPeriods = [{
        period: 'month',
        text: 'This month' 
    }, {
        period: 'week',
        text: 'This week'         
    }, {
        period: 'today',
        text: 'Today'         
    }];    

    const handleSetByDays = (days: number) => {
        const endDate = moment().endOf('day');
        const startDate = endDate.clone().subtract(days, 'days');
        props.setDates(startDate.valueOf(), endDate.valueOf());
        setActiveButtonId(`days-${days}`);
    }

    const handleSetByPeriod = (period: string) => {
        let startDate;
        switch(period) {
            case 'month':
                startDate = moment().startOf('month');
                break;
            case 'week':
                startDate = moment().startOf('week');
                break;
            case 'today':
                startDate = moment().startOf('day');
                break;
            default:
                return;
        }
        const endDate = moment().endOf('day');
        props.setDates(startDate.valueOf(), endDate.valueOf());
        setActiveButtonId(`relative-${period}`);
    }
    

    let combinedRootClasses = classes.root;
    if (props.className) {
        combinedRootClasses = clsx(classes.root, props.className)
    }
    return(
        <Box className={combinedRootClasses}>
            <ButtonGroup className={classes.group} color='primary' aria-label='outlined primary button group'>
                {buttonNums.map((num, idx) => (
                    <DateRangeButton
                        key={`days-${idx}`}
                        range={num}
                        text={`${num} Days`}
                        isSelected={activeButtonId === `days-${num}`}
                        onClick={handleSetByDays}
                    />                    
                ))}
            </ButtonGroup>

            <ButtonGroup className={classes.group} color='primary' aria-label='outlined primary button group'>
                {buttonPeriods.map((d, idx) => (
                    <DateRangeButton
                        key={`period-${idx}`}
                        range={d.period}
                        text={d.text}
                        isSelected={activeButtonId === `relative-${d.period}`}
                        onClick={handleSetByPeriod}
                    />
                ))}
            </ButtonGroup>            
        </Box>
    );
}

export default DateRangeButtonSelect;
