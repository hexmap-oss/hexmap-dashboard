/* 
 * src/component/widgets/DayByDaySelect.tsx
 */
import clsx from 'clsx';
import DateFnsUtils from '@date-io/date-fns';
import { addDays, isAfter, isBefore, startOfDay, subDays } from 'date-fns';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { 
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
 } from '@material-ui/pickers';
import endOfDay from 'date-fns/endOfDay';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        dayArrowButton: {
            border: 'none',
            backgroundColor: 'inherit',
        },
        dayArrow: {
            fontSize: 40,
            color: theme.palette.grey[500],
            marginLeft: theme.spacing(2),
            marginRight: theme.spacing(2),
        },
        dayArrowActive: {
            cursor: 'pointer',
            fill: theme.palette.primary.main,
            transition: theme.transitions.create('fill', {
                easing: theme.transitions.easing.sharp,
                duration: '100ms',
            }),
            '&:hover': {
                fill: theme.palette.primary.light,
            }
        },
        datePicker: {
            '& .MuiSvgIcon-root': {
                color: theme.palette.primary.main,
            },
            '& .MuiInput-underline:hover:not(.Mui-disabled):before': {
                borderColor: theme.palette.primary.light,
            } 
        }
    })
);

interface DayByDaySelectProps {
    collectionStartTime: number;
    collectionEndTime: number;
    startTime: number;
    onChangeStartTime: (startTime: number) => void;
    onChangeEndTime: (endTime: number) => void;
}

const DayByDaySelect = (props: DayByDaySelectProps) => {
    const classes = useStyles();
    const { startTime, onChangeEndTime, onChangeStartTime } = props;
    const selectedDate = startOfDay(startTime);
    const prevIsEnabled = isAfter(subDays(selectedDate, 1), props.collectionStartTime);
    const nextIsEnabled = isBefore(addDays(selectedDate, 1), props.collectionEndTime);


    const handleDateChange = (newDate: Date | null) => {
        if (newDate) {
            const newStartTime = startOfDay(newDate).getTime();
            const newEndTime = endOfDay(newDate).getTime();
            onChangeStartTime(newStartTime);
            onChangeEndTime(newEndTime);
        }
    }

    const handlePreviousDay = () => {
        if (selectedDate) {
            const newDate = subDays(selectedDate, 1);
            handleDateChange(newDate);            
        }
    };

    const handleNextDay = () => {
        if (selectedDate) {
            const newDate = addDays(selectedDate, 1);
            handleDateChange(newDate);
        }
    };

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <button className={classes.dayArrowButton} onClick={handlePreviousDay} disabled={!prevIsEnabled}>
                <NavigateBeforeIcon 
                    className={prevIsEnabled
                        ? clsx(classes.dayArrow, classes.dayArrowActive)
                        : classes.dayArrow}
                />
            </button>

            <KeyboardDatePicker
                id='date-picker-dialog'
                className={classes.datePicker}
                label='Date picker dialog'
                format='MM/dd/yyyy'
                minDate={props.collectionStartTime}
                maxDate={props.collectionEndTime}
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />            
        
            <button className={classes.dayArrowButton} onClick={handleNextDay} disabled={!nextIsEnabled}>
                <NavigateNextIcon
                    className={nextIsEnabled
                        ? clsx(classes.dayArrow, classes.dayArrowActive)
                        : classes.dayArrow
                    }
                />
            </button>
        </MuiPickersUtilsProvider>
    );
}

export default DayByDaySelect;
