/* 
 * src/components/widgets/DateTimeSlider.tsx
 */
import clsx from 'clsx';
import { format, endOfDay, isAfter, isBefore, startOfDay, subDays, addDays } from 'date-fns';
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import PrettoSlider from './PrettoSlider';
import { useEffect } from 'react';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        dayArrow: {
            fontSize: 40,
            color: theme.palette.grey[500],
        },
        dayArrowActive: {
            cursor: 'pointer',
            fill: theme.palette.primary.main,
            transition: theme.transitions.create('fill', {
                easing: theme.transitions.easing.sharp,
                duration: '100ms',
            }),
            '&:hover': {
                fill: theme.palette.primary.light,
            }
        },        
        slider: {
            flexGrow: 1,
        },
        sliderLabelGroup: {
            marginBottom: -30,            
        },
        sliderLabel: {
            display: 'inline-block',
        },
        dateText: {
            fontSize: 10,            
        },
        sliderLabelRight: {
            float: 'right'
        }
    })
);


interface DateTimeSliderProps {
    collectionStartTime: number;
    collectionEndTime: number;
    sliderStartTime: number;
    sliderEndTime: number;
    onChangeStartTime: (startTime: number) => void;
    onChangeEndTime: (endTime: number) => void;
};


const DateTimeSlider = (props: DateTimeSliderProps) => {
    const classes = useStyles();
    const { sliderStartTime, sliderEndTime, onChangeEndTime, onChangeStartTime } = props;
    // internal state to track time without to mitigate sending an API request on each tick
    const [internalStartTime, setInternalStartTime] = useState(sliderStartTime);
    const [internalEndTime, setInternalEndTime] = useState(sliderEndTime);
    const dayMinTime = startOfDay(sliderStartTime).getTime();
    const dayMaxTime = endOfDay(sliderStartTime).getTime();
    const prevIsEnabled = isAfter(subDays(dayMinTime, 1), props.collectionStartTime);
    const nextIsEnabled = isBefore(addDays(dayMinTime, 1), props.collectionEndTime);    


    useEffect(() => {
        setInternalStartTime(sliderStartTime);
        setInternalEndTime(sliderEndTime);
    }, [sliderStartTime, sliderEndTime])

    const handlePreviousDay = () => {
        const nextStartTime = subDays(dayMinTime,  1).getTime();
        const nextEndTime = subDays(dayMaxTime, 1).getTime();
        onChangeStartTime(nextStartTime);
        onChangeEndTime(nextEndTime);        
    };

    const handleNextDay = () => {
        const nextStartTime = addDays(dayMinTime, 1).getTime();
        const nextEndTime = addDays(dayMaxTime, 1).getTime();
        onChangeStartTime(nextStartTime);
        onChangeEndTime(nextEndTime);
    };

    const handleChangeTime = (event: any, newValue: number | number[]) => {
        if (Array.isArray(newValue)) {
            const [startTime, endTime] = newValue;
            setInternalStartTime(startTime);
            setInternalEndTime(endTime);
        }
    };

    const handleCommitChangeTime = (event: any, newValue: number | number[]) => {
        if (Array.isArray(newValue)) {
            const [startTime, endTime] = newValue;
            onChangeStartTime(startTime);
            onChangeEndTime(endTime);
        }
    };

    return (
        <Box className={classes.root}>
            <Button onClick={handlePreviousDay}  disabled={!prevIsEnabled}>
                <NavigateBeforeIcon
                    className={prevIsEnabled
                        ? clsx(classes.dayArrow, classes.dayArrowActive)
                        : classes.dayArrow}
                />
            </Button>

            <Box className={classes.slider}>
                <Box className={classes.sliderLabelGroup}>
                    <Box className={classes.sliderLabel}>
                        <Typography
                            id='time-slider'
                            variant='subtitle2'
                            className={classes.dateText}
                        >
                            {format(new Date(sliderStartTime), 'LLL d, yyyy')}
                        </Typography>

                        <Typography
                            id='time-slider'
                            variant='subtitle2'
                            className={classes.dateText}
                        >
                            {format(new Date(sliderStartTime), 'hh:mm:ss aaa')}
                        </Typography>                        
                    </Box>                  

                    <Box className={clsx(classes.sliderLabel, classes.sliderLabelRight)}>
                        <Typography
                            id='time-slider'
                            variant='subtitle2'
                            className={classes.dateText}
                        >
                            {format(new Date(sliderEndTime), 'LLL d, yyyy')}
                        </Typography>

                        <Typography
                            id='time-slider'
                            variant='subtitle2'
                            className={classes.dateText}
                        >
                            {format(new Date(sliderEndTime), 'hh:mm:ss aaa')}
                        </Typography>                                          
                    </Box>
                </Box>
                <PrettoSlider
                    className={classes.slider}
                    min={dayMinTime}
                    max={dayMaxTime}
                    value={[internalStartTime, internalEndTime]}
                    onChange={handleChangeTime}
                    onChangeCommitted={handleCommitChangeTime}
                    aria-labelledby='time-slider'
                />
            </Box>

            <Button onClick={handleNextDay} disabled={!nextIsEnabled}>
                <NavigateNextIcon
                    className={nextIsEnabled
                        ? clsx(classes.dayArrow, classes.dayArrowActive)
                        : classes.dayArrow
                    }
                />
            </Button>
        </Box>
    );
}

export default DateTimeSlider;
