/* 
 * src/components/widgets/DateRangeButtonSelect.tsx
 */
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Button, { ButtonProps } from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& .MuiTypography-body1': {
                color: 'inherit'
            }
        },
        hover: {
            '&:hover': {
                backgroundColor: theme.palette.primary.light,
            },
        },
        selected: {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.light,

            '&:hover': {
                color: theme.palette.common.white,
                backgroundColor: theme.palette.primary.main,
            },
        },
    })
);


interface DateRangeButtonProps extends ButtonProps {
    range: number | string,
    text: string,
    isSelected: boolean,
    onClick: (range: any) => void,
};

const DateRangeButton = (props: DateRangeButtonProps) => {
    const classes = useStyles();

    const clickHandler = () => {
        props.onClick(props.range)
    }

    const { onClick, className, isSelected, ...rest } = props;
    const toggledClass = props.isSelected
        ? clsx(className, classes.root, classes.hover, classes.selected)
        : clsx(className, classes.root, classes.hover);
    return (
        <Button type='button'
                className={toggledClass}
                onClick={clickHandler}
                {...rest}>
            <Typography>{props.text}</Typography>
        </Button>
    )
};

export default DateRangeButton;
