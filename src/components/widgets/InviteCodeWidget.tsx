/* 
 * src/components/widgets/InviteCodeWidget.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formGroup: {
            justifyContent: 'center'
        },
        spin: {
            animation: '$spin 1s 1'
        },
        '@keyframes spin': {
            '0%': {
                transform: 'rotate(0deg)'
            },
            '100%': {
                transform: 'rotate(360deg)'
            }
        }
    }),
);

interface InviteCodeWidgetProps {
    code: string;
    codeIsRefreshing: boolean;
    refreshCode: () => void;
}

const InviteCodeWidget = (props: InviteCodeWidgetProps) => {
    const classes = useStyles();

    return (
        <FormGroup className={classes.formGroup} row>
            <FormControl>
                <TextField
                    id='standard-basic'
                    label='Invitation Code'
                    placeholder='Code'
                    value={props.code}
                    InputProps={{
                        readOnly: true,
                    }}
                    InputLabelProps={{
                        shrink: true
                    }}                    
                />
            </FormControl>

            <IconButton
                aria-label='refresh'
                color='primary'
                onClick={props.refreshCode}
            >
                <AutorenewIcon
                    className={props.codeIsRefreshing ? classes.spin : undefined}
                />
            </IconButton>            
        </FormGroup>
    );
};

export default InviteCodeWidget;
