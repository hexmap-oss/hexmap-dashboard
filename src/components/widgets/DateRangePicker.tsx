/* 
 * src/components/widgets/DateRangeInput.tsx
 */
import clsx from 'clsx';
import { useState, useEffect } from 'react';
import moment, { Moment } from 'moment';
import { DateRangePicker as AirbnbDateRangePicker, FocusedInputShape, isInclusivelyBeforeDay } from 'react-dates';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
//-- New way to import styles
// import WithStylesContext from 'react-with-styles/lib/WithStylesContext';
// import AphroditeInterface from 'react-with-styles-interface-aphrodite';
// import ReactDatesDefaultTheme from 'react-dates/lib/theme/DefaultTheme';
//-- Old way to import styles
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';


type PickerDateType = Moment | null;
type FocusedInputType = FocusedInputShape | null;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        // override library styles
        root: {
            '& div.DateRangePicker': {
                backgroundColor: 'inherit',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
                borderBottomStyle: 'solid',
            },
            '& .DateRangePickerInput': {
                backgroundColor: 'inherit',
            },
            '& .DateInput': {
                background: 'none',
            },
            '& input': {
                background: 'none',
            },
            '& .DateRangePickerInput__withBorder': {
                border: 'none'
            },
            '& .DateInput_input__focused': {
                borderBottom: 'none',
            },
            // Will edit everything selected including everything between a range of dates
            '& .CalendarDay__selected_span': {
                background: theme.palette.secondary.light, //background
                color: theme.palette.secondary.dark, //text
                borderWidth: 1,
                borderStyle: 'solid',
                borderColor: theme.palette.grey[200]
            },
            // // Will edit selected date or the endpoints of a range of dates
            // '& .CalendarDay__selected': {
            //     background: theme.palette.primary.main,
            //     color: theme.palette.common.white,
            //     borderWidth: 1,
            //     borderStyle: 'solid',
            //     borderColor: theme.palette.grey[200]                
            // },
            // // Will edit when hovered over. _span style also has this property
            // '& .CalendarDay__selected:hover': {
            //     background: theme.palette.secondary.dark,
            //     color: theme.palette.common.white,
            // },
            // // Will edit when the second date (end date) in a range of dates
            // // is not yet selected. Edits the dates between your mouse and said date
            // '& .CalendarDay__hovered_span:hover': {
            //     background: theme.palette.secondary.light,
            // },
            // '& .CalendarDay__hovered_span': {
            //     background: theme.palette.primary.light,
            // }
        },
        centerCalendar: {
            textAlign: 'center',
            '& .DateRangePicker_picker': {
                left: '-168px !important'
            },
        }
    }),
);

interface DateRangeInputProps {
    minDate: number | null;
    maxDate: number | null;
    startDate: number | null;
    endDate: number | null;
    setDates: (startDate: number, endDate: number) => void;
    center?: boolean;
};

const DateRangeInput = (props: DateRangeInputProps) => {
    const classes = useStyles();
    const [focusedInput, setFocusedInput] = useState<FocusedInputType>(null);

    useEffect(() => {}, [props.minDate]);

    const handleDateRangeChange = (arg: {startDate: PickerDateType, endDate: PickerDateType}) => {
        const startUnixTime = arg.startDate ? arg.startDate.valueOf() : startDate!.valueOf();
        const endUnixTime = arg.endDate ? arg.endDate.valueOf() : endDate!.valueOf();
        props.setDates(startUnixTime, endUnixTime);
    }

    const handleFocusChange = (arg: FocusedInputType) => {
        setFocusedInput(arg);
    }

    const minDate = props.minDate ? moment(props.minDate) : undefined;
    const maxDate = props.maxDate ? moment(props.maxDate) : undefined;
    const startDate = props.startDate ? moment(props.startDate) : null;
    const endDate = props.endDate ? moment(props.endDate) : null;
    return (
        <Grid>
            <div className={ props.center
                ? clsx(classes.root, classes.centerCalendar)
                : classes.root}
            >
                <AirbnbDateRangePicker
                    minDate={minDate}
                    maxDate={maxDate}
                    startDate={startDate}
                    startDateId='date-picker-start-date'
                    endDate={endDate}
                    endDateId='date-picker-end-date'
                    hideKeyboardShortcutsPanel={true}
                    isOutsideRange={day => !isInclusivelyBeforeDay(day, moment())}
                    onDatesChange={handleDateRangeChange}
                    focusedInput={focusedInput}
                    onFocusChange={handleFocusChange}
                    keepOpenOnDateSelect
                />
            </div>
        </Grid>
    );
}


export default DateRangeInput;
