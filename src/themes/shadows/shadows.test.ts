// https://gist.github.com/phacks/6c3c3a5f395f6e9660ae132c237250a3

import createShadows, { hexToRgb } from './shadows';

describe('[config/shadows] hexToRgb', () => {
  it('should convert an hex color to {r, g, b}', () => {
    const testSuite = [
      { hex: '#000000', expectedRgb: { r: 0, g: 0, b: 0 } },
      { hex: '#000', expectedRgb: { r: 0, g: 0, b: 0 } },
      { hex: '#FFF', expectedRgb: { r: 255, g: 255, b: 255 } },
      { hex: '#A13FB2', expectedRgb: { r: 161, g: 63, b: 178 } },
    ];

    testSuite.map(singleCase =>
      expect(hexToRgb(singleCase.hex)).toEqual(singleCase.expectedRgb),
    );
  });
});

describe('[config/shadows] createShadows', () => {
  it('should return an array of 25 box-shadow values', () => {
    expect(createShadows('#A13FB2').length).toEqual(25);
    expect(createShadows('#A13FB2')[0]).toEqual('none');
    expect(createShadows('#A13FB2')[1]).toEqual(
      '0px 1px 3px 0px rgba(161,63,178,0.2),0px 1px 1px 0px rgba(161,63,178,0.14),0px 2px 1px -1px rgba(161,63,178,0.12)',
    );
  });
});
