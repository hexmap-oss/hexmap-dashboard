/* 
 * src/themes/base.ts
*/
import { createStyles, Theme } from '@material-ui/core';
import { darkTheme } from './darkTheme';

// constants
export const SIDEBAR_WIDTH = 65;
export const DRAWER_WIDTH = 240;

// return selected themes
export function getThemeByName(theme: string): Theme {
    return themeMap[theme];
}

const themeMap: { [key: string]: Theme } = {
    darkTheme
};


// base theme
function BaseAppTheme(theme: Theme) {
    return createStyles({
        root: {
            display: 'flex'
        },
        drawer: {
            width: DRAWER_WIDTH,
            flexShrink: 0,
            whiteSpace: 'nowrap',
        },
        drawerOpen: {
            width: DRAWER_WIDTH,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
            overflowX: 'hidden'
        },
        drawerClose: {
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            overflowX: 'hidden',
            width: theme.spacing(7) + 1,
            [theme.breakpoints.up('sm')]: {
                width: theme.spacing(8) + 1,
            },		
        },
        drawerFooter: {
            position: 'absolute',
            bottom: 0,
            width: DRAWER_WIDTH,
        },
        sidebarItemText: {
            color: theme.palette.primary.main,
        },
        toolbar: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
        },
        content: {
            top: 0,
            left: 0,
            right: 0,         
            flexGrow: 1,
            marginTop: '65px',
            width: `calc(100% - ${SIDEBAR_WIDTH}px)`,
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            })          
        },
        contentShift: {
            width: `calc(100% - ${DRAWER_WIDTH}px)`,
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            })    
        }
    });
}

export default BaseAppTheme;
