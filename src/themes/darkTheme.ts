/* 
 * src/themes/darkTheme.ts
 */
import { unstable_createMuiStrictModeTheme as createTheme } from '@material-ui/core';
import createShadows from './shadows/shadows';
import '@fontsource/lato';
import '@fontsource/roboto-mono';

const DARK_THEME_COLORS = {
    PrimaryLight: '#f2f3ff',
    PrimaryMain: '#535672',
    PrimaryDark: '#00011f',
    SecondaryLight: '#b8e4d2',
    SecondaryMain: '#4ec299',
    SecondaryDark: '#007749',
}

export const darkTheme = createTheme({
    palette: {
        primary: {
            light: DARK_THEME_COLORS.PrimaryLight, 
            main: DARK_THEME_COLORS.PrimaryMain,
            dark: DARK_THEME_COLORS.PrimaryDark,
        },
        secondary: {
            light: DARK_THEME_COLORS.SecondaryLight, 
            main: DARK_THEME_COLORS.SecondaryMain,
            dark: DARK_THEME_COLORS.SecondaryDark,
            contrastText: 'white',
        }        
    },
    overrides: {
        MuiTypography: {
            h6: {
                fontWeight: 700
            }
        }
    },
    shadows: createShadows('#BBB'),
    typography: {
        fontFamily: [
            'Lato',
            // '-apple-system',
            // 'BlinkMacSystemFont',
            // '"Segoe UI"',
            // 'Roboto',
            'Roboto Mono',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            // '"Apple Color Emoji"',
            // '"Segoe UI Emoji"',
            // '"Segoe UI Symbol"',
        ].join(','),
        allVariants: {
            color: '#333'
        }
    }     
});

