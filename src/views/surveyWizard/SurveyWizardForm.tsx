/* 
 * src/views/surveyWizard/SurveyWizardForm.tsx
 */
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';

import { saveSurveySchema, selectIsActive, selectQuestions } from './surveyWizardSlice';
import QuestionStack from '../../components/questionBuilder/QuestionStack';
import {
    addQuestion,
    deleteQuestion,
    moveQuestion,
    saveQuestion,
    fetchSurveySchema,
} from './surveyWizardSlice';
import { useEffect } from 'react';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: 'auto',
            paddingLeft: '5%',
            paddingRight: '5%',
            maxWidth: 760,
        },
        saveButton: {
            textAlign: 'center',
            paddingBottom: 48,
            paddingTop: 48,
        },
        saveButtonHelpText: {
            marginTop: 8,
            color: theme.palette.grey[700],
            fontSize: 14
        }
    }),
);

const SurveyWizardForm = () => {
    const classes = useStyles();
    const questions = useSelector(selectQuestions);
    const isActive = useSelector(selectIsActive);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchSurveySchema());
    }, [dispatch]);

    const handleSaveQuestionSchema = () => {
        dispatch(saveSurveySchema(questions));
    }

    return (
        <Box className={classes.root}>
            <Box flexGrow={1} p={1}>
                <QuestionStack
                    questions={questions}
                    addQuestion={addQuestion}
                    deleteQuestion={deleteQuestion}
                    moveQuestion={moveQuestion}
                    saveQuestion={saveQuestion}
                    disabled={isActive}
                />

                <Box className={classes.saveButton}>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={handleSaveQuestionSchema}
                        startIcon={<SaveIcon />}
                        disabled={isActive}
                    >
                        Save Survey
                    </Button>

                    { isActive
                        ? <Typography className={classes.saveButtonHelpText}>Save disabled, the survey is active.</Typography>
                        : undefined
                    }                    
                </Box>
            </Box>
        </Box>
    );
}

export default SurveyWizardForm;