/* 
 * src/views/surveyWizard/surveyWizardSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import update from 'immutability-helper';

import { RootState } from '../../app/store';
import SurveyService, { SurveyQuestionJSON, SurveySchemaParameters, SurveySchemaResponse } from '../../api/survey';
import {
    CheckboxOption,
    DropdownOption,
    JSONQuestionTypeLookup,
    MoveStackQuestion,
    QuestionTypes,
    StackQuestion
} from '../../components/questionBuilder/questionBuilderTypes';
import { isDebugMode } from '../../app/utils';


interface SurveyWizardState {
    questions: StackQuestion[];
    isActive: boolean;
    isSaving: boolean;
};


const testInitialState: SurveyWizardState = {
    questions: [
        {
            id: 1,
            type: QuestionTypes.dropdown,
            title: 'Age',
            fieldName: 'age',
            options: ['10', '20', '30'] as DropdownOption[]
        },
        {
            id: 2,
            type: QuestionTypes.checkbox,
            title: 'Favorite Food',
            fieldName: 'favorite-food',
            options: ['Hot dog', 'Clams', 'Donut'] as CheckboxOption[]
        },
        {id: 3, type: QuestionTypes.number, title: 'Dogs Owned', fieldName: 'dogs-owned'},
        {id: 4, type: QuestionTypes.address, title: 'Pick a Cool Place', fieldName: 'cool-place'},
        {id: 5, type: QuestionTypes.textBox, title: 'Write a Poem', fieldName: 'poem'}
    ],
    isActive: false,
    isSaving: false,
};

const initialState: SurveyWizardState = {
    questions: [] as StackQuestion[],
    isActive: false,
    isSaving: false,
};


export const fetchSurveySchema = createAsyncThunk(
    'surveyWizard/fetchSchema',
    async(_, thunkAPI) => {
        try {
            const response = await SurveyService.fetchSurveySchema();
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const saveSurveySchema = createAsyncThunk(
    'surveyWizard/saveSchema',
    async(questions: StackQuestion[], thunkAPI) => {
        try {
            // reduce stack object to JSON schema of survey
            const jsonQuestions: SurveyQuestionJSON[] = questions.map(question => {
                const fields = question.options
                    ? { choices: question.options }
                    : {}
                const questionTypeId = question.hardcodedId
                    ? question.hardcodedId
                    : Object.values(QuestionTypes).indexOf(question.type) + 1;

                return {
                    id: questionTypeId,
                    prompt: question.title,
                    colName: question.fieldName,
                    fields: fields,
                    answerRequired: true
                }
            });

            const schemaParams: SurveySchemaParameters = {
                language: 'en',
                aboutText: '',
                termsOfService: '',
                questions: jsonQuestions,
            };
            const response = await SurveyService.saveSurveySchema(schemaParams);
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const surveyWizardSlice = createSlice({
    name: 'surveyWizard',
    initialState: isDebugMode() ? testInitialState : initialState,
    reducers: {
        addQuestion: (state, action: PayloadAction<StackQuestion>) => {
            const newQuestions = [...state.questions, action.payload];
            state.questions = newQuestions;
        },
        deleteQuestion: (state, action: PayloadAction<number>) => {
            const newQuestions = [...state.questions];
            newQuestions.splice(action.payload, 1);
            state.questions = newQuestions;
        },
        moveQuestion: (state, action: PayloadAction<MoveStackQuestion>) => {
            const question = state.questions[action.payload.fromIndex];
            state.questions = update(state.questions, {
                $splice: [
                    [action.payload.fromIndex, 1],
                    [action.payload.toIndex, 0, question],
                ]
            });      
        },
        saveQuestion: (state, action: PayloadAction<StackQuestion>) => {
            console.log(action.payload);
            const newQuestion = action.payload;
            state.questions = state.questions.map(existingQuestion => {
                if (existingQuestion.id === action.payload.id) {
                    return newQuestion;
                }
                return existingQuestion;
            });
        },
    },
    extraReducers: builder => builder
        .addCase(fetchSurveySchema.pending, (state, action) => {})
        .addCase(fetchSurveySchema.fulfilled, (state, action: PayloadAction<SurveySchemaResponse>) => {
            state.questions = action.payload.survey.map((question: SurveyQuestionJSON, index: number) => {
                // map the string value to enum key (if exists) or use an integer
                // to lookup up enum key from intermediary JSON map object (deprecated)
                const questionType = question.type
                    ? QuestionTypes[question.type as keyof typeof QuestionTypes]
                    : JSONQuestionTypeLookup[question.id];

                const optionsTypes = [QuestionTypes.checkbox, QuestionTypes.dropdown];
                return  {
                    id: index + 1,
                    type: questionType,
                    title: question.prompt,
                    fieldName: question.colName,
                    options: optionsTypes.includes(questionType)
                        ? question.fields.choices
                        : undefined,
                    hardcodedId: question.id >= 100 ? question.id : undefined
                };
            })

            state.isActive = action.payload.started;
        })
        .addCase(saveSurveySchema.pending, (state, action) => {
            state.isSaving = true;
        })
        .addCase(saveSurveySchema.fulfilled, (state, action) => {
            state.isSaving = false;

        })
        .addCase(saveSurveySchema.rejected, (state, action) => {
            state.isSaving = false;
        })                
});

export const { addQuestion, deleteQuestion, moveQuestion, saveQuestion } = surveyWizardSlice.actions;

export const selectQuestions = (state: RootState) => state.surveyWizard.questions;
export const selectIsActive = (state: RootState) => state.surveyWizard.isActive;

export default surveyWizardSlice.reducer;
