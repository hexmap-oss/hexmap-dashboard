/* 
 * src/view/surveyWizard/SurveyWizardHeader.tsx
 */
import { useSelector } from 'react-redux';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import CommentOutlinedIcon from '@material-ui/icons/CommentOutlined';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import RoomIcon from '@material-ui/icons/Room';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { mdiNumeric } from '@mdi/js';

import { selectIsActive } from './surveyWizardSlice';
import { FormTypes } from '../../components/questionBuilder/questionBuilderTypes';
import DraggableButton from '../../components/questionBuilder/DraggableButton';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 16,
        },
        buttonRow: {
            display: 'flex',
        },
        button: {
            margin: 5,
            paddingRight: 12,
            paddingLeft: 12,
            fontSize: 13,
            fontWeight: 'bold',

            '&:hover': {
                backgroundColor: theme.palette.primary.dark
            },

            '&:disabled': {
                backgroundColor: theme.palette.primary.light
            }
        },
        buttonIcon: {
            fontSize: 13,
            marginRight: 5,
        }
    }),
);


const SurveyWizardHeader = () => {
    const classes = useStyles();
    const isActive = useSelector(selectIsActive);

    return (
        <Paper elevation={1} className={classes.root}>
            <Typography variant='body1'>
                Generate the demographic survey for users of the mobile users below. Drag components
                from the selections above to the survey stack below. Age, gender, email and home/work
                location prompts are mandatory and cannot be deleted.                
            </Typography>

            <Box className={classes.buttonRow}>
                <DraggableButton
                    classes={classes}
                    text={'Dropdown'}
                    Icon={KeyboardArrowDownIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'dropdown'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}             
                    text={'Checkboxes'}
                    Icon={CheckBoxOutlinedIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'checkbox'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}            
                    text={'Number'}
                    MdiIcon={mdiNumeric}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'number'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}
                    text={'Address'}
                    Icon={RoomIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'address'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}                    
                    text={'Text Box'}
                    Icon={CommentOutlinedIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'textBox'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}         
                    text={'Time'}
                    Icon={ScheduleIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'number'
                    }}
                    isDisabled={true}
                />
            </Box>                                                           
        </Paper>
    );
}

export default SurveyWizardHeader;
