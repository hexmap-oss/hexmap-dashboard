/* 
 * src/views/userSettings/UserSettingsCard.tsx
 */
import { useState } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import DeleteSweepIcon from '@material-ui/icons/DeleteSweep';
import DeleteAccountWarningDialog from './DeleteAccountWarningDialog';


export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        container: {
            paddingTop: theme.spacing(3),
            justifyContent: 'center',
            textAlign: 'center'
        },
        buttonContainer: {
            marginTop: theme.spacing(3),
        },
        deleteButton: {
            backgroundColor: theme.palette.error.light,
            color: theme.palette.grey[100],
            borderColor: theme.palette.error.main,
            borderStyle: 'solid',
            borderWidth: 1,
            fontWeight: 600,

            '&:hover': {
                backgroundColor: theme.palette.error.main,
                color: theme.palette.common.white,
            }
        },
    })
);



const UserSettingsCard = () => {
    const classes = useStyles();
    const [deleteAccountWarningOpen, setDeleteAccountWarningOpen] = useState(false);
    const surveyName = 'test';

    const handleDeleteAccount = () => {
        alert('TODO: Deleting account...');
    }

    return (
        <>
        <Card className={classes.root} elevation={0}>
            <CardContent>
                <Grid container className={classes.container}>
                    <Grid item sm={12}>
                        <Typography variant='h6'>Delete User Account</Typography>
                        <Typography variant='subtitle2'>Permanently remove your user account from your organization's dashboard.</Typography>

                        <Box className={classes.buttonContainer}>
                            <Button
                                className={classes.deleteButton}
                                startIcon={<DeleteSweepIcon />}
                                onClick={() => setDeleteAccountWarningOpen(true)}
                            >
                                Delete account
                            </Button>
                        </Box>
                    </Grid>                    
                </Grid>    
            </CardContent>
        </Card>

        <DeleteAccountWarningDialog
            deleteUserAccount={handleDeleteAccount}
            isOpen={deleteAccountWarningOpen}
            setOpen={setDeleteAccountWarningOpen}
            surveyName={surveyName}
        />
        </>
    );
};

export default UserSettingsCard;
