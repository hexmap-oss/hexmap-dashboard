/* 
 * src/views/userSettings/UserResetPasswordCard.tsx
 */
import clsx from 'clsx';
import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        container: {
            paddingTop: theme.spacing(3),
            justifyContent: 'center',
            textAlign: 'center'
        },
        formContainer: {
            width: '70%',
            margin: 'auto',
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2),            
        },
        inputFieldContainer: {
            '& .MuiFormControlLabel-label': {
                marginRight: theme.spacing(2),
            }
        },
        inputField: {
            width: 300,
        },
        fieldBottomMargin: {
            marginBottom: theme.spacing(2),
        },
        submitButtonContainer: {
            width: '100%',
        },
        submitHelperText: {
            marginTop: 10,
            textAlign: 'right',
            height: 30,
        },        
        submitButton: {
            width: 200,
        }
    })
);


interface ValidateErrors {
    currentPassword?: boolean;
    newPassword?: boolean;
    confirmNewPassword?: boolean;    
};

const UserResetPasswordCard = () => {
    const classes = useStyles();
    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [errors, setErrors] = useState<ValidateErrors>({});
    const [helperText, setHelperText] = useState('');


    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        validateResetPassword();
    };

    const validateResetPassword = () => {
        const errors = {
            currentPassword: !currentPassword,
            newPassword: !newPassword,
            confirmNewPassword: !confirmNewPassword,
        } as ValidateErrors;

        if (!currentPassword) {
            setHelperText('Enter current password to continue.');
        } else if (
            (!!newPassword && !!confirmNewPassword) &&
            (newPassword !== confirmNewPassword)) {
            errors.newPassword = true;
            errors.confirmNewPassword = true;
            setHelperText('Entered passwords do not match.');
        }
        setErrors(errors);
    };

    return (
        <Card elevation={0}>
            <CardContent>
                <Grid container className={classes.container}>
                <Typography variant='h6'>Edit password</Typography>
                    <Grid item sm={12}>
                        <Box className={classes.formContainer}>
                            <form onSubmit={handleSubmit}>
                                <FormControl component='fieldset' error={true}>
                                    <FormGroup>
                                        <FormControlLabel
                                            className={clsx(classes.inputFieldContainer, classes.fieldBottomMargin)}
                                            label='Enter your current password:'
                                            labelPlacement='start'
                                            control={
                                                <TextField
                                                    className={classes.inputField}
                                                    id='current-password'
                                                    placeholder='Current password'
                                                    value={currentPassword}
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setCurrentPassword(e.target.value)}
                                                    type='password'
                                                    error={errors.currentPassword}
                                                />
                                            }
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <FormControlLabel
                                            className={classes.inputFieldContainer}
                                            label='Enter new password:'
                                            labelPlacement='start'
                                            control={
                                                <TextField
                                                    className={classes.inputField}
                                                    id='new-password'
                                                    placeholder='New password'
                                                    value={newPassword}
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setNewPassword(e.target.value)}
                                                    type='password'
                                                    error={errors.newPassword}
                                                />
                                            }
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <FormControlLabel
                                            className={clsx(classes.inputFieldContainer)}
                                            label='Confirm new password:'
                                            labelPlacement='start'
                                            control={
                                                <TextField
                                                    className={classes.inputField}
                                                    id='confirm-password'
                                                    placeholder='New password'
                                                    value={confirmNewPassword}
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setConfirmNewPassword(e.target.value)}
                                                    type='password'
                                                    error={errors.confirmNewPassword}
                                                />
                                            }
                                        />
                                    </FormGroup>                                

                                    <Box className={classes.submitButtonContainer}>
                                        <FormHelperText className={classes.submitHelperText} error>
                                            {helperText}
                                        </FormHelperText>

                                        <Button
                                            className={classes.submitButton}
                                            type='submit'
                                            variant='outlined'
                                            color='primary'
                                        >
                                            Update Password
                                        </Button>
                                    </Box>
                                </FormControl>
                            </form>
                        </Box>
                    </Grid>                    
                </Grid>
            </CardContent>
        </Card>
    );
};

export default UserResetPasswordCard;
