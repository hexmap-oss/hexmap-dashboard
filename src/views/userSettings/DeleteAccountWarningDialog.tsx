/* 
 * src/views/userSettings/DeleteAccountWarningDialog.tsx
 */
import { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        deleteButton: {
            color: theme.palette.error.main,
        },
        declineButton: {
            color: theme.palette.info.main,
        },
    })
);

interface DeleteAccountWarningDialogProps {
    isOpen: boolean;
    surveyName: string;

    deleteUserAccount: () => void;
    setOpen: (isOpen: boolean) => void;
};

const DeleteAccountWarningDialog = ({isOpen, surveyName, deleteUserAccount, setOpen}: DeleteAccountWarningDialogProps) => {
    const classes = useStyles();
    const [typedSurveyName, setTypedSurveyName] = useState('');
    const [error, setError] = useState(false);

    const handleDeleteAccount = () => {
        if (typedSurveyName !== surveyName) {
            setError(true);
        } else {
            deleteUserAccount();
            handleDismiss();
        }
    }

    const handleDismiss = () => {
        setError(false);
        setOpen(false);
    };

    return (
        <div>
            <Dialog
                open={isOpen}
                onClose={handleDismiss}
                aria-labelledby='alert-dialog-title'
                aria-describedby='alert-dialog-description'
            >
                <DialogTitle id='alert-dialog-title'>
                    {'Delete account'}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id='alert-dialog-description'>
                        {`Caution: this will delete your user account and remove your access to the dashboard.
                          Please enter your survey name below to confirm.`}
                    </DialogContentText>

                    <TextField
                        autoFocus
                        margin='dense'
                        id='survey-name'
                        label='Survey Name'
                        type='text'
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setTypedSurveyName(e.target.value)}
                        helperText={error ? 'Survey name is incorrect' : undefined}
                        error={error}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button className={classes.deleteButton} onClick={handleDeleteAccount} autoFocus>
                        Delete My Account
                    </Button>
                    <Button className={classes.declineButton} onClick={handleDismiss} autoFocus>
                        Take Me Back
                    </Button>                    
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default DeleteAccountWarningDialog;
