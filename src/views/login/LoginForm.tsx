/*
 * src/views/login/LoginForm.tsx
 */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, lighten, Theme } from '@material-ui/core/styles';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

import { loginUser } from './loginSlice';
import AccessSelect from './accessSelect';
import { 
    clearState,
    selectError,
    selectIsLoading
} from './loginSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            borderRadius: 10,
            backgroundColor: 'rgba(255, 255, 255, 0.06)',
            marginTop: 30,
            padding: theme.spacing(3),
            position: 'relative',
        },
        inputField: {
            '& .MuiInput-root': {
                color: theme.palette.grey[400],
            },
            '& .MuiInput-root.Mui-focused': {
                color: theme.palette.grey[100],
            },
            '& .MuiInput-underline': {
                borderBottomWidth: 1,
                borderBottomStyle: 'solid',
                borderBottomColor: 'rgba(255, 255, 255, 0.5)'
            },
            '& .MuiInput-underline.Mui-focused': {
                borderBottomWidth: 0,
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
        },
        inputLabel: {
            color: theme.palette.grey[400],
            '&.Mui-focused': {
                color: theme.palette.grey[400],
            }
        },
        forgotPasswordText: {
            color: theme.palette.grey[300],
            float: 'right',
            fontSize: 13
        },
        loginButton: {
            backgroundColor: theme.palette.primary.main,
            '&:hover': {
                backgroundColor: lighten(theme.palette.primary.main, 0.05)
            }
        },
        progressBar: {
            position: 'absolute',
            bottom: 0,
            width: '395px',
            opacity: 0.1
        },
        helperTextContainer: {
            alignItems: 'center',
            color: theme.palette.primary.light,
            display: 'flex',
            paddingLeft: 20,
            paddingTop: 10,
            height: 16
        },        
        helperText: {
            color: theme.palette.primary.light,
            marginLeft: 10,
            fontSize: 14,
        }      
    })
);

const LoginForm = () => {
    const classes = useStyles();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const loginIsLoading = useSelector(selectIsLoading);
    const loginError = useSelector(selectError);
    const dispatch = useDispatch();

    useEffect(() => {
        return () => {
            dispatch(clearState());
        };
    }, [dispatch]);

    const handleLogin = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        dispatch(loginUser({email, password}));
    };

    return (
        <>
        <Container className={classes.container} maxWidth='xs'>
            <form onSubmit={handleLogin}>
                <Grid container spacing={3}>
                <Grid item xs={3}>
                    <AccessSelect />
                </Grid>
                    <Grid item xs={9}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    className={classes.inputField}
                                    label='Email'
                                    name='email'
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)}
                                    size='small'
                                    value={email}
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.inputLabel
                                        }
                                    }}
                                    InputProps={{
                                        className: classes.inputField
                                    }}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    className={classes.inputField}
                                    label='Password'
                                    name='password'
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)}
                                    size='small'
                                    type='password'
                                    value={password}
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.inputLabel
                                        }
                                    }}
                                    InputProps={{
                                        className: classes.inputField
                                    }}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Link to={'/login/reset-password'}>
                                    <Typography className={classes.forgotPasswordText}>
                                        Forgot password?
                                    </Typography>
                                </Link>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={9}>
                        <Button
                            className={classes.loginButton}
                            color='primary'
                            type='submit'
                            variant='contained'
                            fullWidth
                        >
                            Log in
                        </Button>
                    </Grid>
                </Grid>
            </form>

            { loginIsLoading
                ?   <LinearProgress
                        className={classes.progressBar}
                        color='primary'
                    />
                : undefined
            }
        </Container>

        <Box className={classes.helperTextContainer}>
        { loginError
            ? (
                <>
                <ErrorOutlineIcon fontSize={'small'} />
                <Typography className={classes.helperText}>
                    Email or password could not be found, please try again.
                </Typography>
                </>
            )
            : undefined
        }
        </Box>
        </>        
    );
}

export default LoginForm;
