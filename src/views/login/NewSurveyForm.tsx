/*
 * src/views/login/NewSurveyForm.tsx
 */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            borderRadius: 10,
            backgroundColor: 'rgba(255, 255, 255, 0.06)',
            marginTop: 30,
            padding: theme.spacing(3),
            minWidth: 450
        },
        title: {
            color: theme.palette.common.white,
            paddingBottom: 10,
        },
        inputField: {
            '& .MuiInput-root': {
                color: theme.palette.grey[400],
            },
            '& .MuiInput-root.Mui-focused': {
                color: theme.palette.grey[100],
            },
            '& .MuiInput-underline': {
                borderBottomWidth: 1,
                borderBottomStyle: 'solid',
                borderBottomColor: 'rgba(255, 255, 255, 0.5)'
            },
            '& .MuiInput-underline.Mui-focused': {
                borderBottomWidth: 0,
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
        },
        inputLabel: {
            color: theme.palette.grey[400],
            '&.Mui-focused': {
                color: theme.palette.grey[400],
            }
        },
        submitButton: {
            color: theme.palette.info.main,
            float: 'right',
        },
        helperTextContainer: {
            alignItems: 'center',
            color: theme.palette.primary.light,
            display: 'flex',
            paddingLeft: 20,
            paddingTop: 10
        },
        helperText: {
            color: theme.palette.primary.light,
            marginLeft: 10,
            fontSize: 14,
        },
        helperSubtext: {
            color: theme.palette.primary.light,
            fontSize: 14,
        },        
        link: {
            '&:any-link': {
                color: theme.palette.info.light
            }
        }
    })
);

const NewSurveyForm = () => {
    const classes = useStyles();
    const [email, setEmail] = useState('');
    const [helperText, setHelperText] = useState(false);
    
    const handleResetPassword = (event: React.FormEvent<HTMLFormElement> | React.MouseEvent) => {
        event.preventDefault();
        setHelperText(true);
    }

    return (
        <>
        <Container className={classes.container} maxWidth='xs'>
            <form onSubmit={handleResetPassword}>
                <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <Typography className={classes.title} variant='h6'>Reset password</Typography>
                        <TextField
                            className={classes.inputField}
                            label='Enter your email'
                            name='email'
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)}
                            size='small'
                            value={email}
                            InputLabelProps={{
                                classes: {
                                    root: classes.inputLabel
                                }
                            }}
                            InputProps={{
                                className: classes.inputField
                            }}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button onClick={handleResetPassword} className={classes.submitButton}>
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Container>

        { helperText
            ? (
                <>
                <Box className={classes.helperTextContainer}>
                    <ErrorOutlineIcon fontSize={'small'} />
                    <Typography className={classes.helperText}>
                        A password reset email has been sent to this address if an account exists.
                    </Typography>
                </Box>
                <Typography className={classes.helperSubtext}>
                    Click&nbsp;<Link className={classes.link} to='/login'>here</Link> to return.
                </Typography>
                </>
            )
            : undefined
        }
        </>
    );
}

export default NewSurveyForm;
