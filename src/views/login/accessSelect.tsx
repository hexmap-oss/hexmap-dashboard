/*
 * src/views/login/accessSelect.tsx
 */
import clsx from 'clsx';
import { useState } from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import GroupIcon from '@material-ui/icons/Group';
import PersonIcon from '@material-ui/icons/Person';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            justifyContent: 'space-around',
            color: theme.palette.common.white,
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
        },
        button: {
            color: theme.palette.common.white,

            '&:disabled': {
                color: theme.palette.grey[800]
            }
        },
        iconButtonLabel: {
            display: 'flex',
            flexDirection: 'column',
            fontSize: 8,
        },
        active: {
            color: theme.palette.info.main
        }
    })
);


const AccessSelect = () => {
    const classes = useStyles();
    const [organizationSelected, setOrganizationSelected] = useState(true);

    return (
        <Box className={classes.container}>
            <IconButton
                className={organizationSelected
                    ? classes.button
                    : clsx(classes.button, classes.active)
                }
                classes={{label: classes.iconButtonLabel}}
                onClick={() => setOrganizationSelected(false)}
                disabled
            >
                <PersonIcon />

                <Box>
                    User
                </Box>                
            </IconButton>

            <IconButton
                className={organizationSelected
                    ? clsx(classes.button, classes.active)
                    : classes.button
                }
                classes={{label: classes.iconButtonLabel}}
                onClick={() => setOrganizationSelected(true)}
            >
                <GroupIcon />

                <Box>
                    Organization
                </Box>                
            </IconButton>
        </Box>
    );
}

export default AccessSelect;
