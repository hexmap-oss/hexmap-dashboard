/* 
 * src/views/login/loginSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

import { LoginRequest } from './loginTypes';
import AuthService from '../../api/auth';
import { User } from '../../app/commonTypes';


interface LoginState {
    isAuthorized: boolean;
    isLoading: boolean;
    isLoadingJWT: boolean;
    error: boolean;
    user?: User;
};


const testInitialState: LoginState = {
    isLoading: false,
    isLoadingJWT: false,
    isAuthorized: false,
    error: false,
    user: undefined,
};

const initialState: LoginState = {
    isLoading: false,
    isLoadingJWT: false,
    isAuthorized: false,
    error: false,
    user: undefined,
};

export const loginUser = createAsyncThunk(
    'user/login',
    async ({email, password}: LoginRequest, thunkAPI) => {
        try {
            return await AuthService.login(email, password) as User;
        } catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
);

export const initJWT = createAsyncThunk(
    'user/init',
    async (_, thunkAPI) => {
        try {
            return await AuthService.init();
        } catch (error: any) {
            console.log(error.message);
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const loginSlice = createSlice({
    name: 'login',
    initialState: initialState,
    reducers: {
        clearState: state => {
            state = testInitialState;
        },
        logoutUser: state => {
            return state = {
                isLoading: false,
                isLoadingJWT: false,
                isAuthorized: false,
                error: false,
            };
        }
    },
    extraReducers: builder => builder
        .addCase(loginUser.pending, (state) => {
            state.isLoading = true;
        })
        .addCase(loginUser.fulfilled, (state, action: PayloadAction<User>) => {
            state.isLoading = false;
            state.isAuthorized = true;
            state.user = action.payload;
        })
        .addCase(loginUser.rejected, (state) => {
            state.isLoading = false;
            state.isAuthorized = false;
            state.error = true;
        })

        .addCase(initJWT.pending, (state) => {
            state.isLoadingJWT = true;
        })
        .addCase(initJWT.fulfilled, (state, action: PayloadAction<User>) => {
            state.isLoadingJWT = false;
            state.isAuthorized = true;
            state.user = action.payload;
        })
        .addCase(initJWT.rejected, (state) => {
            state.isLoadingJWT = false;
            state.isAuthorized = false;
        })
});


export const { clearState, logoutUser } = loginSlice.actions;

export const selectError = (state: RootState) => state.login.error;
export const selectIsLoading = (state: RootState) => state.login.isLoading;
export const selectIsLoadingJWT = (state: RootState) => state.login.isLoadingJWT;
export const selectIsAuthorized = (state: RootState) => state.login.isAuthorized;
export const selectUserAccessToken = (state: RootState) => state.login.user ? state.login.user.AccessToken : null;

export default loginSlice.reducer;
