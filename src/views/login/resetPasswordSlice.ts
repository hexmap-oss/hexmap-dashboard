/* 
 * src/views/login/resetPasswordSlice.ts
 */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import AuthService from '../../api/auth';
import { RootState } from '../../app/store';
import { UserPasswordUpdateRequest } from './loginTypes';


interface ResetPasswordState {
    showHelperText: Boolean;
};

const testInitialState: ResetPasswordState = {
    showHelperText: false,
};


export const sendPasswordReset = createAsyncThunk(
    'user/resetPassword',
    async (email: String, thunkAPI) => {
        try {
            const baseURL = window.location.origin;
            const response = await AuthService.resetPassword(baseURL, email);
            return response;
        } catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const sendNewUserPassword = createAsyncThunk(
    'user/newPassword',
    async (newPasswordData: UserPasswordUpdateRequest, thunkAPI) => {
        try {
            const response = await AuthService.updatePassword(
                newPasswordData.email, String(newPasswordData.password), newPasswordData.token);
            return response;
        } catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)


export const resetPasswordSlice = createSlice({
    name: 'resetPassword',
    initialState: testInitialState,
    reducers: {},
    extraReducers: builder => builder
        .addCase(sendPasswordReset.pending, (state) => {})
        .addCase(sendPasswordReset.fulfilled, (state, action) => {
            state.showHelperText = true;
        })
        .addCase(sendPasswordReset.rejected, (state) => {

        })
});



export const selectShowHelperText = (state: RootState) => state.resetPassword.showHelperText;

export default resetPasswordSlice.reducer;

