/*
 * src/views/login/ResetPasswordForm.tsx
 */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import clsx from 'clsx';
import qs from 'qs';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import { UserPasswordUpdateRequest } from './loginTypes';
import { sendNewUserPassword } from './resetPasswordSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            borderRadius: 10,
            backgroundColor: 'rgba(255, 255, 255, 0.06)',
            marginTop: 30,
            padding: theme.spacing(3),
            minWidth: 450
        },
        title: {
            color: theme.palette.common.white,
            paddingBottom: 20,
        },
        inputField: {
            '& .MuiInput-root': {
                color: theme.palette.grey[400],
            },
            '& .MuiInput-root.Mui-focused': {
                color: theme.palette.grey[100],
            },
        },
        inputFieldDefault: {
            '& .MuiInput-underline': {
                borderBottomWidth: 1,
                borderBottomStyle: 'solid',
                borderBottomColor: 'rgba(255, 255, 255, 0.5)'
            },
            '& .MuiInput-underline.Mui-focused': {
                borderBottomWidth: 0,
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: 'rgba(255, 255, 255, 0.8)'
            },
        },
        inputFieldError: {
            '& .MuiInput-underline': {
                borderBottomWidth: 1,
                borderBottomStyle: 'solid',
                borderBottomColor: 'rgba(226, 33, 33, 0.5)'
            },
            '& .MuiInput-underline.Mui-focused': {
                borderBottomWidth: 0,
                borderBottomColor: 'rgba(226, 33, 33, 0.8)'
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: 'rgba(226, 33, 33, 0.8)'
            },
        },       
        inputFieldSuccess: {
            '& .MuiInput-underline': {
                borderBottomWidth: 1,
                borderBottomStyle: 'solid',
                borderBottomColor: 'rgba(80, 190, 80, 0.5)'
            },
            '& .MuiInput-underline.Mui-focused': {
                borderBottomWidth: 0,
                borderBottomColor: 'rgba(80, 190, 80, 0.8)'
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: 'rgba(80, 190, 80, 0.8)'
            },
        },                 
        inputLabel: {
            color: theme.palette.grey[400],
            '&.Mui-focused': {
                color: theme.palette.grey[400],
            }
        },
        submitButton: {
            color: theme.palette.info.main,
            float: 'right',
        },
        disabledButton: {
            color: theme.palette.grey[600],
            float: 'right',
        }
    })
);


const ResetPasswordForm = () => {
    const classes = useStyles();
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const dispatch = useDispatch();
    const location = useLocation();

    // get URL parameters (useParams hook returns empty)
    const params = qs.parse(location.search.slice(1)) as unknown as UserPasswordUpdateRequest;
    const passwordsMatch = password === confirmPassword;

    const handleSubmitNewPassword = (event: React.FormEvent<HTMLFormElement> | React.MouseEvent) => {
        event.preventDefault();

        if (passwordsMatch) {
            const newPasswordData = {
                email: params.email,
                password: password,
                token: params.token,
            };
            console.log(newPasswordData);
            dispatch(sendNewUserPassword(newPasswordData));
        }
    }

    // style the textfields depending on matching password text input
    let textFieldClass = clsx(classes.inputField, classes.inputFieldDefault);
    if (password) {
        if (passwordsMatch) {
            textFieldClass = clsx(classes.inputField, classes.inputFieldSuccess);
        } else {
            textFieldClass = clsx(classes.inputField, classes.inputFieldError);
        }
    }

    return (
        <Container className={classes.container} maxWidth='xs'>
            <form onSubmit={handleSubmitNewPassword}>
                <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <Typography className={classes.title} variant='h6'>Reset password</Typography>
                        <TextField
                            className={textFieldClass}
                            label='Enter a new password'
                            name='password'
                            type='password'
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)}
                            size='small'
                            value={password}
                            InputLabelProps={{
                                classes: {
                                    root: classes.inputLabel
                                }
                            }}
                            InputProps={{
                                className: classes.inputField
                            }}
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                            className={textFieldClass}
                            label='Confirm password'
                            name='confirmPassword'
                            type='password'
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setConfirmPassword(e.target.value)}
                            size='small'
                            value={confirmPassword}
                            InputLabelProps={{
                                classes: {
                                    root: classes.inputLabel
                                }
                            }}
                            InputProps={{
                                className: classes.inputField
                            }}
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Button
                            onClick={handleSubmitNewPassword}
                            className={passwordsMatch ? classes.disabledButton : classes.submitButton}
                            disabled={!passwordsMatch}
                        >
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Container>
    );
}

export default ResetPasswordForm;
