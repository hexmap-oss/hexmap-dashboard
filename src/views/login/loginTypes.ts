/* 
 * src/views/login/loginTypes.ts
 */

export type LoginRequest = {
    email: string;
    password: string;
}

export type UserPasswordResetRequest = {
    baseURL: string;
    email: string;
}

export interface UserPasswordUpdateRequest {
    email: string;
    password?: string;
    token: string;
}