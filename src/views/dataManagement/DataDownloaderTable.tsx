/*
 * src/views/dataManagement/DataDownloaderTable.tsx
 */
import React from 'react';
import { useSelector } from 'react-redux';
import { format } from 'date-fns';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import { 
    selectSurveyDataLastEnd,
    selectSurveyDataLastStart,
    selectSurveyDataIsExporting,
    selectTripsDataLastEnd,
    selectTripsDataLastStart,
    selectTripsDataIsExporting
} from './dataManagementSlice';


interface EnhancedTableProps {
    classes?: ReturnType<typeof useStyles>;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    return (
        <TableHead>
            <TableRow>
                <TableCell></TableCell>

                <TableCell
                    key={'exportType'}
                    align='center'
                    padding='normal'
                >
                    <Typography variant='h6'>Type</Typography>
                </TableCell>

                <TableCell
                    key={'exportDate'}
                    align='center'
                    padding='normal'
                >
                    <Typography variant='h6'>Last Export Date</Typography>
                </TableCell>

                <TableCell
                    key={'exportStatus'}
                    align='center'
                    padding='normal'
                >
                    <Typography variant='h6'>Status</Typography>
                </TableCell>
            </TableRow>
        </TableHead>
    );
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        tableContainer: {
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(4)
        },
        tableRow: {
            '&.Mui-selected': {
                backgroundColor: 'inherit',
            },
            '&.MuiTableRow:hover': {
                backgroundColor: theme.palette.grey[50],
            },
            '&.Mui-selected:hover': {
                backgroundColor: theme.palette.grey[50],
            }            
        }
    })
);

interface DownloaderTableProps {
    exportType: string;
    setExportType: (type: string) => void;
};

const DownloaderTable = (props: DownloaderTableProps) => {
    const { exportType, setExportType } = props;

    const classes = useStyles();
    const surveyDataLastStart = useSelector(selectSurveyDataLastStart);
    const surveyDataLastEnd = useSelector(selectSurveyDataLastEnd);
    const surveyDataIsExporting = useSelector(selectSurveyDataIsExporting);
    const tripsDataLastStart = useSelector(selectTripsDataLastStart);
    const tripsDataLastEnd = useSelector(selectTripsDataLastEnd);
    const tripsDataIsExporting = useSelector(selectTripsDataIsExporting);
    const surveyDataIsSelected = exportType === 'surveyData';
    const tripsDataIsSelected =  exportType === 'tripsData';

    const handleRowClick = (event: React.MouseEvent, rowName: string) => {
        setExportType(rowName);
    };    

    const surveyDataText = (surveyDataLastStart && surveyDataLastEnd)
        ? `${format(surveyDataLastStart, 'yyyy-MM-dd')} – ${format(surveyDataLastEnd, 'yyyy-MM-dd')}`
        : ''
    const tripsDataText = (tripsDataLastStart && tripsDataLastEnd)
        ? `${format(tripsDataLastStart, 'yyyy-MM-dd')} – ${format(tripsDataLastEnd, 'yyyy-MM-dd')}`
        : ''

    return (
        <TableContainer className={classes.tableContainer}>
            <Table
                aria-labelledby='tableTitle'
                size={'medium'}
                aria-label='enhanced table'
            >
                <EnhancedTableHead />                            
                <TableBody>
                    <TableRow
                        hover
                        className={classes.tableRow}
                        onClick={(event: React.MouseEvent) => handleRowClick(event, 'surveyData')}
                        role='checkbox'
                        aria-checked={surveyDataIsSelected}
                        tabIndex={-1}
                        key={'survey-data-row'}
                        selected={surveyDataIsSelected}
                    >
                        <TableCell padding='checkbox'>
                            <Radio
                                checked={surveyDataIsSelected}
                                color='primary'
                                name='survey-data-radio-button'
                            />
                        </TableCell>
                        <TableCell align='center'><b>{'Survey Data'}</b></TableCell>
                        <TableCell align='center'>{surveyDataText}</TableCell>
                        <TableCell align='center'>{surveyDataIsExporting ? 'exporting...' : 'complete'}</TableCell>
                    </TableRow>

                    <TableRow
                        hover
                        className={classes.tableRow}
                        onClick={(event: React.MouseEvent) => handleRowClick(event, 'tripsData')}
                        role='checkbox'
                        aria-checked={tripsDataIsSelected}
                        tabIndex={-1}
                        key={'trips-data-row'}
                        selected={tripsDataIsSelected}
                    >
                        <TableCell padding='checkbox'>
                            <Radio
                                checked={tripsDataIsSelected}
                                color='primary'
                                name='trips-data-radio-button'
                            />
                        </TableCell>
                        <TableCell align='center'><b>{'Trips'}</b></TableCell>
                        <TableCell align='center'>{tripsDataText}</TableCell>
                        <TableCell align='center'>{tripsDataIsExporting ? 'exporting...' : 'complete'}</TableCell>
                    </TableRow>                                
                </TableBody>
            </Table>
        </TableContainer>
    );    
};

export default DownloaderTable;
