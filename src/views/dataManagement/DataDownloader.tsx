/*
 * src/views/dataManagement/DataDownloader.tsx
 */
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import GetAppIcon from '@material-ui/icons/GetApp';
import { Icon as MdiIcon } from '@mdi/react';
import { mdiBasketUnfill } from '@mdi/js';

import DateRangePicker from '../../components/widgets/DateRangePicker';
import { 
    connectWebsocket,
    selectMinDate,
    selectMaxDate,
    selectStartDate,
    selectEndDate,
    setStartDate,
    setEndDate,
    selectWebsocketConnectionId,
    fetchSurveyStatus,
    selectSurveyDataIsExporting,
    selectTripsDataIsExporting,
    selectSurveyDataArchiveUri,
    selectTripsDataArchiveUri,
    exportSurveyData,
    exportTripsData
} from './dataManagementSlice';
import DownloaderTable from './DataDownloaderTable';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),       
        },
        title: {
            borderBottomColor: theme.palette.grey[300],
            borderBottomStyle: 'solid',
            borderBottomWidth: 1,
            fontWeight: 700,
            fontSize: '1.8rem',
        },
        calendarGroup: {
            justifyContent: 'center',
            paddingTop: theme.spacing(2)
        },
        downloadButtonGroup: {
            display: 'flex',
            justifyContent: 'center'
        },
        downloadButton: {
            margin: theme.spacing(1),
        }
    })
);


const DataDownloader = () => {
    const classes = useStyles();
    const [exportType, setExportType] = useState('surveyData');
    const [downloadUri, setDownloadUri] = useState<string>();

    const minDate = useSelector(selectMinDate);
    const maxDate = useSelector(selectMaxDate);
    const startDate = useSelector(selectStartDate);
    const endDate = useSelector(selectEndDate);
    const surveyDataIsExporting = useSelector(selectSurveyDataIsExporting);
    const surveyDataArchiveUri = useSelector(selectSurveyDataArchiveUri);
    const tripsDataIsExporting = useSelector(selectTripsDataIsExporting);
    const tripsDataArchiveUri = useSelector(selectTripsDataArchiveUri);
    const websocketConnectionId = useSelector(selectWebsocketConnectionId);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchSurveyStatus())
        dispatch(connectWebsocket())
    }, [dispatch]);

    useEffect(() => {
        if (exportType === 'surveyData' && !surveyDataIsExporting && surveyDataArchiveUri) {
            setDownloadUri(surveyDataArchiveUri);
        }
        if (exportType === 'tripsData' && !tripsDataIsExporting && tripsDataArchiveUri) {
            setDownloadUri(tripsDataArchiveUri);
        }        
    }, [exportType, surveyDataIsExporting, tripsDataIsExporting, surveyDataArchiveUri, tripsDataArchiveUri]);


    const setUnixDates = (start: number, end: number) => {
        dispatch(setStartDate(start));
        dispatch(setEndDate(end));
    }

    const handleCreateArchive = () => {
        if (websocketConnectionId && startDate && endDate) {
            switch (exportType) {
                case 'surveyData':
                    dispatch(exportSurveyData({
                        connectionId: websocketConnectionId,
                        startTime: Math.floor(startDate / 1000),
                        endTime: Math.ceil(endDate / 1000),
                        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    }));
                    break
                case 'tripsData':
                    dispatch(exportTripsData({
                        connectionId: websocketConnectionId,
                        startTime: Math.floor(startDate / 1000),
                        endTime: Math.ceil(endDate / 1000),
                        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    }));
                    break
            }
        }
    }

    return (
        <Paper className={classes.root} elevation={3}>
            <Typography className={classes.title} variant='h4'>
                Download Tables
            </Typography>

            <Grid className={classes.calendarGroup} container>
                <Grid item sm={4}>
                    <DateRangePicker
                        minDate={minDate}
                        maxDate={maxDate}
                        startDate={startDate}
                        endDate={endDate}
                        setDates={setUnixDates}
                        center
                    />               
                </Grid>                
            </Grid>

            <Box>
                <DownloaderTable
                    exportType={exportType}
                    setExportType={setExportType}
                />
            </Box>

            <Box className={classes.downloadButtonGroup}>
                <Button
                    className={classes.downloadButton}
                    variant='contained'
                    color='primary'
                    startIcon={<MdiIcon path={mdiBasketUnfill} size={1} />}
                    onClick={handleCreateArchive}
                    disabled={!websocketConnectionId}
                >
                    Export
                </Button>
                <Button
                    className={classes.downloadButton}
                    variant='contained'
                    color='primary'
                    href={downloadUri}
                    startIcon={<GetAppIcon />}
                    disabled={!downloadUri}
                >
                    Download
                </Button>
            </Box>
        </Paper>
    );
};

export default DataDownloader;
