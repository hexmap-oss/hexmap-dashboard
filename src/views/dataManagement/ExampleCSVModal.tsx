/*
 * src/views/dataManagement/UploadSubwayStations.tsx
 */
import { ReactNode } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Typography } from '@material-ui/core';
import SimpleTable from '../../components/widgets/SimpleTable';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        helpText: {},
        table: {
            minWidth: 500
        },
    })
);


interface ExampleCSVModalProps {
    open: boolean;
    helpText: string | ReactNode[];
    title: string;
    onClose: () => void;
    // setOpen: (isOpen: boolean) => void;
}

const ExampleCSVModal = (props: ExampleCSVModalProps) => {
    const classes = useStyles();
    const tableHeaders = [
        {key: 'longitude', label: 'longitude'},
        {key: 'latitude', label: 'latitude'},
        {key: 'name', label: ['name', <i>(optional)</i>]},
        {key: 'other', label: ['other', <i>(optional)</i>]},
    ];
    const tableData = [
        {id: 1, longitude: -73.60419368, latitude: 45.44593094, name: 'ANGRIGNON', other: 1},
        {id: 2, longitude: -73.59421491, latitude: 45.45100128, name: 'MONK', other: 2},
        {id: 3, longitude: -73.58177134, latitude: 45.45672866, name: 'JOLICOEUR', other: 7},
        {id: 4, longitude: '...', latitude: '...', name: '...', other: '...'},
    ];

    // const handleClose = () => {
    //     setOpen(false);
    // }

    return (
        <>
        { props.open ? (
            <Dialog 
                aria-labelledby='example-csv-title'
                onClose={props.onClose} 
                open
            >
                <DialogTitle id='customized-dialog-title'>
                    {props.title}
                </DialogTitle>

                <DialogContent dividers>
                    <Typography className={classes.helpText}>{props.helpText}</Typography>
                    <SimpleTable
                        headers={tableHeaders}
                        rows={tableData}
                    />
                </DialogContent>

                <DialogActions>
                    <Button autoFocus onClick={props.onClose} color="primary">
                        Close
                    </Button>
                </DialogActions>            
            </Dialog>) 
        : undefined }
        </>
       
    );
}

export default ExampleCSVModal;
