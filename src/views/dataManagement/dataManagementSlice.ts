/* 
 * src/views/dataManagement/dataManagementSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../../app/store';
import DataManagementService, { ExportDataParameters } from '../../api/dataManagement';
import WebsocketService from '../../api/dataManagementWebsocket';


interface DataManagementState {
    minDate: number | null;
    maxDate: number | null;
    startDate: number | null;
    endDate: number | null;
    surveyDataLastStart?: number;
    surveyDataLastEnd?: number;
    surveyDataIsExporting: boolean;
    surveyDataArchiveUri?: string;
    tripsDataLastStart?: number;
    tripsDataLastEnd?: number;
    tripsDataIsExporting: boolean;
    tripsDataArchiveUri?: string;
    websocketConnectionId?: string;
};

const testInitialState: DataManagementState = {
    minDate: new Date(2021, 0, 1, 0, 0, 0).getTime(),
    maxDate: new Date().getTime(),
    startDate: new Date(2021, 0, 1, 0, 0, 0).getTime(),
    endDate: new Date().getTime(),
    surveyDataIsExporting: false,
    tripsDataIsExporting: false,
};

// const initialState: DataManagementState = {
//     minDate: null,
//     maxDate: null,
//     startDate: null,
//     endDate: null,
// };

export const fetchSurveyStatus = createAsyncThunk(
    'dataManagement/fetchSurveyStatus',
    async(_, thunkAPI) => {
        try {
            const response = await DataManagementService.fetchSurveyStatus();
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const connectWebsocket = createAsyncThunk(
    'dataManagement/handleWebsocketExport',
    async(_, thunkAPI) => {
        const { dispatch } = thunkAPI;

        try {
            const socket = WebsocketService.connect();
            
            socket.onopen = function(e) { 
                socket.send('CONNECTION_ID_REQUEST')
            }
    
            socket.onmessage = function(e) {
                const payload = JSON.parse(e.data)
    
                if (payload.type) {
                    switch (payload.type) {
                        case 'connection-id-response':
                            dispatch(dataManagementSlice.actions.setWebsocketConnectionId(payload.connectionId));
                            break
                        case 'raw-export-started':
                            break
                        case 'raw-export-complete':
                            dispatch(dataManagementSlice.actions.setSurveyExportMetadata(payload));
                            break
                        case 'trips-export-started':
                            break
                        case 'trips-export-complete':
                            dispatch(dataManagementSlice.actions.setTripsExportMetadata(payload));
                            break
                        default:
                            console.log('Websocket message not matched.', payload.type)
                    }
                } else {
                    console.log('Unknown websocket event:', e)
                }
            }
    
            socket.onerror = function(e) { 
                console.log('Websocket error:', e)
            }
            return
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }        
    }
)

export const exportSurveyData = createAsyncThunk(
    'dataManagement/exportSurveyData',
    async(params: ExportDataParameters, thunkAPI) => {
        try {
            const response = await DataManagementService.initiatiateExportSurveyData(params);
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }        
    }
)

export const exportTripsData = createAsyncThunk(
    'dataManagement/exportTripsData',
    async(params: ExportDataParameters, thunkAPI) => {
        try {
            const response = await DataManagementService.initiatiateExportTripsData(params);
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }        
    }
)


export const dataManagementSlice = createSlice({
    name: 'dataManagement',
    initialState: testInitialState,
    reducers: {
        setStartDate: (state, action: PayloadAction<number>) => {
            state.startDate = action.payload;
        },
        setEndDate: (state, action: PayloadAction<number>) => {
            state.endDate = action.payload;
        },
        setSurveyExportMetadata: (state, action) => {
            state.surveyDataLastStart = action.payload.raw.exportStart * 1000;
            state.surveyDataLastEnd = action.payload.raw.exportEnd * 1000;
            state.surveyDataArchiveUri = action.payload.raw.uri;
            state.surveyDataIsExporting = false;
        },
        setTripsExportMetadata: (state, action) => {
            state.tripsDataLastStart = action.payload.trips.exportStart * 1000;
            state.tripsDataLastEnd = action.payload.trips.exportEnd * 1000;
            state.tripsDataArchiveUri = action.payload.trips.uri;
            state.tripsDataIsExporting = false;
        },
        setWebsocketConnectionId: (state, action: PayloadAction<string>) => {
            console.log(`Websocket connected: ${action.payload}`);
            state.websocketConnectionId = action.payload;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchSurveyStatus.fulfilled, (state, action) => {
            state.startDate = action.payload.startTime * 1000;
            state.minDate = action.payload.startTime * 1000;
            state.surveyDataLastStart = action.payload.lastExport.raw.exportStart * 1000;
            state.surveyDataLastEnd = action.payload.lastExport.raw.exportEnd * 1000;
            state.surveyDataArchiveUri = action.payload.lastExport.raw.uri;
            state.tripsDataLastStart = action.payload.lastExport.trips.exportStart * 1000;
            state.tripsDataLastEnd = action.payload.lastExport.trips.exportEnd * 1000;
            state.tripsDataArchiveUri = action.payload.lastExport.trips.uri;
        })
        .addCase(exportSurveyData.fulfilled, (state, action) => {
            state.surveyDataIsExporting = true;
        })
        .addCase(exportTripsData.fulfilled, (state, action) => {
            state.tripsDataIsExporting = true;
        })        
});

export const { setStartDate, setEndDate, setSurveyExportMetadata, setTripsExportMetadata, setWebsocketConnectionId } = dataManagementSlice.actions;

export const selectMinDate = (state: RootState) => state.dataManagement.minDate;
export const selectMaxDate = (state: RootState) => state.dataManagement.maxDate;
export const selectStartDate = (state: RootState) => state.dataManagement.startDate;
export const selectEndDate = (state: RootState) => state.dataManagement.endDate;
export const selectSurveyDataLastStart = (state: RootState) => state.dataManagement.surveyDataLastStart;
export const selectSurveyDataLastEnd = (state: RootState) => state.dataManagement.surveyDataLastEnd;
export const selectSurveyDataIsExporting = (state: RootState) => state.dataManagement.surveyDataIsExporting;
export const selectSurveyDataArchiveUri = (state: RootState) => state.dataManagement.surveyDataArchiveUri;
export const selectTripsDataLastStart = (state: RootState) => state.dataManagement.tripsDataLastStart;
export const selectTripsDataLastEnd = (state: RootState) => state.dataManagement.tripsDataLastEnd;
export const selectTripsDataIsExporting = (state: RootState) => state.dataManagement.tripsDataIsExporting;
export const selectTripsDataArchiveUri = (state: RootState) => state.dataManagement.tripsDataArchiveUri;
export const selectWebsocketConnectionId = (state: RootState) => state.dataManagement.websocketConnectionId;

export default dataManagementSlice.reducer;
