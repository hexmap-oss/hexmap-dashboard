/*
 * src/views/dataManagement/UploadSubwayStations.tsx
 */
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import FileInput from '../../components/widgets/FileInput';
import ExampleCSVModal from './ExampleCSVModal';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),       
        },
        title: {
            borderBottomColor: theme.palette.grey[300],
            borderBottomStyle: 'solid',
            borderBottomWidth: 1,
            fontWeight: 700,
            fontSize: '1.8rem',
        },
        helpTextContainer: {
            display: 'flex',
            alignItems: 'center',
        },
        csvModalButton: {
            color: theme.palette.info.main,
            marginLeft: theme.spacing(1),
        },
        fileInputContainer: {
            marginTop: 15
        },
        deleteButton: {
            color: theme.palette.common.white,
            backgroundColor: theme.palette.error.main
        },
        deleteControlContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        }
    })
);


const UploadSubwayStations = () => {
    const classes = useStyles();
    const [dataExists] = useState(true);
    const [exampleOpen, setExampleOpen] = useState(false);

    const handleOpen = () => {
        setExampleOpen(true);
    }

    const handleClose = () => {
        setExampleOpen(false);
    }

    return (
        <>
        <Paper className={classes.root} elevation={3}>
            <Typography className={classes.title} variant='h4'>
                Upload Subway Stations
            </Typography>
            <Box className={classes.helpTextContainer}>
                <Typography variant='subtitle2'>
                    Upload metro stations .csv file with &nbsp;<code>latitude</code>&nbsp; and &nbsp;<code>longitude</code>&nbsp; columns.
                </Typography>
                <Button
                    className={classes.csvModalButton}
                    onClick={handleOpen}
                >
                    Example
                </Button>
            </Box>

            <Grid className={classes.fileInputContainer} container>
                <Grid item sm={8}>
                    <FileInput
                        labelText={'Stops CSV'}
                    />
                </Grid>
                <Grid className={classes.deleteControlContainer} item sm={4}>
                    <Box>
                        { dataExists
                        ? (
                            <Button
                                className={classes.deleteButton}
                                variant='contained'
                                startIcon={<DeleteForeverIcon />}
                                size={'small'}
                            >
                                Delete
                            </Button>
                        )
                        : (
                            <Typography variant='subtitle2'>
                                No subway data uploaded.
                            </Typography>
                        )}
                    </Box>
                </Grid>
            </Grid>
        </Paper>

        <ExampleCSVModal
            open={exampleOpen}
            onClose={handleClose}
            helpText={[
                'A .csv file with the following schema is required for the trip breaker to handle trips made by underground metro or subway. The only needed columns are ',
                <code>latitude</code>,
                ' and ',
                <code>longitude</code>, '.']}
            title={'Formatting CSV Data'}
            // setOpen={handleOpen}
        />
        </>
    );
};

export default UploadSubwayStations;
