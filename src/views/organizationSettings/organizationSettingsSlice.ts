/* 
 * src/views/organizationSettings/organizationSettingsSlice.ts
 */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';


interface OrganizationSettingsState {
    avatarUri?: string;
    contactEmail?: string;
};

const testInitialState: OrganizationSettingsState = {
    avatarUri: undefined,
    contactEmail: 'abc@powell.ca'
};

// const initialState: OrganizationSettingsState = {
//     avatarUri: undefined,
//     contactEmail: '',
// }

export const organizationSettingsSlice = createSlice({
    name: 'organizationSettings',
    initialState: testInitialState,
    reducers: {
        setContactEmail: (state, action: PayloadAction<string>) => {
            state.contactEmail = action.payload;
        },
        uploadAvatar: (state, action: PayloadAction<string>) => {
            console.log('Upload avatar reducer')
        }        
    }
});
export const { setContactEmail, uploadAvatar } = organizationSettingsSlice.actions;

export const selectAvatarUri = (state: RootState) => state.organizationSettings.avatarUri;
export const selectContactEmail = (state: RootState) => state.organizationSettings.contactEmail;

export default organizationSettingsSlice.reducer;
