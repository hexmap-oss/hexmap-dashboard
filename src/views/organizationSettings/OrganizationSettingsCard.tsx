/* 
 * src/views/organizationSettings/OrganizationSettingsCard.tsx
 */
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import AvatarFileSelect from './AvatarFileSelect';
import {
    selectAvatarUri,
    selectContactEmail,
    setContactEmail
} from './organizationSettingsSlice';



export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: 16,
            paddingRight: 16,
        },
        container: {
            paddingTop: theme.spacing(3),
        },
        formContainer: {
            paddingLeft: '5%',
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2),            
        },
        inputFieldContainer: {
            '& .MuiFormControlLabel-label': {
                marginRight: theme.spacing(2),
            }
        },
        inputField: {
            width: 300,
        },
        emailButtonContainer: {
            display: 'flex',
            alignItems: 'center'
        }
    })
);


const OrganizationSettingsCard = () => {
    const classes = useStyles();
    const avatarUri = useSelector(selectAvatarUri);
    const contactEmail = useSelector(selectContactEmail);
    const [email, setEmail] = useState<string>(contactEmail || '');
    const dispatch = useDispatch();

    const handleUpdateEmail = () => {
        dispatch(setContactEmail(email));
    }

    return (
        <Card className={classes.root} elevation={0}>
            <CardContent>
                <Grid className={classes.container} container spacing={3}>
                    <Grid item sm={12}>
                        <Typography variant='h6'>Organization Picture</Typography>
                        <Typography variant='subtitle2'>Choose an image to represent your organization on the dashboard and within the HexMap app.</Typography>                

                        <AvatarFileSelect imgUri={avatarUri} />
                    </Grid>

                    <Grid item sm={7}>
                        <Typography variant='h6'>App Contact Email</Typography>
                        <Typography variant='subtitle2'>Edit an email for survey users to contact your organization through the app.</Typography>                

                        <Box className={classes.formContainer}>
                            <form onSubmit={handleUpdateEmail}>
                                <FormControl component='fieldset' error={true}>
                                    <FormGroup>
                                        <FormControlLabel
                                            className={classes.inputFieldContainer}
                                            label='Contact email'
                                            labelPlacement='start'
                                            control={
                                                <TextField  
                                                    className={classes.inputField}
                                                    id='current-password'
                                                    placeholder='info@your-organization.com'
                                                    value={email}
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)}
                                                    type='email'
                                                    fullWidth
                                                />
                                            }
                                        />
                                    </FormGroup>
                                </FormControl>
                            </form>
                        </Box>
                    </Grid>
                    <Grid className={classes.emailButtonContainer} item sm={5}>
                        <Box>
                            <Button
                                color='secondary'
                                variant='contained'
                                onClick={handleUpdateEmail}
                            >
                                Update Email
                            </Button>
                        </Box>
                    </Grid>              
                </Grid>
            </CardContent>
        </Card>
    );
};

export default OrganizationSettingsCard;
