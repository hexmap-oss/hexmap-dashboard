/* 
 * src/views/organizationSettings/AvatarFileSelect.tsx
 */
import Image from 'material-ui-image';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import DefaultAvatar from './defaultAvatar.png'
import FileInput from '../../components/widgets/FileInput';
import { Typography } from '@material-ui/core';


export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4),
        },
        avatarContainer: {
            display: 'flex',
            justifyContent: 'center',
        },
        avatar: {
            borderColor: theme.palette.primary.main,
            borderStyle: 'solid',
            borderWidth: 3,
            borderRadius: '50%',
            filter: 'drop-shadow(0px 0px 8px #999)',
            overflow: 'visible',
        },
        fileInputContainer: {
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            justifyContent: 'center',
            alignContent: 'center'
        },
        helpText: {
            color: theme.palette.grey[700]
        }
    })
);

interface AvatarFileSelectProps {
    imgUri?: string;
};

const AvatarFileSelect = ({imgUri}: AvatarFileSelectProps) => {
    const classes = useStyles();

    return(
        <Grid container className={classes.root}>
            <Grid item sm={3}>
                <Box className={classes.avatarContainer}>
                    <Image
                        className={classes.avatar}
                        src={imgUri ? imgUri : DefaultAvatar}
                        animationDuration={1000}
                        style={{
                            paddingTop: 'none',
                            height: '80px',
                            width: '80px',
                        }}
                    />
                </Box>
            </Grid>
            <Grid item sm={9} md={7}>
                <Box className={classes.fileInputContainer}>
                    <FileInput
                        labelText={'Logo'}
                    />
                    <Typography className={classes.helpText}>
                        Recommended avatar dimensions are 120 x 120 px. Other sizes will automatically be rescaled.
                    </Typography>
                </Box>
            </Grid>            
        </Grid>
    );
}

export default AvatarFileSelect;
