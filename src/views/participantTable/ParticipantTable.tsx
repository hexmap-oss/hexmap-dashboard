/* 
 * src/views/participantUsers/participantUsersTable.tsx
 */
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import {
    fetchParticipantTable,
    selectSearchText
} from './participantTableSlice';
import UsersTable from '../../components/usersTable/UsersTable';
import { getComparator } from './participantTableTypes';
import {
    setColumnVisibility,
    selectColumns,
    selectRows,
    selectTotalRows,
} from './participantTableSlice';
import { Order, UsersTableRow } from '../../components/usersTable/usersTableTypes';


const formatTableSortingJSON = (orderBy: string, orderAsc: boolean) => {
    if (orderBy) {
        return JSON.stringify(
            { [orderBy]: Number(orderAsc) }
        );
    }
    return JSON.stringify([]);
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: 32,
            paddingRight: 32,
            paddingBottom: 32,
        },

    }),
);

const ParticipantUsersTable = () => {
    const classes = useStyles();
    const [currentPage, setCurrentPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(8);
    // const [tableSorting, setTableSorting] = useState(undefined);
    const [tableOrderAsc, setTableOrderAsc] = useState(false);
    const [tableOrderBy, setTableOrderBy] = useState<keyof UsersTableRow>('id');    
    const columns = useSelector(selectColumns);
    const rows = useSelector(selectRows);
    const searchText = useSelector(selectSearchText);
    const totalRows = useSelector(selectTotalRows);
    const dispatch = useDispatch();

    const tableSorting = new Map<string, number>([[tableOrderBy, Number(tableOrderAsc)]]);
    
    useEffect(() => {
        dispatch(fetchParticipantTable({
            itemsPerPage: rowsPerPage,
            pageIndex: Math.max(1, currentPage + 1),
            searchString: searchText,
            sorting: formatTableSortingJSON(tableOrderBy, tableOrderAsc)
        }));
    }, [dispatch, searchText, tableOrderAsc, tableOrderBy, currentPage, rowsPerPage]);

    const handleColumnVisibility = (columnLabel: string, isVisible: boolean) => {
        dispatch(setColumnVisibility({label: columnLabel, isVisible}));
    }

    const handleChangePage = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    };

    const handleChangeRowsPerPage = (rowsPerPage: number) => {
        setRowsPerPage(rowsPerPage);
        setCurrentPage(0);
    };

    const handleRowClick = (rowIndex: number) => {
        console.log(rowIndex);
    }

    const handleSort = (order: Order, orderBy: keyof UsersTableRow) => {
        const isAsc = tableOrderBy === orderBy && tableOrderAsc;
        setTableOrderAsc(isAsc);
        setTableOrderBy(orderBy);
    };

    const handleShowMapUser = () => {
        
    };

    return (
        <Paper className={classes.root} elevation={0}>
            <UsersTable
                rows={rows}
                columns={columns}
                getComparator={getComparator}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                onRowClick={handleRowClick}
                onSort={handleSort}
                onToggleColumnVisibility={handleColumnVisibility}
                onToolbarButtonClick={handleShowMapUser}
                page={currentPage}
                rowsPerPage={rowsPerPage}
                sorting={tableSorting}
                toolbarButtonAriaLabel={'user map'}
                toolbarButtonTooltipTitle={'Show on Map'}                
                totalRows={totalRows}
            />
        </Paper>
    );
};


export default ParticipantUsersTable;
