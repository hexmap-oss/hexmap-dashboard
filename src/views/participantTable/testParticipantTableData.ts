/* 
 * src/views/metrics/testInstallationsData.tsx
 */

const participantUsersResponse = {
    status: 'success',
    type: 'MobileUserTable',
    results: {
        pagination: {
            totalPages: 4,
            currentPage: 1,
            totalItems: 27
        },
        data: [
            {
                Age: '35-44',
                Email: 'esn.mrd@gmail.com',
                Gender: 'Male',
                created_at: '2021-03-04T16:37:35Z',
                itinerum_version: '4.1.6-vanilla',
                member_type: 'At home',
                model: 'Pixel 2 XL',
                name: 'Ali',
                os: 'Android',
                os_version: '11',
                uuid: '467859cc-f582-4abb-8a0a-c3ebbc085e93'
            },
            {
                Age: '35-44',
                Email: 'kyle@test2.com',
                Gender: 'Other/Neither',
                created_at: '2021-02-19T22:56:48Z',
                itinerum_version: '4.1.6-vanilla',
                member_type: 'A full-time worker',
                model: 'Pixel 3a',
                name: 'Kyle',
                os: 'Android',
                os_version: '11',
                travel_mode_alt_work: 'Car',
                travel_mode_work: 'Public Transit',
                uuid: '4f74d77a-f907-479a-85b5-db208d4b3041'
            },
            {
                Age: '16-24',
                Email: 'test@test.com',
                Gender: 'Male',
                created_at: '2021-02-16T17:35:58Z',
                itinerum_version: '4.1.6-vanilla',
                member_type: 'A full-time worker',
                model: 'sdk_gphone_x86',
                name: 'Ali',
                os: 'Android',
                os_version: '11',
                travel_mode_alt_work: 'N/A',
                travel_mode_work: 'Walk',
                uuid: '213c220f-ef5d-4aca-bfe0-8ea2dbee3967'
            },
            {
                Age: '35-44',
                Email: 'ayazdizadeh@hexmap.io',
                Gender: 'Male',
                created_at: '2021-02-13T20:30:01Z',
                itinerum_version: '2.0.2(3)',
                member_type: 'A full-time worker',
                model: 'iPhone13,4',
                name: 'Ali',
                os: 'iOS',
                os_version: '14.2',
                travel_mode_alt_work: 'Car',
                travel_mode_work: 'Car',
                uuid: 'AB1DF18F-DEB7-47C0-BE80-CCC39047B65E'
            },
            {
                Age: '25-34',
                Email: 'kyle-feb12-android@email.com',
                Gender: 'Male',
                created_at: '2021-02-12T15:06:02Z',
                itinerum_version: '4.1.5-vanilla',
                member_type: 'A full-time worker',
                model: 'Pixel 3a',
                name: 'Kyle',
                os: 'Android',
                os_version: '11',
                travel_mode_alt_work: 'Bicycle',
                travel_mode_work: 'Walk',
                uuid: 'b82a58f6-1949-4c35-b3d9-e723f9318389'
            },
            {
                Age: '16-24',
                Email: 'kyle-ios-feb11@email.com',
                Gender: 'Does not wish to respond',
                created_at: '2021-02-12T04:59:10Z',
                itinerum_version: '2.0.3(4)',
                member_type: 'A full-time worker',
                model: 'iPhone 8',
                name: 'Kyle',
                os: 'iOS',
                os_version: '14.4',
                travel_mode_alt_work: 'Walk',
                travel_mode_work: 'Public Transit',
                uuid: '1A6E95E6-4E9C-4257-BA38-445CCE3FD645'
            },
            {
                Age: '16-24',
                Email: 'test@test.com',
                Gender: 'Male',
                created_at: '2021-02-12T03:20:14Z',
                itinerum_version: '4.1.4-vanilla',
                member_type: 'A full-time worker',
                model: 'sdk_gphone_x86',
                name: 'Ali',
                os: 'Android',
                os_version: '11',
                travel_mode_alt_work: 'N/A',
                travel_mode_work: 'Walk',
                uuid: '7ce4defe-fd8f-4bee-97c6-9d08b0047adc'
            },
            {
                Age: '35-44',
                Email: 'kyle-feb11-ios@email.com',
                Gender: 'Does not wish to respond',
                created_at: '2021-02-11T16:05:23Z',
                itinerum_version: '2.0.2(3)',
                member_type: 'A full-time worker',
                model: 'iPhone 8',
                name: 'Kyle',
                os: 'iOS',
                os_version: '14.4',
                travel_mode_alt_work: 'Bicycle',
                travel_mode_work: 'Walk',
                uuid: '0D7308E1-9143-405C-BEA8-AFC6F34FDEBA'
            }
        ],
        columns: [
            'created_at',
            'uuid',
            'model',
            'itinerum_version',
            'os',
            'os_version',
            'member_type',
            'travel_mode_work',
            'travel_mode_alt_work',
            'travel_mode_study',
            'travel_mode_alt_study',
            'Gender',
            'Age',
            'Email',
            'name'
        ]
    }
};

export const testParticipantUsersTableColumns = participantUsersResponse.results.columns;
export const testParticipantUsersTableRows = participantUsersResponse.results.data;