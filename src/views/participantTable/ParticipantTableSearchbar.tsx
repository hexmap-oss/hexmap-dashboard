/* 
 * src/views/participantUsers/ParticipantUsersSearchbar.tsx
 */
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import SearchInput from '../../components/widgets/SearchInput';
import { 
    selectSearchText,
    setSearchText
} from './participantTableSlice';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: '24px 32px'
        }
    }),
);


const ParticipantUsersSearchbar = () => {
    const classes = useStyles();
    const searchText = useSelector(selectSearchText);
    const dispatch = useDispatch();

    const handleChange = (searchText: string) => {
        dispatch(setSearchText(searchText));
    }

    return (
        <Paper className={classes.root} elevation={0}>
            <SearchInput
                searchText={searchText}
                onChange={handleChange}
            />
        </Paper>
    );
}

export default ParticipantUsersSearchbar;

