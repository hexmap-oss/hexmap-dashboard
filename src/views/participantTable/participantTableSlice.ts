/* 
 * src/views/participantUsers/participantUsersTableSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { ColumnToggle, TableColumn, TableRow } from './participantTableTypes';
import { RootState } from '../../app/store';
import ParticipantsService, { ParticipantTableParameters } from '../../api/participants';
import { UsersTableColumn } from '../../components/usersTable/usersTableTypes';


interface ParticipantUsersTableState {
    columns: TableColumn[],
    rows: TableRow[],
    searchText: string,
    totalRows: number,
};

const testInitialState: ParticipantUsersTableState = {
    columns: [
    //     { rowKey: 'id', numeric: false, disablePadding: true, label: 'ID', visible: false},
    //     { rowKey: 'email', numeric: false, disablePadding: true, label: 'Email address' },
    //     { rowKey: 'registeredAt', numeric: false, disablePadding: false, label: 'Registered At' },
    //     { rowKey: 'active', numeric: false, disablePadding: false, label: 'Active' },
    //     { rowKey: 'userLevel', numeric: false, disablePadding: false, label: 'User Level' },
    //     { rowKey: 'manage', numeric: false, disablePadding: false, label: 'Manage' }        
    ] as TableColumn[],
    rows: [
    //     { id: 1, email: 'richard.breiss@gmail.com', registeredAt: new Date(), userLevel: 'Admin', active: true },
    //     { id: 2, email: 'hornsby413@hotmail.com', registeredAt: new Date(), userLevel: 'Member', active: true },
    //     { id: 3, email: 'dragoNslayer6@mail.ru', registeredAt: new Date(), userLevel: 'Member', active: false },
    //     { id: 4, email: 'steve@apple.com', registeredAt: new Date(), userLevel: 'Participant', active: true },
    ] as TableRow[],
    searchText: '',
    totalRows: 0,
};

// const initialState: OrganizationUsersTableState = {
//     columns: [],
//     rows: [],
// }

export const fetchParticipantTable = createAsyncThunk(
    'participantUsers/fetchTable',
    async(params: ParticipantTableParameters, thunkAPI) => {
        try {
            const response = await ParticipantsService.fetchParticipantTable(params);
            return response.data.results;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const participantTableSlice = createSlice({
    name: 'participantTable',
    initialState: testInitialState,
    reducers: {
        setColumnVisibility: (state, action: PayloadAction<ColumnToggle>) => {
            const newColumns = state.columns.map(c => {
                const col = Object.assign({} as UsersTableColumn, c)
                if (col.label === action.payload.label) {
                    col.visible = action.payload.isVisible;
                }
                return col;
            });
            state.columns = newColumns;
        },
        setSearchText: (state, action: PayloadAction<string>) => {
            state.searchText = action.payload;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchParticipantTable.pending, (state, action) => {})
        .addCase(fetchParticipantTable.fulfilled, (state, action) => {
            const idColumn: TableColumn = {rowKey: 'id', numeric: false, disablePadding: true, label: 'ID', visible: false};
            const columns: TableColumn[] = action.payload.columns.map((columnLabel: string, idx: number) => {
                // check if column has been disabled by user or default to 'visible'
                const existingCol = state.columns.find(c => c.label === columnLabel)
                return {
                    rowKey: columnLabel,
                    label: columnLabel,
                    visible: existingCol ? existingCol.visible : true,
                }
            });
            const rows: TableRow[] = action.payload.data.map((d: any, idx: number) => {
                d.id = idx + 1;
                d.selectable = true;
                return d;
            });

            state.columns = [idColumn, ...columns];
            state.rows = rows;
            state.totalRows = action.payload.pagination.totalItems;
        })  
        .addCase(fetchParticipantTable.rejected, (state, action) => {
            console.log(2, action.payload);
        })
});

export const { setColumnVisibility, setSearchText } = participantTableSlice.actions;

export const selectColumns = (state: RootState) => state.participantTable.columns;
export const selectRows = (state: RootState) => state.participantTable.rows;
export const selectSearchText = (state: RootState) => state.participantTable.searchText;
export const selectTotalRows = (state: RootState) => state.participantTable.totalRows;

export default participantTableSlice.reducer;
