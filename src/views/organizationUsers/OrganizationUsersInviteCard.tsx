/* 
 * src/views/organizationUsers/OrganizationUsersInviteCard.tsx
 */
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import InviteCodeWidget from '../../components/widgets/InviteCodeWidget';
import {
    fetchUserInviteCode,
    refreshUserInviteCode,
    selectInviteCode,
    selectInviteCodeIsRefreshing
} from './organizationUsersCodeSlice';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(2),
            paddingBottom: theme.spacing(3),
        }
    })
);

const OrganizationUserInviteCard = () => {
    const classes = useStyles();
    const inviteCode = useSelector(selectInviteCode);
    const inviteCodeIsRefreshing = useSelector(selectInviteCodeIsRefreshing);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUserInviteCode());
    }, [dispatch]);

    const handleRefreshInviteCode = () => {
        dispatch(refreshUserInviteCode());
    }

    return (
        <Paper className={classes.root} elevation={0}>
            <Container disableGutters>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant='h6'>
                            Invite dashboard users
                        </Typography>

                        <Typography variant='body2'>
                            Invite researcher users by providing the access code below. Researcher users have access to view and download survey data, but cannot edit any settings. Only one code may be active at a time. Researcher users have to include this code in their registration.
                        </Typography>
                    </Grid>

                    <Grid item xs={12}>
                        <InviteCodeWidget
                            code={inviteCode}
                            codeIsRefreshing={inviteCodeIsRefreshing}
                            refreshCode={handleRefreshInviteCode}
                        />
                    </Grid>                            
                </Grid>
            </Container>
        </Paper>
    );
};

export default OrganizationUserInviteCard;
