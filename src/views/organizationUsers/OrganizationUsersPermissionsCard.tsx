/* 
 * src/views/organizationUsers/OrganizationUsersPermissionsCard.tsx
 */
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import UsersTable from '../../components/usersTable/UsersTable';
import { UsersTableRow } from '../../components/usersTable/usersTableTypes';
import { selectColumns, selectRows } from './organizationUsersTableSlice';
import { getComparator, TableRow } from './organizationUsersTableTypes';
import {
    fetchOrganizationUsers,
    deleteUsers
} from './organizationUsersTableSlice';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingTop: theme.spacing(1),
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2),
        },
        tableContainer: {
            height: 600,
            width: '100%'
        }
    })
);


const OrganizationUsersPermissionsCard = () => {
    const classes = useStyles();
    const columns = useSelector(selectColumns);
    const rows = useSelector(selectRows);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchOrganizationUsers());
    }, [dispatch]);

    const handleDeleteUsers = (rows: UsersTableRow[]) => {
        const organizationRows = rows as TableRow[];
        const emails = organizationRows.map(r => r.email);
        dispatch(deleteUsers(emails));
    }

    return (
        <Paper className={classes.root} elevation={0}>
            <Container className={classes.tableContainer} disableGutters>
                <UsersTable
                    title={'Dashboard user permissions'}
                    rows={rows}
                    columns={columns}
                    colorScheme={'delete'}
                    getComparator={getComparator}
                    onToolbarButtonClick={handleDeleteUsers}
                    toolbarButtonAriaLabel={'delete users'}
                    toolbarButtonTooltipTitle={'Delete Users'}
                    selectMany
                    disableSelectAll
                />
            </Container>
        </Paper>
    );
}


export default OrganizationUsersPermissionsCard;