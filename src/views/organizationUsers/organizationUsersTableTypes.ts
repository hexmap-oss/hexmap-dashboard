/* 
 * src/views/organizationUsers/organizationUsersTableTypes.ts
 */
import { Order, UsersTableColumn, UsersTableRow } from '../../components/usersTable/usersTableTypes';


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
}
  
export function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (a: { [key in Key]: number | string | boolean }, b: { [key in Key]: number | string | boolean }) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}


export interface TableColumn extends UsersTableColumn {};

export interface TableRow extends UsersTableRow {
    email: string;
    registeredAt: number;
    userLevel: string;
    active: boolean;
    // manage?: typeof DeleteButton;
}

export interface APIUserTableRow {
    email: string;
    createdAt: number;
    userLevel: string;
    active: boolean;    
};


