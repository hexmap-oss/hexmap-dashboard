/* 
 * src/views/organizationUsers/organizationUsersTableSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

import { TableColumn, TableRow, APIUserTableRow } from './organizationUsersTableTypes';
import OrganizationUsersService from '../../api/organizationUsers';


interface OrganizationUsersTableState {
    columns: TableColumn[],
    rows: TableRow[]
};

const testInitialState: OrganizationUsersTableState = {
    columns: [
        { rowKey: 'id', label: 'ID', visible: false},
        { rowKey: 'email', label: 'Email address' },
        { rowKey: 'registeredAt', label: 'Registered At', date: true},
        { rowKey: 'active', label: 'Active' },
        { rowKey: 'userLevel', label: 'User Level' },
    ] as TableColumn[],
    rows: [
        { id: 1, email: 'richard.breiss@gmail.com', registeredAt: new Date().getTime(), userLevel: 'Admin', active: true, selectable: false},
        { id: 2, email: 'hornsby413@hotmail.com', registeredAt: new Date().getTime(), userLevel: 'Member', active: true, selectable: true },
        { id: 3, email: 'dragoNslayer6@mail.ru', registeredAt: new Date().getTime(), userLevel: 'Member', active: false, selectable: true },
        { id: 4, email: 'steve@apple.com', registeredAt: new Date().getTime(), userLevel: 'Participant', active: true, selectable: true },
    ] as TableRow[]
};

const initialState: OrganizationUsersTableState = {
    columns: testInitialState.columns,
    rows: [],
}


export const fetchOrganizationUsers = createAsyncThunk(
    'organizationUsers/table',
    async (_, thunkAPI) => {
        try {
            const response = await OrganizationUsersService.fetchOrganizationUsers();
            const rows = response.data.results.data.map((r: APIUserTableRow, idx: number) => {
                return {
                    id: idx + 1,
                    email: r.email,
                    registeredAt: r.createdAt * 1000,
                    userLevel: r.userLevel,
                    active: true,
                    selectable: r.userLevel !== "admin",
                
            }}) as TableRow[];
            return rows;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)


export const organizationUsersTableSlice = createSlice({
    name: 'organizationUsersTable',
    initialState: initialState,
    reducers: {
        deleteUsers: (state, action: PayloadAction<string[]>) => {
            const deletedUserEmails = action.payload;
            state.rows = state.rows.filter(row => deletedUserEmails.indexOf(row.email) === -1);
        }
    },
    extraReducers: builder => builder
        .addCase(fetchOrganizationUsers.fulfilled, (state, action) => {
            state.rows = action.payload;
        })
        .addCase(fetchOrganizationUsers.rejected, (state, action) => {
            console.log('API rejected: could not fetch organization users table data');
        })
});
export const { deleteUsers } = organizationUsersTableSlice.actions;

export const selectColumns = (state: RootState) => state.organizationUsersTable.columns;
export const selectRows = (state: RootState) => state.organizationUsersTable.rows;

export default organizationUsersTableSlice.reducer;
