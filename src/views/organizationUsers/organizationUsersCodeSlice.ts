/* 
 * src/views/organizationUsers/organizationUsersTableSlice.ts
 */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { RootState } from '../../app/store';
import OrganizationUsersService from '../../api/organizationUsers';
import { isDebugMode } from '../../app/utils';


interface OrganizationUsersCodeState {
    inviteCode: string;
    isRefreshing: boolean;
};

const testInitialState: OrganizationUsersCodeState = {
    inviteCode: '',
    isRefreshing: false
};

const initialState: OrganizationUsersCodeState = {
    inviteCode: '',
    isRefreshing: false,
}


export const fetchUserInviteCode = createAsyncThunk(
    'organizationUsers/inviteCode',
    async(_, thunkAPI) => {
        try {
            const response = await OrganizationUsersService.fetchInviteCode();
            const inviteCode = response.data.results.token;
            return inviteCode;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const refreshUserInviteCode = createAsyncThunk(
    'organizationUsers/refreshInviteCode',
    async(_, thunkAPI) => {
        try {
            const response = await OrganizationUsersService.refreshInviteCode();
            const inviteCode = response.data.results.token;
            return inviteCode;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const organizationUsersCodeSlice = createSlice({
    name: 'organizationUsersCode',
    initialState: isDebugMode() ? testInitialState : initialState,
    reducers: {},
    extraReducers: builder => builder
        .addCase(fetchUserInviteCode.pending, (state, action) => {
            state.isRefreshing = true;
            state.inviteCode = '';
        })
        .addCase(fetchUserInviteCode.fulfilled, (state, action) => {
            state.isRefreshing = false;
            state.inviteCode = action.payload;
        })
        .addCase(fetchUserInviteCode.rejected, (state, action) => {
            state.isRefreshing = false;
            console.log('API rejected: could not fetch organization user invite code')
        })

        .addCase(refreshUserInviteCode.pending, (state, action) => {
            state.isRefreshing = true;
            state.inviteCode = '';
        })
        .addCase(refreshUserInviteCode.fulfilled, (state, action) => {
            state.isRefreshing = false;
            state.inviteCode = action.payload;
        })
        .addCase(refreshUserInviteCode.rejected, (state, action) => {
            state.isRefreshing = false;
            console.log('API rejected: coult not refresh organization user invite code')
        })        
});


export const selectInviteCode = (state: RootState) => state.organizationUsersCode.inviteCode;
export const selectInviteCodeIsRefreshing = (state: RootState) => state.organizationUsersCode.isRefreshing;

export default organizationUsersCodeSlice.reducer;
