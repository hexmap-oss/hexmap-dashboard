/* 
 * src/views/surveySettings/PromptSettings.tsx
 */
import { useState } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DateRangeIcon from '@material-ui/icons/DateRange';
import FlagIcon from '@material-ui/icons/Flag';

import { useStyles } from './cardStyles';
import GridItemAlignedFormInput from '../../components/widgets/GridItemAlignedFormInput'
import SaveButton from '../../components/widgets/SaveButton';


const PromptSettings = () => {
    const classes = useStyles();
    const [maxDays, setMaxDays] = useState<string | number>(14);
    const [maxPrompts, setMaxPrompts] = useState<string | number>(20);

    return (
        <Card className={classes.root} elevation={0}>
            <CardContent>
                {/* Heading text */}
                <Typography className={classes.title} variant='h6'>Prompt Settings</Typography>
                <Typography className={classes.subtext} variant='subtitle2'>Adjust the prompt period and other survey attributes below.</Typography>

                {/* Inputs */}
                <Grid container spacing={3} className={classes.inputContainer}>
                    <GridItemAlignedFormInput
                        label={'Max. Number of Days'}
                        Icon={<DateRangeIcon />}
                        value={maxDays}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={setMaxDays}
                    />

                    <GridItemAlignedFormInput
                        label={'Max. Number of Prompts'}
                        Icon={<FlagIcon />}
                        value={maxPrompts}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={setMaxPrompts}
                    />                    
                </Grid>

                {/* Save button */}
                <Grid container>
                    <Grid item xs={12} sm={10} md={9}>
                        <SaveButton
                            className={classes.saveButton}
                            isEdited={false}
                            isSaving={false}
                            onClick={() => {}}
                        />
                    </Grid>
                </Grid>       
            </CardContent>
        </Card>
    );
};

export default PromptSettings;
