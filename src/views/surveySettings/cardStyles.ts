/*
 * src/views/surveySettings/cardStyles.ts
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        // page
        root: {
            paddingLeft: 16,
            paddingRight: 16,
        },
        title: {
            paddingLeft: theme.spacing(1),
            borderBottomWidth: 1,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.grey[300],
            marginBottom: 10,            
        },
        subtext: {
            color: theme.palette.grey[800],
        },
        // buttons
        editButton: {
            marginRight: 8,
            marginLeft: 8,
            backgroundColor: theme.palette.primary.main,
        },
        saveButton: {
            float: 'right',
            backgroundColor: theme.palette.primary.main,
            '&:hover': {
                backgroundColor: theme.palette.primary.light,
            }
        },
        resetButtonContainer: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 32
        },
        resetButton: {
            color: theme.palette.common.white,
            backgroundColor: theme.palette.error.main,
            '&:hover': {
                backgroundColor: theme.palette.error.dark,                
            }
        },
        // form fields
        inputContainer: {
            alignItems: 'center',
            paddingTop: 24,
            paddingBottom: 16,
        },
        label: {
            display: 'flex',
            justifyContent: 'center'
        },
        // form labels
        labelText: {
            fontWeight: 700,
            fontSize: 14
        },        
        centerButtons: {
            display: 'flex',
            justifyContent: 'space-evenly',
        },
        // status indicator
        statusIndicator: {
            display: 'flex',
            alignItems: 'center',
            paddingTop: theme.spacing(1),
        },
        statusIndicatorLight: {
            backgroundColor: '#01AA78',
            height: 8,
            width: 8,
            borderRadius: '50%',
            marginRight: theme.spacing(3)
        },        
    })
);
