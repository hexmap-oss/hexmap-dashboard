/*
 * src/views/surveySettings/SurveySettings.tsx
 */
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PencilOutlineIcon from '@material-ui/icons/CreateOutlined';

import { useStyles } from './cardStyles';
import SurveyDescriptionModal from './SurveyDescriptionModal';
import GridItemAlignedInput from '../../components/widgets/GridItemAlignedInput'
import SaveButton from '../../components/widgets/SaveButton';
import { 
    selectIsEdited,
    setIsEdited
} from './surveySettingsSlice';


const SurveySettings = () => {
    const classes = useStyles();
    const [contactEmail, setContactEmail] = useState<string | number>('user@email.com');
    const isEdited = useSelector(selectIsEdited);
    const dispatch = useDispatch();

    const handleOnChange = (email: string | number) => {
        dispatch(setIsEdited(true));
        setContactEmail(email);
    }

    return (
        <Card className={classes.root} elevation={0}>
            <CardContent>
                {/* Heading text */}
                <Typography className={classes.title} variant='h4'>Survey Settings</Typography>
                <Typography className={classes.subtext} variant='subtitle1'>Adjust survey disclaimers and an email to contact for support.</Typography>

                {/* Inputs */}
                <Grid container spacing={3} className={classes.inputContainer}>
                    <Grid item className={classes.label} xs={4}>
                        <Typography className={classes.labelText}>About Your Survey</Typography>
                    </Grid>
                    <Grid item xs={8} sm={6} md={5}>
                        {/* Input with icon offset by Grid */}
                        <Box className={classes.centerButtons}>
                            <SurveyDescriptionModal
                                className={classes.editButton}
                                buttonText={'Survey Description'}
                                startIcon={<PencilOutlineIcon/>}
                            />

                            <SurveyDescriptionModal
                                className={classes.editButton}
                                buttonText={'Terms of Service'}
                                startIcon={<PencilOutlineIcon/>}
                            /> 
                        </Box>                       
                    </Grid>

                    <GridItemAlignedInput
                        label={'Contact Email'}
                        Icon={<MailOutlineIcon />}
                        value={contactEmail}
                        onChange={handleOnChange}
                    />
                </Grid>


                {/* Save button */}
                <Grid container>
                    <Grid item xs={12} sm={10} md={9}>
                        <SaveButton
                            className={classes.saveButton}
                            isEdited={isEdited}
                            isSaving={false}
                            onClick={() => {}}
                        />
                    </Grid>
                </Grid>                
            </CardContent>
        </Card>
    );
};

export default SurveySettings;
