/* 
 * src/views/surveySettings/surveySettingsSlice.ts
 */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface SettingsState {
    isEdited: boolean;
};

const initialState: SettingsState = {
    isEdited: false,
};

export const settingsSlice = createSlice({
    name: 'settings',
    initialState,
    reducers: {
        setIsEdited: (state, action: PayloadAction<boolean>) => {
            state.isEdited = action.payload;
        }
    }
});

export const { setIsEdited } = settingsSlice.actions;

export const selectIsEdited = (state: RootState) => state.settings.isEdited;

export default settingsSlice.reducer;
