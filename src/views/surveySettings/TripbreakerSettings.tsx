/* 
 * src/views/surveySettings/TripbreakerSettings.tsx
 */
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import TimerIcon from '@material-ui/icons/Timer';
import { Icon as MdiIcon } from '@mdi/react';
import { mdiRuler } from '@mdi/js';

import { useStyles } from './cardStyles';
import GridItemAlignedFormInput from '../../components/widgets/GridItemAlignedFormInput'
import SaveButton from '../../components/widgets/SaveButton';
import { 
    selectIsEdited,
    setIsEdited
} from './surveySettingsSlice';


const TripbreakerSettings = () => {
    const classes = useStyles();
    const [minGPSAccuracyMeters, setMinGPSAccuracyMeters] = useState<string | number>(450);
    const [breakPeriodSeconds, setBreakPeriodSeconds] = useState<string | number>(360);
    const [coldStartDistanceMeters, setColdStartDistanceMeters] = useState<string | number>(750);
    const [subwayStationBufferMeters, setSubwayStationBufferMeters] = useState<string | number>(300);
    const isEdited = useSelector(selectIsEdited);
    const dispatch = useDispatch();

    const handleOnChangeGPSAccuracy = (value: string | number) => {
        dispatch(setIsEdited(true));
        setMinGPSAccuracyMeters(value);
    };

    const handleOnChangeBreakPeriod = (value: string | number) => {
        dispatch(setIsEdited(true));
        setBreakPeriodSeconds(value);
    };

    const handleOnChangeColdStartDistance = (value: string | number) => {
        dispatch(setIsEdited(true));
        setColdStartDistanceMeters(value);
    };

    const handleOnChangeSubwayStationBuffer = (value: string | number) => {
        dispatch(setIsEdited(true));
        setSubwayStationBufferMeters(value);
    };

    return (
        <Card className={classes.root} elevation={0}>
            <CardContent>
                {/* Heading text */}
                <Typography className={classes.title} variant='h6'>Tripbreaker Settings</Typography>

                {/* Inputs */}
                <Grid container spacing={3} className={classes.inputContainer}>
                    <GridItemAlignedFormInput
                        label={'GPS Accuracy Threshold (meters)'}
                        Icon={<GpsFixedIcon />}
                        value={minGPSAccuracyMeters}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={handleOnChangeGPSAccuracy}
                    />

                    <GridItemAlignedFormInput
                        label={'Break Period (seconds)'}
                        Icon={<TimerIcon />}
                        value={breakPeriodSeconds}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={handleOnChangeBreakPeriod}
                    />

                    <GridItemAlignedFormInput
                        label={'Cold Start Distance (meters)'}
                        Icon={<MdiIcon path={mdiRuler} size={1} />}
                        value={coldStartDistanceMeters}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={handleOnChangeColdStartDistance}
                    />

                    <GridItemAlignedFormInput
                        label={'Subway Station Buffer (meters)'}
                        Icon={<MdiIcon path={mdiRuler} size={1} />}
                        value={subwayStationBufferMeters}
                        type={'number'}
                        InputProps={{ inputProps: { min: 0, style: { textAlign: 'center' }} }}
                        onChange={handleOnChangeSubwayStationBuffer}
                    />
                </Grid>

                {/* Save button */}
                <Grid container>
                    <Grid item xs={12} sm={10} md={9}>
                        <SaveButton
                            className={classes.saveButton}
                            isEdited={isEdited}
                            isSaving={false}
                            onClick={() => {}}
                        />
                    </Grid>
                </Grid>       
            </CardContent>
        </Card>
    );
};

export default TripbreakerSettings;
