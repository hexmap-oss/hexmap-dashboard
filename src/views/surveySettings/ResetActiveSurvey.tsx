/*
 * src/views/surveySettings/ResetActiveSurvey.tsx
 */
import { useState } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';

import { useStyles } from './cardStyles';


const SurveySettings = () => {
    const classes = useStyles();
    const [surveyStart] = useState(new Date(2021, 1, 24, 12, 54, 33));

    return (
        <Card className={classes.root} elevation={0}>
            <CardContent>
                {/* Heading text */}
                <Typography className={classes.title} variant='h5'>Reset Survey</Typography>
                <Typography className={classes.subtext} variant='subtitle1'>Clears all the users and coordinates from the database to begin a new survey. Use with caution.</Typography> 

                {/* Activity indicator */}
                <Box className={classes.statusIndicator}>
                    <Box className={classes.statusIndicatorLight} />
                    <Typography>Active survey started at {surveyStart.toISOString()}.</Typography>
                </Box>

                <Box className={classes.resetButtonContainer}>
                    <Button
                        className={classes.resetButton}
                        startIcon={<DeleteIcon />}
                    >
                        Reset
                    </Button>
                </Box>
            </CardContent>
        </Card>
    );
};

export default SurveySettings;
