/* 
 * src/views/odMap/ODMapTable.tsx
 */
import { useMemo, useState } from 'react';
import { useSelector } from 'react-redux'
import { createStyles, withStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

// import { ODFlowDict as rows } from '../../data/od_flows_dict';
import ODTablePaginationActions from './ODMapTablePagination';
import { ODMapTableRow, Order } from './ODMapTypes';
import {
    selectBoundaryName,
    selectTripsMatrix,
} from './ODMapSlice';
import { sortRecordKeys } from './ODMapUtils';


function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
}
  
export function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (a: { [key in Key]: number | string | boolean }, b: { [key in Key]: number | string | boolean }) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}



const headCells = [
    {id: 'startMunicipality', label: 'Origin'},
    {id: 'endMuncipality', label: 'Destination'},
    {id: 'trips', label: 'Trips'},
];


const StyledHeadCell = withStyles((theme: Theme) =>
    createStyles({
        head: {
            backgroundColor: theme.palette.primary.dark,
            color: theme.palette.primary.light,
            fontWeight: 600,
        },
    })
)(TableCell);

const StyledTableCell = withStyles((theme: Theme) =>
    createStyles({
        body: {
            fontSize: 14,
            padding: 6,
            paddingLeft: 8,
            maxWidth: 300,
        },
    })
)(TableCell);



const ODMapTable = () => {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [rows, setRows] = useState<ODMapTableRow[]>([]);
    const boundaryName = useSelector(selectBoundaryName);
    const tripsMatrix = useSelector(selectTripsMatrix);

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    // format new rows when new tripMatrix data is received
    useMemo(() => {
        if (tripsMatrix) {
            const tripsMatrixKeys = sortRecordKeys(tripsMatrix);
            const r = tripsMatrixKeys.map((boundaryId) => (
                {
                    id: boundaryId,
                    startMunicipality: tripsMatrix[boundaryId].boundaryName,
                    endMunicipality: boundaryName,
                    trips: tripsMatrix[boundaryId].trips,
                    selectable: true,
                }
            ))
            setRows(r);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tripsMatrix]);

    return (
        <TableContainer>
            <Table aria-label='od map pagination table'>
                <TableHead>
                    <TableRow>
                    { headCells.map(headCell => (
                        <StyledHeadCell key={headCell.id}>
                            { headCell.label }
                        </StyledHeadCell>
                    ))}
                    </TableRow>
                </TableHead>

                <TableBody>
                    {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows
                    ).map((row: ODMapTableRow) => (
                        <TableRow key={row.id}>
                            <StyledTableCell>{row.startMunicipality}</StyledTableCell>
                            <StyledTableCell>{row.endMunicipality}</StyledTableCell>
                            <StyledTableCell>{row.trips}</StyledTableCell>
                        </TableRow>
                    ))}
                </TableBody>

                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[10, 20, 50, { label: 'All', value: -1 }]}
                            colSpan={3}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'rows per page' },
                                native: true
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={ODTablePaginationActions}
                        >
                        </TablePagination>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
}


export default ODMapTable;
