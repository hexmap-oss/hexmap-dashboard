/* 
 * src/views/odMap/ODArcMap.tsx
 */
import { useMemo, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { scalePow } from 'd3-scale';
import DeckGL from '@deck.gl/react';
import { PickInfo, RGBAColor } from 'deck.gl';
import { GeoJsonLayer, ArcLayer } from '@deck.gl/layers';
import mapboxgl from 'mapbox-gl';
import { FlyToInterpolator, StaticMap } from 'react-map-gl';
import bbox from '@turf/bbox';

import {
    selectRegion,
    selectStartTimestamp,
    selectEndTimestamp,
    selectMapDirection,
    selectTransportMode,
    selectTripPurpose,
} from './ODMapControlSlice';
import {
    fetchBoundaries,
    fetchTripsMatrix,
    selectBoundaries,
    setBoundaryName,
    selectTripsMatrix,
    selectTripsMatrixMin,
    selectTripsMatrixMax,
} from './ODMapSlice';
import { calculateArcs } from './ODMapUtils';
import { mapTheme } from './ODMapColors';


// Mitigates a babel transpiling issue due to upgrading to mapbox-gl v2, eventually remove.
// @ts-ignore
// eslint-disable-next-line import/no-webpack-loader-syntax
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;


const INITIAL_VIEW_STATE = {
    longitude: -73.63,
    latitude: 45.52,
    zoom: 8,
    maxZoom: 12,
    pitch: 30,
    bearing: 0,
    transitionDuration: 1000,
    transitionInterpolator: new FlyToInterpolator(),
};
const MAP_STYLE = 'https://basemaps.cartocdn.com/gl/dark-matter-gl-style/style.json';
const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiYWxpeWF6ZGkyIiwiYSI6ImNrbGl1ZXNzbzAybDQydXBoemNrZnEybTIifQ.lq1ZzhziZdYlamMMRjo3GQ';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        map: {
            height: 'calc(100vh - 129px)',
            width: '100%',
            position: 'relative',
        }
    })
);

const ODArcMap = () => {
    const classes = useStyles();
    const regionName = useSelector(selectRegion);
    const boundaries = useSelector(selectBoundaries);
    const startTimestamp = useSelector(selectStartTimestamp);
    const endTimestamp = useSelector(selectEndTimestamp);
    const mapDirection = useSelector(selectMapDirection);
    const tripPurpose = useSelector(selectTripPurpose);
    const transportMode = useSelector(selectTransportMode);
    const tripsMatrix = useSelector(selectTripsMatrix);
    const tripsMatrixDomain = [useSelector(selectTripsMatrixMin), useSelector(selectTripsMatrixMax)];
    const [viewState, setViewState] = useState(INITIAL_VIEW_STATE);
    const [hoverBoundary, setHoverBoundary] = useState<number | null>(null);
    const [boundaryId, setBoundaryId] = useState<number | null>(null);
    const dispatch = useDispatch();

    const colors = mapDirection === 'generation'
        ? mapTheme.generation
        : mapTheme.attraction

    // load new geojson boundary when region is changed
    useEffect(() => {
        if (regionName) {
            dispatch(fetchBoundaries(regionName));
        }
    }, [dispatch, regionName])

    // load new tripmatrix when boundary is changed
    useEffect(() => {
        if (!!boundaryId) {
            dispatch(fetchTripsMatrix({
                boundaryId,
                startTime: startTimestamp,
                endTime: endTimestamp,
                mapType: mapDirection,
                transportMode,
                tripPurpose,
            }));
        }
    }, [dispatch, boundaryId, startTimestamp, endTimestamp, mapDirection, transportMode, tripPurpose]);

    // update viewport on new boundaries data
    useEffect(() => {
        if (boundaries) {
            const [minLng, minLat, maxLng, maxLat] = bbox(boundaries);

            const newViewState = Object.assign({}, viewState, {
                longitude: (maxLng + minLng) / 2,
                latitude: (maxLat + minLat) / 2,
                transitionDuration: 1000,
                transitionInterpolator: new FlyToInterpolator()
            })
            setViewState(newViewState);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [boundaries])

    // calcuate new arcs on new tripsMatrix data
    const arcs = useMemo(() => {
        if (!!boundaries && !!boundaryId && !!tripsMatrix) {
            return calculateArcs(boundaries, boundaryId, tripsMatrix)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tripsMatrix]);

    // handle boundary mouse click
    const handleSelectBoundary = (feature: any) => {
        setBoundaryId(feature.object.properties.id)
        dispatch(setBoundaryName(feature.object.properties.label));
    };

    // handle boundary mose hover
    const getTooltip = (info: any) => {
        return info.object?.properties.label;
    }

    // BUILD LAYER DATA
    const colorScale = scalePow<RGBAColor, RGBAColor>()
        .domain(tripsMatrixDomain)
        .range([colors.FILL_MIN, colors.FILL_MAX]);

    const layers = [
        new GeoJsonLayer({
            id: 'geojson-outline',
            data: boundaries,
            lineWidthScale: 40,
            lineWidthMinPixels: 1,
            getFillColor: colors.FILL_DEFAULT,
            getLineColor: (d) => {
                const feat = d as GeoJSON.Feature;
                if (hoverBoundary && hoverBoundary === feat.properties?.id) {
                    return colors.BOUNDARY_HOVER;
                }
                return colors.BOUNDARY_DEFAULT;
            }, 
            pickable: true,
            updateTriggers: {
                getLineColor: hoverBoundary,
            }
        }),
        new GeoJsonLayer({
            id: 'geojson',
            data: boundaries,
            extruded: true,
            stroked: true,
            lineWidthScale: 20,
            lineWidthMinPixels: 1,
            getElevation: (d) => {
                const feat = d as GeoJSON.Feature;
                if (tripsMatrix) {
                    const id = feat.properties?.id;
                    if (tripsMatrix[id]) {
                        return tripsMatrix[id].trips * 50;
                    }
                }
                return 0;
            },
            getFillColor: (d) => {
                const feat = d as GeoJSON.Feature;
                if (boundaryId && feat.properties?.id === boundaryId) {
                    return colors.FILL_SELECTED;
                }
                // return a color value according the scale
                if (tripsMatrix) {
                    const id = feat.properties?.id;
                    if (tripsMatrix[id]) {
                        const value = tripsMatrix[id].trips;
                        const [ r, g, b ] = colorScale(value);
                        return [ r, g, b, 200] as RGBAColor;
                    }  
                }
                return colors.FILL_DEFAULT;
            },
            onClick: handleSelectBoundary,
            onHover: (event: PickInfo<any>) => {
                setHoverBoundary(event.object
                    ? event.object.properties.id
                    : null
                );
            },            
            pickable: true,
            updateTriggers: {
                getElevation: tripsMatrix,
                getFillColor: tripsMatrix,
            }
        }),
        new ArcLayer({
            id: 'arc',
            data: arcs,
            getSourcePosition: (d: any) => d.source,
            getTargetPosition: (d: any) => d.target,
            getSourceColor: (d: any) => colors.ARC_START,
            getTargetColor: (d :any) => colors.ARC_END,
        }),
    ];

    return (
        <div className={classes.map}>
            <DeckGL
                layers={layers}
                initialViewState={viewState}
                getTooltip={getTooltip}  // causes strange width issue when cursor is at bottom of map
                width={'100%'}
                controller
            >
                <StaticMap
                    mapStyle={MAP_STYLE}
                    preventStyleDiffing={true}
                    mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
                />
            </DeckGL>
        </div>
    );
};


export default ODArcMap;