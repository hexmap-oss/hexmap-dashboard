/* 
 * src/views/odMap/ODMapControl.tsx
 */
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import DateFnsUtils from '@date-io/date-fns';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { KeyboardDatePicker, KeyboardTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import {
    fetchAllRegions,
    selectAllRegions,
    selectMapDirection,
    selectTripPurposeOptions,
    selectRegion,
    selectTripPurpose,
    selectStartTimestamp,
    selectTransportMode,
    selectTransportModeOptions,
    setTimestamps,
    setMapDirection,
    setTripPurpose,
    setRegion,
    setTransportMode,
} from './ODMapControlSlice';
import DropdownSelect from '../../components/widgets/DropdownSelect';
import { dateToUnixTime, parseUnixTime } from '../../app/utils';
import { Box } from '@material-ui/core';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dateControl: {
            display: 'inline-block',
            margin: theme.spacing(1)
        },        
        formControl: {
            margin: theme.spacing(1)
        },
        dateInput: {
            marginLeft: theme.spacing(1)
        }
    })
);


const ODMapControl = () => {
    const classes = useStyles();
    const allRegions = useSelector(selectAllRegions);
    const selectedRegion = useSelector(selectRegion);
    const mapDirection = useSelector(selectMapDirection);
    const transportModeOptions = useSelector(selectTransportModeOptions);
    const selectedTransportMode = useSelector(selectTransportMode);
    const startTimestamp = useSelector(selectStartTimestamp);
    const tripPurposeOptions = useSelector(selectTripPurposeOptions);
    const selectedTripPurpose = useSelector(selectTripPurpose);
    const [intervalM, setIntervalM] = useState(180);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchAllRegions());
    }, [dispatch]);

    useEffect(() => {
        dispatch(setTimestamps({
            startTimestamp,
            endTimestamp: startTimestamp + (intervalM * 60)
        }));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, intervalM]);

    const handleChangeRegion = (region: string) => {
        dispatch(setRegion(region));
    }

    const handleDateChange = (date: Date | null) => {
        if (date) {
            dispatch(setTimestamps({
                startTimestamp: dateToUnixTime(date),
                endTimestamp: dateToUnixTime(date) + (intervalM * 60)
            }));
        }
    }

    const handleChangeInterval = (interval: string) => {
        setIntervalM(parseInt(interval, 10));
    }

    const handleChangeMapDirection = (event: React.MouseEvent, mapDirection: string) => {
        if (mapDirection) {
            dispatch(setMapDirection(mapDirection));
        }
    }

    const handleChangeTransportMode = (transportMode: string) => {
        dispatch(setTransportMode(transportMode));
    }

    const handleChangePurpose = (purpose: string) => {
        dispatch(setTripPurpose(purpose));
    }

    return (
        <Grid container> 
            <Grid item lg={6} xs={12} >
                {/* Region selection */}
                <DropdownSelect
                    label={'Region'}
                    options={allRegions}
                    value={selectedRegion}
                    onChange={handleChangeRegion}
                />
                <Box className={classes.dateControl}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            id={'date-picker-dialog'}
                            label={'Date'}
                            format={'yyyy-MM-dd'}
                            value={parseUnixTime(startTimestamp)}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change map date'
                            }}
                        />

                        <KeyboardTimePicker
                            className={classes.dateInput}
                            id={'time-picker-dialog'}
                            label={'Time'}
                            value={parseUnixTime(startTimestamp)}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change map time',
                            }}
                        />                        
                    </MuiPickersUtilsProvider>
                </Box>

                <DropdownSelect
                    label={'Interval (minutes)'}
                    options={['15', '30', '60', '180', '360', '720', '1440']}
                    value={intervalM.toString()}
                    onChange={handleChangeInterval}
                />
            </Grid>

            <Grid item lg={6} xs={12}>
                {/* Trip direction selection */}
                <FormControl className={classes.formControl}>
                    <ToggleButtonGroup
                        aria-label='toggle OD map generation or attraction'
                        value={mapDirection}
                        onChange={handleChangeMapDirection}
                        exclusive
                    >
                        <ToggleButton value='generation'>
                            Generation
                        </ToggleButton>
                        <ToggleButton value='attraction'>
                            Attraction
                        </ToggleButton>
                    </ToggleButtonGroup>
                </FormControl>
                
                {/* Mode of transport selection */}
                <DropdownSelect
                    label={'Transport Mode'}
                    options={transportModeOptions}
                    value={selectedTransportMode}
                    onChange={handleChangeTransportMode}
                />

                {/* Trip purpose selection */}
                <DropdownSelect
                    label={'Purpose'}
                    options={tripPurposeOptions}
                    value={selectedTripPurpose}
                    onChange={handleChangePurpose}
                />                
            </Grid>
        </Grid>
    );
}

export default ODMapControl;
