/* 
 * src/views/odMap/ODMapControlSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../../app/store';
import ODMapService from '../../api/odMap';
import { MapTimes } from './ODMapTypes';


interface ODMapControlState {
    allRegions: string[];
    endTimestamp: number;
    mapDirection: string;
    region: string;  // must not be undefined to control dropdown component
    tripPurpose: string;
    tripPurposeOptions: string[];
    startTimestamp: number;
    transportMode: string;
    transportModeOptions: string[];    
};

const testInitialState: ODMapControlState = {
    allRegions: [],
    endTimestamp: 1506344400,  // 2017-09-25 90:00:00-0400,
    mapDirection: 'generation',
    tripPurpose: 'all',
    tripPurposeOptions: [
        'all',
        'work',
        'education',
        'shopping',
        'health',
        'leisure',
        'unlabeled',        
    ],
    region: '',
    startTimestamp: 1506333600,  // 2017-09-25 06:00:00-0400
    transportMode: 'all',
    transportModeOptions: [
        'all',
        'walk',
        'bike',
        'car',
        'public transit',
        'unlabeled',
    ],    
}


export const fetchAllRegions = createAsyncThunk(
    'odMap/fetchAllRegions',
    async(_, thunkAPI) => {
        try {
            const response = await ODMapService.fetchRegions();
            return response.data.results.regions;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)


export const ODMapControlSlice = createSlice({
    name: 'odMapControl',
    initialState: testInitialState,
    reducers: {
        setMapDirection: (state, action: PayloadAction<string>) => {
            state.mapDirection = action.payload;
        },
        setTransportMode: (state, action: PayloadAction<string>) => {
            state.transportMode = action.payload;
        },
        setTripPurpose: (state, action: PayloadAction<string>) => {
            state.tripPurpose = action.payload;
        },
        setRegion: (state, action: PayloadAction<string>) => {
            state.region = action.payload;
        },        
        setTimestamps: (state, action: PayloadAction<MapTimes>) => {
            state.startTimestamp = action.payload.startTimestamp;
            state.endTimestamp = action.payload.endTimestamp;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchAllRegions.pending, (state, action) => {
            state.allRegions = [];
            state.region = '';
        })
        .addCase(fetchAllRegions.fulfilled, (state, action: PayloadAction<string[]>) => {
            state.allRegions = action.payload;
            state.region = action.payload[0];
        })
        .addCase(fetchAllRegions.rejected, (state, action) => {})
});


export const { setMapDirection, setTransportMode, setTripPurpose, setTimestamps, setRegion } = ODMapControlSlice.actions;
export const selectAllRegions = (state: RootState) => state.odMapControl.allRegions;
export const selectEndTimestamp = (state: RootState) => state.odMapControl.endTimestamp;
export const selectMapDirection = (state: RootState) => state.odMapControl.mapDirection;
export const selectRegion = (state: RootState) => state.odMapControl.region;
export const selectTripPurpose = (state: RootState) => state.odMapControl.tripPurpose;
export const selectTripPurposeOptions = (state: RootState) => state.odMapControl.tripPurposeOptions;
export const selectStartTimestamp = (state: RootState) => state.odMapControl.startTimestamp;
export const selectTransportMode = (state: RootState) => state.odMapControl.transportMode;
export const selectTransportModeOptions = (state: RootState) => state.odMapControl.transportModeOptions;

export default ODMapControlSlice.reducer;
