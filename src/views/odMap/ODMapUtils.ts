/* 
 * src/views/odMap/ODMapUtils.ts
 */
import { ODArc, TripsMatrixRecord } from './ODMapTypes';


export function sortRecordKeys(rec: Record<number, any>) {
    return Object.keys(rec).map(v => parseInt(v)).sort();
}


export function calculateTripMatrixDomain(tripMatrix: Record<number, TripsMatrixRecord>) {
    let min = 1E16,
        max = 0;

    for (let boundaryId in tripMatrix) {
        const numTrips = tripMatrix[boundaryId].trips;
        if (numTrips < min) min = numTrips;
        if (numTrips > max) max = numTrips;
    }
    return { min, max };
}


export function calculateArcs(boundaries: GeoJSON.FeatureCollection<GeoJSON.MultiPolygon>, boundaryId: number, tripMatrix: Record<number, TripsMatrixRecord>) {
    const selected = boundaries.features.find(f => f.properties?.id === boundaryId)
    if (!selected) {
        console.log("Selected boundary could not be matched in source data:", boundaryId)
        return
    }
    const centroidLookup: { [id: number]: number[] } = {};
    boundaries.features.forEach(f => {
        if (f.properties) {
            centroidLookup[f.properties.id] = f.properties.centroid;
        }
    });
    // Object.keys sends strings to .map() instead of numbers requiring
    // additional casting. Maybe a better way to do this.
    return Object.keys(tripMatrix).map((toIdStr: string): ODArc => {
        const toId = Number(toIdStr);
        return {
            source: selected.properties?.centroid,
            target: centroidLookup[toId],
            value: tripMatrix[toId].trips
        };
    });
}
