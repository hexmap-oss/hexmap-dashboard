/* 
 * src/views/odMap/ODMapTypes.ts
 */
import { Position } from '@deck.gl/core';

export type Order = 'asc' | 'desc';


export interface ODMapTableColumn {
    rowKey: keyof ODMapTableRow;
    startMunicipality: string;
    endMunicipality: string;
    trips: number;
    disablePadding?: boolean;
    numeric?: boolean;
    visible?: boolean;
}

export interface ODMapTableRow {
    id: number;
    startMunicipality: string;
    endMunicipality: string;
    trips: number;    
    selectable: boolean;
}


export interface ODArc {
    source: number[];
    target: number[];
    value: number;
    gain?: Position;
    quantile?: Position;
}

export type TripsMatrixRecord = {
    trips: number;
    boundaryName: string;
}

export type MapTimes = {
    startTimestamp: number;
    endTimestamp: number;
}
