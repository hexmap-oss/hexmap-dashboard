/* 
 * src/views/odMap/ODMapSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../../app/store';
import ODMapService, { TripsMatrixParameters } from '../../api/odMap';
import { calculateTripMatrixDomain } from './ODMapUtils';
import { TripsMatrixRecord } from './ODMapTypes';


interface ODMapState {
    boundaryName: string;
    boundaries: GeoJSON.FeatureCollection<GeoJSON.MultiPolygon> | undefined;
    tripsMatrix: Record<number, TripsMatrixRecord> | undefined;
    tripsMatrixMin: number;
    tripsMatrixMax: number;
};

const testInitialState: ODMapState = {
    boundaryName: '',
    boundaries: undefined,
    tripsMatrix: undefined,
    tripsMatrixMin: -1,
    tripsMatrixMax: -1,
}

export const fetchBoundaries = createAsyncThunk(
    'odMap/fetchBoundaries',
    async(regionName: string, thunkAPI) => {
        try {
            const response = await ODMapService.fetchBoundaries(regionName);
            return response.data.results;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const fetchTripsMatrix = createAsyncThunk(
    'odMap/fetchTripsMatrix',
    async(params: TripsMatrixParameters, thunkAPI) => {
        try {
            const response = await ODMapService.fetchTripsMatrix(params);
            const { odMap } = thunkAPI.getState() as RootState;
            const odMapState = odMap as ODMapState;
            return {
                jsonMatrix: response.data.results,
                boundaries: odMapState.boundaries,
            }
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const ODMapSlice = createSlice({
    name: 'odMap',
    initialState: testInitialState,
    reducers: {
        setBoundaryName: (state, action: PayloadAction<string>) => {
            state.boundaryName = action.payload;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchBoundaries.pending, (state, action) => {})
        .addCase(fetchBoundaries.fulfilled, (state, action) => {
            state.boundaries = action.payload;
        })
        .addCase(fetchTripsMatrix.fulfilled, (state, action) => {
            const jsonMatrix = action.payload.jsonMatrix;
            const boundaries = action.payload.boundaries;

            // hacky way to generate trips matrix with labels from json matrix
            const tripsMatrix: Record<number, TripsMatrixRecord> = {};
            const boundariesLookup: { [id: number]: string } = {};
            boundaries?.features.forEach((f) => {
                const boundaryId = f.properties?.id;
                const boundaryName = f.properties?.label;
                boundariesLookup[boundaryId] = boundaryName;
            })
            for (const _id in jsonMatrix) {
                const boundaryId = _id as unknown as number;
                tripsMatrix[boundaryId] = {
                    boundaryName: boundariesLookup[boundaryId],
                    trips: jsonMatrix[boundaryId],
                }
            }
            state.tripsMatrix = tripsMatrix;
            
            const { min, max } = calculateTripMatrixDomain(tripsMatrix);
            state.tripsMatrixMin = min;
            state.tripsMatrixMax = max;
        })
});

export const { setBoundaryName } = ODMapSlice.actions;

export const selectBoundaryName = (state: RootState) => state.odMap.boundaryName;
export const selectBoundaries = (state: RootState) => state.odMap.boundaries;
export const selectTripsMatrix = (state: RootState) => state.odMap.tripsMatrix;
export const selectTripsMatrixMin = (state: RootState) => state.odMap.tripsMatrixMin;
export const selectTripsMatrixMax = (state: RootState) => state.odMap.tripsMatrixMax;

export default ODMapSlice.reducer;

