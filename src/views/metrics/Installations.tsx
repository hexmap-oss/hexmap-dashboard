/* 
 * src/views/metrics/Installations.tsx
 */
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { ResponsiveBar } from '@nivo/bar';
import { ResponsivePie } from '@nivo/pie';

import { ChartColors } from './metricsConstants';
import { ChartSliceType } from './metricsTypes';
import { useSelector } from 'react-redux';
import { selectInstallationsBarData, selectInstallationsPieData } from './metricsSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        chartContainer: {
            height: 261,
            position: 'relative',
        },
        noDataOverlay: {
            position: 'absolute',
            height: '100%',
            width: '100%',
            top: 80,
            left: 0,
            textAlign: 'center',
            color: theme.palette.grey[400],
            fontSize: 40,
            fontWeight: 800,
            display: 'none',
        },
        showNoDataOverlay: {
            display: 'inherit'
        },
        responsivePieContainer: {
            position: 'absolute',
            height: '100%',
            width: '100%',
        },
        hideXS: {
            [theme.breakpoints.down('sm')]: {
                display: 'none',
            },            
        }
    })
);


const Installations = () => {
    const classes = useStyles();
    const installationsBarData = useSelector(selectInstallationsBarData);
    const installationsPieData = useSelector(selectInstallationsPieData);
    const isNoData = installationsBarData.length === 0;

    const getOSColor = (slice: ChartSliceType) => {
        return ChartColors[slice.id];
    }

    const formatBarChartLabels = (e: string | number | Date) => {
        return 'false';
    }

    return (
        <Card>
            <CardContent>
                <Typography>Installations</Typography>

                <Grid container spacing={2}>
                    <Grid className={classes.chartContainer} item xs={12} md={8}>
                        <ResponsiveBar
                            data={installationsBarData}
                            keys={[ 'android', 'ios' ]}
                            indexBy='date'
                            margin={{ top: 20, right: 20, bottom: 40, left: 60 }}
                            padding={0.1}
                            colors={getOSColor}
                            borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                tickValues: 'abc',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0
                            }}
                            axisLeft={{
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Installations',
                                legendPosition: 'middle',
                                legendOffset: -40
                            }}
                            labelSkipWidth={12}
                            labelSkipHeight={12}
                            labelTextColor={'white'}
                            animate={true}
                            motionConfig={'gentle'}
                            minValue={installationsBarData.length ? undefined : 0}
                            maxValue={installationsBarData.length ? undefined : 100}
                        />
                        <Box className={clsx(classes.noDataOverlay, installationsBarData.length ? undefined : classes.showNoDataOverlay)}>
                            No Data
                        </Box>
                    </Grid>

                    <Grid item md={4} className={clsx(classes.chartContainer, classes.hideXS)}>
                        {/* Place response pie chart in a container since otherwise it will
                            continually grow in height within a CSS grid*/}
                        <Box className={classes.responsivePieContainer}>
                            <ResponsivePie
                                data={isNoData ? [{ id: 'noData', label: 'No data', value: 100 }] : installationsPieData }
                                margin={{ top: 20, right: 60, bottom: 60, left: 20 }}
                                colors={getOSColor}
                                enableArcLabels={false}
                                enableArcLinkLabels={false}
                                arcLabel={d => `${d.label} (${d.value})`}
                                legends={[
                                    {
                                        anchor: 'bottom',
                                        direction: 'row',
                                        translateY: 40,
                                        translateX: 30,
                                        itemWidth: 100,
                                        itemHeight: 18,
                                        itemTextColor: '#999',
                                        symbolSize: 18,
                                        symbolShape: 'circle',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemTextColor: '#000'
                                                }
                                            }
                                        ]
                                    }
                                ]}
                                arcLabelsTextColor={'white'}
                            />
                        </Box>
                    </Grid>                    
                </Grid>

            </CardContent>
        </Card>
    );
};

export default Installations;