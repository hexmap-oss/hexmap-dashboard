/* 
 * src/views/metrics/metricsTypes.ts
 */

export const ChartColors: { [id: string] : string } = {
    android: '#3cde88',
    ios: '#35A7FF',
    noData: '#AAAAAA'
};
