/* 
 * src/views/metrics/metricsSlice.ts
 */
import { subDays } from 'date-fns';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import MetricsService, { ChartsParameters, ChartsResponse } from '../../api/metrics';
import { RootState } from '../../app/store';
import { isDebugMode } from '../../app/utils';
import {
    DetectedTripDatesCalendarItem,
    InstallationsBarChartItem,
    InstallationsPieChartItem,
    PromptResponsesBarItem,
    QuickStatsRow,
    UserActivityLineChartSeries
} from './metricsTypes';
import {
    testDetectedTripDatesData,
    testInstallationsBarData,
    testInstallationsPieData,
    testPromptLabels,
    testPromptResponsesBarData,
    testPromptResponsesBarLabels,
    testUserActivityLineData
} from './testMetricsData';

const initialNow = new Date();


interface MetricsState {
    endTime: number,
    isUpdating: boolean,
    maxTime: number,
    minTime: number,
    startTime: number,
    promptNum: number,
    quickStatsRows: QuickStatsRow[],
    installationsBarData: InstallationsBarChartItem[],
    installationsPieData: InstallationsPieChartItem[],
    userActivityLineData: UserActivityLineChartSeries[],
    detectedTripDatesCalendarData: DetectedTripDatesCalendarItem[],
    promptLabels: string[],
    promptResponsesBarData: PromptResponsesBarItem[],
    promptResponsesBarLabels: string[],
};

const testInitialState: MetricsState = {
    endTime: initialNow.getTime(),
    isUpdating: false,
    maxTime: initialNow.getTime(),
    minTime: subDays(initialNow, 7).getTime(),
    startTime: subDays(initialNow, 7).getTime(),
    promptNum: 0,
    quickStatsRows: [
        {name: 'Sign-ups in last 24 hours', value: 4},
        {name: 'Active users in past 15 minutes', value: 2},
        {name: 'Active users in past 24 hours', value: 5},
        {name: 'Number of points collected in last 24 hours', value: 284}
    ],
    installationsBarData: testInstallationsBarData,
    installationsPieData: testInstallationsPieData,
    userActivityLineData: testUserActivityLineData,
    detectedTripDatesCalendarData: testDetectedTripDatesData,
    promptLabels: testPromptLabels,
    promptResponsesBarData: testPromptResponsesBarData,
    promptResponsesBarLabels: testPromptResponsesBarLabels,
};

const initialState: MetricsState = {
    endTime: initialNow.getTime(),
    maxTime: initialNow.getTime(),
    minTime: subDays(initialNow, 7).getTime(),
    isUpdating: false,
    startTime: subDays(initialNow, 7).getTime(),
    promptNum: 0,
    quickStatsRows: [],
    installationsBarData: [],
    installationsPieData: [],
    userActivityLineData: [],
    detectedTripDatesCalendarData: [],
    promptLabels: [],
    promptResponsesBarData: [],
    promptResponsesBarLabels: [],
};

export const fetchChartMetrics = createAsyncThunk(
    'metrics/fetchChartData',
    async(params: ChartsParameters, thunkAPI) => {
        try {
            const response = await MetricsService.fetchChartsData(params);
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const metricsSlice = createSlice({
    name: 'metrics',
    initialState: isDebugMode() ? testInitialState : initialState,
    reducers: {
        setEndTime: (state, action: PayloadAction<number>) => {
            state.endTime = action.payload;
        },
        setStartTime: (state, action: PayloadAction<number>) => {
            state.startTime = action.payload;
        },
        setPromptNum: (state, action: PayloadAction<number>) => {
            state.promptNum = action.payload;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchChartMetrics.pending, (state, action) => {
            state.isUpdating = true;
        })
        .addCase(fetchChartMetrics.fulfilled, (state, action: PayloadAction<ChartsResponse>) => {
            state.isUpdating = false;
            if (action.payload.overview) {
                state.quickStatsRows = action.payload.overview;
            }
            state.installationsBarData = action.payload.installationsBarChart;
            state.installationsPieData = action.payload.installationsPieChart;
            state.userActivityLineData = action.payload.activeUsersLineChart;
            state.detectedTripDatesCalendarData = action.payload.detectedTripDatesCalendar;
            state.promptLabels = action.payload.promptLabels;
            state.promptResponsesBarData = action.payload.promptResponsesBarChart;
            state.promptResponsesBarLabels = action.payload.promptResponsesBarLabels;
        })
        .addCase(fetchChartMetrics.rejected, (state, action) => {
            state.isUpdating = false;
        })
});

export const { setEndTime, setPromptNum, setStartTime } = metricsSlice.actions;

export const selectDetectedTripDatesData = (state: RootState) => state.metrics.detectedTripDatesCalendarData;
export const selectEndTime = (state: RootState) => state.metrics.endTime;
export const selectInstallationsBarData = (state: RootState) => state.metrics.installationsBarData;
export const selectInstallationsPieData = (state: RootState) => state.metrics.installationsPieData;
export const selectIsUpdating = (state: RootState) => state.metrics.isUpdating;
export const selectMaxTime = (state: RootState) => state.metrics.maxTime;
export const selectMinTime = (state: RootState) => state.metrics.minTime;
export const selectPromptNum = (state: RootState) => state.metrics.promptNum;
export const selectPromptLabels = (state: RootState) => state.metrics.promptLabels;
export const selectPromptResponsesBarData = (state: RootState) => state.metrics.promptResponsesBarData;
export const selectPromptResponsesBarLabels = (state: RootState) => state.metrics.promptResponsesBarLabels;
export const selectQuickStatsRows = (state: RootState) => state.metrics.quickStatsRows;
export const selectStartTime = (state: RootState) => state.metrics.startTime;
export const selectUserActivityLineData = (state: RootState) => state.metrics.userActivityLineData;

export default metricsSlice.reducer;

