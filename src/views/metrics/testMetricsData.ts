/* 
 * src/views/metrics/testInstallationsData.tsx
 */

const metricsResponse = {
    status: 'success',
    type: 'MetricsSurveyOverview',
    results: {
        installationsBarChart: [
            {
                android: 1,
                date: '2021, Week 2',
                ios: 1
            },
            {
                android: 3,
                date: '2021, Week 3',
                ios: 3
            },
            {
                android: 0,
                date: '2021, Week 4',
                ios: 1
            },
            {
                android: 5,
                date: '2021, Week 5',
                ios: 1
            },
            {
                android: 5,
                date: '2021, Week 6',
                ios: 4
            },
            {
                android: 2,
                date: '2021, Week 7',
                ios: 1
            },
            {
                android: 0,
                date: '2021, Week 8',
                ios: 0
            },
            {
                android: 1,
                date: '2021, Week 9',
                ios: 0
            }
        ],
        installationsPieChart: [
            {
                id: 'android',
                label: 'Android',
                value: 17
            },
            {
                id: 'ios',
                label: 'iOS',
                value: 11
            }
        ],
        activeUsersLineChart: [
            {
                id: 'Android',
                data: [
                    {
                        x: '2021-03-09T18:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T19:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T20:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T21:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T22:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T23:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T00:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T01:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T02:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T03:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T04:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T05:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T06:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T07:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T08:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T09:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T10:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T11:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T12:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T13:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T14:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T15:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T16:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T17:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T18:00:00Z',
                        y: 0
                    }
                ],
                maxY: 0
            },
            {
                id: 'iOS',
                data: [
                    {
                        x: '2021-03-09T18:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T19:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T20:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T21:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-09T22:00:00Z',
                        y: 1
                    },
                    {
                        x: '2021-03-09T23:00:00Z',
                        y: 1
                    },
                    {
                        x: '2021-03-10T00:00:00Z',
                        y: 1
                    },
                    {
                        x: '2021-03-10T01:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T02:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T03:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T04:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T05:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T06:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T07:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T08:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T09:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T10:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T11:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T12:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T13:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T14:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T15:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T16:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T17:00:00Z',
                        y: 0
                    },
                    {
                        x: '2021-03-10T18:00:00Z',
                        y: 0
                    }
                ],
                maxY: 1
            }
        ],
        promptLabels: [
            'mode',
            'purpose'
        ],
        promptResponsesBarChart: [
            {
                Bicycle: 0,
                Bus: 0,
                Car: 5,
                Subway: 0,
                Walk: 0,
                date: '2021-01-16'
            },
            {
                Bicycle: 1,
                Bus: 0,
                Car: 1,
                Subway: 1,
                Walk: 2,
                date: '2021-01-17'
            },
            {
                Bicycle: 0,
                Bus: 0, 
                Car: 1,
                Subway: 0,
                Walk: 0,            
                date: '2021-01-18'
            },
            {
                Bicycle: 0,
                Bus: 0,         
                Car: 1,
                Subway: 0,
                Walk: 0,                
                date: '2021-01-20'
            },
            {
                Bicycle: 0,
                Bus: 0,                
                Car: 1,
                Subway: 0,
                Walk: 0,                
                date: '2021-01-21'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 1,
                Subway: 0,
                Walk: 0,
                date: '2021-01-22'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 1,
                Subway: 0,
                Walk: 0,
                date: '2021-01-23'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 1,
                Walk: 1,
                date: '2021-01-24'
            },
            {
                Bicycle: 0,
                Bus: 1,
                Car: 0,
                Subway: 2,
                Walk: 2,
                date: '2021-01-25'
            },
            {
                Bicycle: 1,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 0,
                date: '2021-01-26'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-03'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 1,
                Subway: 0,
                Walk: 5,
                date: '2021-02-04'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 3,
                Subway: 0,
                Walk: 0,
                date: '2021-02-05'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 3,
                Subway: 2,
                Walk: 1,
                date: '2021-02-08'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 2,
                Walk: 1,
                date: '2021-02-09'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-10'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-11'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-12'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-19'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-23'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 1,
                date: '2021-02-24'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 1,
                Walk: 3,
                date: '2021-02-26'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 1,
                Walk: 1,
                date: '2021-02-27'
            },
            {
                Bicycle: 0,
                Bus: 0,
                Car: 0,
                Subway: 0,
                Walk: 3,
                date: '2021-03-04'
            },
            {
                Bicycle: 0,
                Bus: 1,
                Car: 0,
                Subway: 1,
                Walk: 2,
                date: '2021-03-05'
            }
        ],
        promptResponsesBarLabels: [
            'Car',
            'Walk',
            'Bicycle',
            'Subway',
            'Bus',
            'Other'
        ],
        detectedTripDatesCalendar: [
            {
                day: '2021-01-11',
                value: 1
            },
            {
                day: '2021-01-12',
                value: 1
            },
            {
                day: '2021-01-16',
                value: 1
            },
            {
                day: '2021-01-17',
                value: 1
            },
            {
                day: '2021-01-18',
                value: 1
            },
            {
                day: '2021-01-19',
                value: 1
            },
            {
                day: '2021-01-20',
                value: 1
            },
            {
                day: '2021-01-21',
                value: 1
            },
            {
                day: '2021-01-22',
                value: 1
            },
            {
                day: '2021-01-23',
                value: 1
            },
            {
                day: '2021-01-24',
                value: 1
            },
            {
                day: '2021-01-25',
                value: 1
            },
            {
                day: '2021-01-26',
                value: 1
            },
            {
                day: '2021-01-27',
                value: 1
            },
            {
                day: '2021-01-28',
                value: 1
            },
            {
                day: '2021-01-29',
                value: 1
            },
            {
                day: '2021-01-30',
                value: 1
            },
            {
                day: '2021-01-31',
                value: 1
            },
            {
                day: '2021-02-01',
                value: 1
            },
            {
                day: '2021-02-02',
                value: 1
            },
            {
                day: '2021-02-03',
                value: 1
            },
            {
                day: '2021-02-04',
                value: 1
            },
            {
                day: '2021-02-05',
                value: 1
            },
            {
                day: '2021-02-06',
                value: 1
            },
            {
                day: '2021-02-07',
                value: 1
            },
            {
                day: '2021-02-08',
                value: 1
            },
            {
                day: '2021-02-09',
                value: 1
            },
            {
                day: '2021-02-10',
                value: 1
            },
            {
                day: '2021-02-11',
                value: 1
            },
            {
                day: '2021-02-12',
                value: 1
            },
            {
                day: '2021-02-13',
                value: 1
            },
            {
                day: '2021-02-14',
                value: 1
            },
            {
                day: '2021-02-15',
                value: 1
            },
            {
                day: '2021-02-16',
                value: 1
            },
            {
                day: '2021-02-17',
                value: 1
            },
            {
                day: '2021-02-18',
                value: 1
            },
            {
                day: '2021-02-19',
                value: 1
            },
            {
                day: '2021-02-20',
                value: 1
            },
            {
                day: '2021-02-21',
                value: 1
            },
            {
                day: '2021-02-22',
                value: 1
            },
            {
                day: '2021-02-23',
                value: 1
            },
            {
                day: '2021-02-24',
                value: 1
            },
            {
                day: '2021-02-25',
                value: 1
            },
            {
                day: '2021-02-26',
                value: 1
            },
            {
                day: '2021-02-27',
                value: 1
            },
            {
                day: '2021-02-28',
                value: 1
            },
            {
                day: '2021-03-01',
                value: 1
            },
            {
                day: '2021-03-02',
                value: 1
            },
            {
                day: '2021-03-03',
                value: 1
            },
            {
                day: '2021-03-04',
                value: 1
            },
            {
                day: '2021-03-05',
                value: 1
            },
            {
                day: '2021-03-06',
                value: 1
            },
            {
                day: '2021-03-08',
                value: 1
            },
            {
                day: '2021-03-09',
                value: 1
            }
        ]
    }
};

export const testInstallationsBarData = metricsResponse.results.installationsBarChart;
export const testInstallationsPieData = metricsResponse.results.installationsPieChart;
export const testUserActivityLineData = metricsResponse.results.activeUsersLineChart;
export const testPromptLabels = metricsResponse.results.promptLabels;
export const testPromptResponsesBarData = metricsResponse.results.promptResponsesBarChart;
export const testPromptResponsesBarLabels = metricsResponse.results.promptResponsesBarLabels;
export const testDetectedTripDatesData = metricsResponse.results.detectedTripDatesCalendar;
