/* 
 * src/views/metrics/QuickStats.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import { selectQuickStatsRows } from './metricsSlice';


enum RowLabels {
    'metrics.statsTable.signups24hr' = 'Sign-ups in last 24 hours',
    'metrics.statsTable.activeUsers15min' = 'Active users in past 15 minutes',
    'metrics.statsTable.activeUsers24hr' = 'Active users in past 24 hours',
    'metrics.statsTable.numPoints24hr' = 'Number of points collected in last 24 hours'
};


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minHeight: 309,
            minWidth: '100%',
        },
        title: {
            borderBottomWidth: 1,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.grey[300]
        },
        tableRow: {
            '& .MuiTableCell-root.MuiTableCell-body': {
                borderBottom: 'none'
            }
        }
    })
);

const QuickStats = () => {
    const classes = useStyles();
    const rows = useSelector(selectQuickStatsRows);

    return (
        <Card className={classes.root}>
            <CardContent>
                <Box className={classes.title}>
                    <Typography variant='subtitle1'>Recent activity</Typography>
                    <Typography variant='h5'>Survey metrics</Typography>
                </Box>

                <Table>
                    <TableBody>
                    {rows.map((row) => (
                        <TableRow className={classes.tableRow} key={row.name}>
                            <TableCell component="th" scope="row">
                                { RowLabels[row.name as keyof typeof RowLabels] }
                            </TableCell>
                            <TableCell align="right">{row.value}</TableCell>
                        </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </CardContent>
        </Card>
    );
};

export default QuickStats;
