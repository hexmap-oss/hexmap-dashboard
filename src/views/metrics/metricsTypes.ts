/* 
 * src/views/metrics/metricsTypes.ts
 */
export type QuickStatsRow = {
    name: string
    value: string | number
}

export type InstallationsBarChartItem = {
    android: number
    ios: number
    date: string
}

export type InstallationsPieChartItem = {
    id: string
    label: string
    value: number
};

export type InstallationsLineChartItem = {
    x: string
    y: number
};

type UserActivityLineChartItem = {
    x: string
    y: number
}

export type UserActivityLineChartSeries = {
    id: string
    data: UserActivityLineChartItem[]
    maxY: number
}

export type DetectedTripDatesCalendarItem = {
    day: string
    value: number
}


export type PromptResponsesBarItem = {
    [key: string]: any,
}

// updates to nivo library pertaining to ResponsiveBar have made this more
// polymorphic of a definition than wished
export type ChartSliceType = {
    id: string | number,
    value?: number | null,
    indexValue?: string | number,
    maxY?: number,
    data: InstallationsBarChartItem[] | InstallationsPieChartItem | InstallationsLineChartItem[] | InstallationsBarChartItem
};
