/* 
 * src/views/metrics/UserActivity.tsx
 */
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { ResponsiveLine } from '@nivo/line';

import { ChartColors } from './metricsConstants';
import { ChartSliceType } from './metricsTypes';
import { useSelector } from 'react-redux';
import { selectUserActivityLineData } from './metricsSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        chartContainer: {
            height: 280,
        },
    })
);

interface UserActivityProps {};

const UserActivity = (props: UserActivityProps) => {
    const classes = useStyles();
    const userActivityLineData = useSelector(selectUserActivityLineData);
    
    const getOSColor = (slice: ChartSliceType) => {
        const key = (slice.id as string).toLowerCase();
        return ChartColors[key];
    }

    const calculateYTicks = (chartData: ChartSliceType[]) => {
        let ticks = 1;
        chartData.forEach(series => {
            ticks = Math.max(ticks, series.maxY!)
        });
        ticks = ticks > 10 ? 10 : ticks;
        return ticks;
    }

    return (
        <Card>
            <CardContent>
                <Typography>Last 24hr Active Users</Typography>

                <Box className={classes.chartContainer}>
                    <ResponsiveLine
                        data={userActivityLineData}
                        margin={{ top: 50, right: 160, bottom: 80, left: 60 }}
                        xScale={{
                            format: '%Y-%m-%dT%H:%M:%S%Z',
                            type: 'time'
                        }}
                        xFormat='time:%Y-%m-%dT%H:%M:%S%Z'
                        yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            tickValues: "every 3 hours",
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: -35,
                            format: '%I:%M %p',
                            legend: 'Time',
                            legendOffset: 60,
                            legendPosition: 'middle'
                        }}
                        axisLeft={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            tickValues: calculateYTicks(userActivityLineData),
                            legend: 'Count',
                            legendOffset: -40,
                            legendPosition: 'middle',
                        }}
                        enableGridX={true}
                        colors={getOSColor}
                        curve={'monotoneX'}
                        enableGridY={false}
                        layers={['grid', 'markers', 'axes', 'areas', 'crosshair', 'lines', 'points', 'slices', 'mesh', 'legends']}
                        lineWidth={3}
                        pointSize={6}
                        pointColor={{ from: 'color', modifiers: [] }}
                        pointBorderWidth={2}
                        pointBorderColor={{ from: 'serieColor' }}
                        useMesh={true}
                        legends={[
                            {
                                anchor: 'bottom-right',
                                direction: 'column',
                                justify: false,
                                translateX: 100,
                                translateY: 0,
                                itemsSpacing: 0,
                                itemDirection: 'left-to-right',
                                itemWidth: 80,
                                itemHeight: 20,
                                itemOpacity: 0.75,
                                symbolSize: 12,
                                symbolShape: 'circle',
                                symbolBorderColor: 'rgba(0, 0, 0, .5)',
                                effects: [
                                    {
                                        on: 'hover',
                                        style: {
                                            itemBackground: 'rgba(0, 0, 0, .03)',
                                            itemOpacity: 1
                                        }
                                    }
                                ]
                            }
                        ]}
                    />
                </Box>
            </CardContent>
        </Card>
    );
};

export default UserActivity;
