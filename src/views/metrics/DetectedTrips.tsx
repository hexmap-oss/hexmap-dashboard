/* 
 * src/views/metrics/DetectedTripsMetrics.tsx
 */
import { startOfYear } from 'date-fns';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { ResponsiveCalendar } from '@nivo/calendar';

import { useSelector } from 'react-redux';
import { selectDetectedTripDatesData } from './metricsSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        chartContainer: {
            height: 280,
        },
    })
);

const DetectedTripsMetrics = () => {
    const classes = useStyles();
    const detectedTripDatesData = useSelector(selectDetectedTripDatesData);
    const now = new Date();

    return (
        <Card>
            <CardContent>
                Detected Trip Dates
                <Box className={classes.chartContainer}>
                    <ResponsiveCalendar
                        data={detectedTripDatesData}
                        from={startOfYear(now)}
                        to={now}
                        emptyColor={'#fff'}
                        colors={[ '#8a8aa1' ]}
                        margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
                        yearSpacing={40}
                        monthBorderWidth={4}
                        monthBorderColor={'#ebf3ff'}
                        dayBorderWidth={3}
                        dayBorderColor={'#ebf3ff'}
                    />
                </Box>
            </CardContent> 
        </Card>  
    );
};

export default DetectedTripsMetrics;
