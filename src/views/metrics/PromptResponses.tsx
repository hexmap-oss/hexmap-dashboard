/* 
 * src/views/metrics/PromptResponses.tsx
 */
import { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import { ResponsiveBar } from '@nivo/bar';

import { selectPromptLabels, selectPromptResponsesBarData, selectPromptResponsesBarLabels, setPromptNum } from './metricsSlice';
import { capitalize, stringArraysEqual } from '../../app/utils';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        title: {
            display: 'inline-block'
        },
        chartContainer: {
            height: 280,
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
            float: 'right',
            zIndex: 1,
        },        
    })
);

function usePrevious(value: string[]) {
    const ref = useRef<string[]>();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

const PromptResponses = () => {
    const classes = useStyles();
    const promptLabels = useSelector(selectPromptLabels);
    const promptResponsesBarData = useSelector(selectPromptResponsesBarData);
    const promptResponsesBarLabels = useSelector(selectPromptResponsesBarLabels);
    const [selectedData, setSelectedData] = useState('');
    const prevPromptLabels = usePrevious(promptLabels);
    const dispatch = useDispatch();

    useEffect(() => {
        const labelsHaveChanged = prevPromptLabels
            ? !stringArraysEqual(prevPromptLabels, promptLabels)
            : true;

        if (labelsHaveChanged && promptLabels[0]) {
            setSelectedData(promptLabels[0]);
        }
    }, [promptLabels]);

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        const selectedLabel = event.target.value as string;
        const selectedLabelNum = promptLabels.indexOf(selectedLabel);
        setSelectedData(selectedLabel);
        dispatch(setPromptNum(selectedLabelNum));
    }
    
    return (
        <Card>
            <CardContent>
                <Typography className={classes.title}>Prompt Responses</Typography>
                <FormControl className={classes.formControl}>
                    <Select
                        labelId='demo-simple-select-helper-label'
                        id='demo-simple-select-helper'
                        value={selectedData}
                        onChange={handleChange}
                    >
                        { promptLabels.map((label: string, i: number) => (
                            <MenuItem key={i} value={label}>{capitalize(label)}</MenuItem>
                        ))}
                    </Select>
                    <FormHelperText>Chart Data</FormHelperText>
                </FormControl>

                <Box className={classes.chartContainer}>
                    <ResponsiveBar
                        data={promptResponsesBarData}
                        keys={promptResponsesBarLabels}
                        indexBy='date'
                        margin={{ top: 20, right: 140, bottom: 100, left: 50 }}
                        padding={0.4}
                        groupMode='grouped'
                        colors={{scheme: 'set1'}}
                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: -45,
                        }}
                        axisLeft={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: 'Responses',
                            legendPosition: 'middle',
                            legendOffset: -40
                        }}
                        enableLabel={false}
                        labelSkipWidth={12}
                        labelSkipHeight={12}
                        labelTextColor={'white'}
                        legends={[
                            {
                                dataFrom: 'keys',
                                anchor: 'bottom-right',
                                direction: 'column',
                                justify: false,
                                translateX: 120,
                                translateY: 0,
                                itemsSpacing: 2,
                                itemWidth: 100,
                                itemHeight: 20,
                                itemDirection: 'left-to-right',
                                itemOpacity: 0.85,
                                symbolSize: 20,
                                effects: [
                                    {
                                        on: 'hover',
                                        style: {
                                            itemOpacity: 1
                                        }
                                    }
                                ]
                            }
                        ]}
                        animate={true}
                        motionConfig={'gentle'}
                    />                    
                </Box>
            </CardContent>
        </Card>
    );
};

export default PromptResponses;
