/* 
 * src/views/metrics/MetricsTimeSelector.tsx
 */
import { useSelector, useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';

import DateRangePicker from '../../components/widgets/DateRangePicker';
import DateRangeButtonSelect from '../../components/widgets/DateRangeButtonSelect';
import { 
    fetchChartMetrics,
    selectMinTime,
    selectMaxTime,
    selectStartTime,
    selectEndTime,
    setStartTime,
    setEndTime,
    selectPromptNum,
    selectQuickStatsRows
} from './metricsSlice';
import { useEffect } from 'react';


const MetricsTimeSelector = () => {
    const minTime = useSelector(selectMinTime);
    const maxTime = useSelector(selectMaxTime);
    const startTime = useSelector(selectStartTime);
    const endTime = useSelector(selectEndTime);
    const promptNum = useSelector(selectPromptNum);
    const quickStatsRows = useSelector(selectQuickStatsRows);
    const dispatch = useDispatch();

    useEffect(() => {
        if (startTime && endTime) {
            dispatch(fetchChartMetrics({
                start: Math.floor(startTime / 1000),
                end: Math.ceil(endTime / 1000),
                countsTable: quickStatsRows.length === 0,
                promptNum,
            }));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps 
    }, [dispatch, startTime, endTime, promptNum]);

    const setTimes = (newStartTime: number, newEndTime: number) => {
        dispatch(setStartTime(newStartTime));
        dispatch(setEndTime(newEndTime));
    }

    return (
        <Grid container>
            <Grid item sm={12} md={4}>
                <DateRangePicker
                    minDate={minTime}
                    maxDate={maxTime}
                    startDate={startTime}
                    endDate={endTime}
                    setDates={setTimes}
                />
            </Grid>

            <Grid item sm={12} md={8}>
                <DateRangeButtonSelect
                    setDates={setTimes}
                />
            </Grid>
        </Grid>
    );
}


export default MetricsTimeSelector;
