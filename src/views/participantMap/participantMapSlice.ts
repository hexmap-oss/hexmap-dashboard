/* 
 * src/views/participantMap/participantMapSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../../app/store';
import ParticipantMapService, { ParticipantCoordinatesParameters, ParticipantCoordinatesResponse, ParticipantUuidsResponse } from '../../api/participantMap';
import { ParticipantUUID } from './participantMapTypes';

interface OrganizationUsersCodeState {
    allUuids: ParticipantUUID[];
    endTime: number;
    isFetchingUuids: boolean;
    isFirstApiCall: boolean;
    lastDetectedTrip?: number;
    lastPipelineRun?: number;
    startTime: number;
    userCollectionStart: number;
    userCollectionEnd: number;
    userCanceledPrompts?: GeoJSON.FeatureCollection<GeoJSON.Point>;
    userCoordinates?: GeoJSON.FeatureCollection<GeoJSON.LineString>;
    userPromptResponses?: GeoJSON.FeatureCollection<GeoJSON.Point>;
    uuid: ParticipantUUID | null;
};

const testInitialState: OrganizationUsersCodeState = {
    allUuids: [],
    isFetchingUuids: false,
    isFirstApiCall: true,
    endTime: new Date(2021, 0, 1, 23, 59, 59).getTime(),
    startTime: new Date(2021, 0, 1, 0, 0, 0).getTime(),
    userCollectionEnd: new Date(2021, 0, 1, 23, 59, 59).getTime(),
    userCollectionStart: new Date(2021, 0, 1, 0, 0, 0).getTime(),
    uuid: null,
};


export const fetchUuids = createAsyncThunk(
    'participantMap/fetchUserIds',
    async(_, thunkAPI) => {
        try {
            const response = await ParticipantMapService.fetchParticipantUuids();
            return response;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const fetchUserCoordinatesUnbounded = createAsyncThunk(
    'participantMap/fetchUserCoordinatesUnbounded',
    async(params: ParticipantCoordinatesParameters, thunkAPI) => {
        try {
            const response = await ParticipantMapService.fetchParticipantCoordinates(params);
            return response;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)

export const fetchUserCoordinates = createAsyncThunk(
    'participantMap/fetchUserCoordinates',
    async(params: ParticipantCoordinatesParameters, thunkAPI) => {
        try {
            const response = await ParticipantMapService.fetchParticipantCoordinates(params);
            return response;
        }
        catch (error) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }    
)


export const participantMapSlice = createSlice({
    name: 'participantMap',
    initialState: testInitialState,
    reducers: {
        setEndTime: (state, action: PayloadAction<number>) => {
            state.endTime = action.payload;
            state.isFirstApiCall = false;
        },
        setStartTime: (state, action: PayloadAction<number>) => {
            state.startTime = action.payload;
            state.isFirstApiCall = false;
        },
        setUuid: (state, action: PayloadAction<ParticipantUUID>) => {
            state.uuid = action.payload;
        }
    },
    extraReducers: builder => builder
        .addCase(fetchUuids.pending, (state, action) => {
            state.isFetchingUuids = true;
            state.allUuids = [];
        })
        .addCase(fetchUuids.fulfilled, (state, action: PayloadAction<ParticipantUuidsResponse>) => {
            state.isFetchingUuids = false;
            state.lastDetectedTrip = action.payload.lastDetectedTrip;
            state.lastPipelineRun = action.payload.lastPipelineRun;
            state.allUuids = action.payload.uuids;
        })  
        .addCase(fetchUserCoordinatesUnbounded.pending, (state, action) => {})
        .addCase(fetchUserCoordinatesUnbounded.fulfilled, (state, action: PayloadAction<ParticipantCoordinatesResponse>) => {
            state.startTime = action.payload.searchStart * 1000;
            state.endTime = action.payload.searchEnd * 1000;
            state.isFirstApiCall = true;

            state.userCollectionStart = action.payload.collectionStart * 1000;
            state.userCollectionEnd = action.payload.collectionEnd * 1000;
            state.userCanceledPrompts = action.payload.cancelledPrompts;
            state.userCoordinates = action.payload.points;
            state.userPromptResponses = action.payload.promptResponses;
        })
        .addCase(fetchUserCoordinates.pending, (state, action) => {})
        .addCase(fetchUserCoordinates.fulfilled, (state, action: PayloadAction<ParticipantCoordinatesResponse>) => {
            state.userCollectionStart = action.payload.collectionStart * 1000;
            state.userCollectionEnd = action.payload.collectionEnd * 1000;
            state.userCanceledPrompts = action.payload.cancelledPrompts;
            state.userCoordinates = action.payload.points;
            state.userPromptResponses = action.payload.promptResponses;
        })
});

export const { setUuid, setStartTime, setEndTime } = participantMapSlice.actions

export const selectAllUuids = (state: RootState) => state.participantMap.allUuids;
export const selectEndTime = (state: RootState) => state.participantMap.endTime;
export const selectIsFetchingUuids = (state: RootState) => state.participantMap.isFetchingUuids;
export const selectIsFirstApiCall = (state: RootState) => state.participantMap.isFirstApiCall;
export const selectLastDetectedTrip = (state: RootState) => state.participantMap.lastDetectedTrip;
export const selectLastPipelineRun = (state: RootState) => state.participantMap.lastPipelineRun;
export const selectStartTime = (state: RootState) => state.participantMap.startTime;
export const selectUserCollectionStartTime = (state: RootState) => state.participantMap.userCollectionStart;
export const selectUserCollectionEndTime = (state: RootState) => state.participantMap.userCollectionEnd;
export const selectUserCanceledPrompts = (state: RootState) => state.participantMap.userCanceledPrompts;
export const selectUserCoordinates = (state: RootState) => state.participantMap.userCoordinates;
export const selectUserPromptResponses = (state: RootState) => state.participantMap.userPromptResponses;
export const selectUuid = (state: RootState) => state.participantMap.uuid;

export default participantMapSlice.reducer;


