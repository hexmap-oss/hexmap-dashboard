/* 
 * src/views/participantMap/testSurveyUsersData.tsx
 */


const surveyUsersResponse = {
    status: "success",
    type: "MobileUserList",
    results: {
        lastPipelineRun: "2021-03-11T17:05:04Z",
        lastDetectedTrip: "2021-03-10T22:44:57Z",
        uuids: [
            {
                created_at: "2021-02-04T18:06:00Z",
                uuid: "8c5a5292-24f7-4147-9f95-d0f308fc61bc"
            },
            {
                created_at: "2021-03-04T16:37:35Z",
                uuid: "467859cc-f582-4abb-8a0a-c3ebbc085e93"
            },
            {
                created_at: "2021-02-12T04:59:10Z",
                uuid: "1A6E95E6-4E9C-4257-BA38-445CCE3FD645"
            },
            {
                created_at: "2021-01-24T19:10:08Z",
                uuid: "cb13448b-da29-4edd-83a4-553cf6965f1d"
            },
            {
                created_at: "2021-02-03T03:12:28Z",
                uuid: "f83e9d19-8950-477a-930a-eb8c37574d18"
            },
            {
                created_at: "2021-01-24T02:49:14Z",
                uuid: "87DB5915-EC48-49E1-917E-E0C9916970B4"
            },
            {
                created_at: "2021-01-24T18:41:53Z",
                uuid: "707ac621-9b15-43bb-a621-770f2ce6fd06"
            },
            {
                created_at: "2021-02-19T22:56:48Z",
                uuid: "4f74d77a-f907-479a-85b5-db208d4b3041"
            },
            {
                created_at: "2021-01-23T18:54:24Z",
                uuid: "6B863021-6893-4DC7-8B4D-2017CA7C77B8"
            },
            {
                created_at: "2021-02-04T17:56:02Z",
                uuid: "b1f78532-d518-4891-8010-5d0066fbd43e"
            },
            {
                created_at: "2021-02-11T16:05:23Z",
                uuid: "0D7308E1-9143-405C-BEA8-AFC6F34FDEBA"
            },
            {
                created_at: "2021-01-11T15:31:43Z",
                uuid: "C900C534-E41B-46CE-B325-45D4086C105A"
            },
            {
                created_at: "2021-02-13T20:30:01Z",
                uuid: "AB1DF18F-DEB7-47C0-BE80-CCC39047B65E"
            },
            {
                created_at: "2021-02-10T23:47:56Z",
                uuid: "fa667ba2-2012-4886-a662-d615756206cc"
            },
            {
                created_at: "2020-02-17T19:25:00Z",
                uuid: "CE97A72A-71B6-4AA7-B3CF-4227A6AE34BE"
            },
            {
                created_at: "2021-02-10T20:56:41Z",
                uuid: "9ab90e56-108c-4ee7-8aaa-4c7a021588f6"
            },
            {
                created_at: "2021-01-17T16:13:36Z",
                uuid: "80ed894f-d3be-4d1e-8d4a-cbea22d4debc"
            },
            {
                created_at: "2021-02-04T19:22:25Z",
                uuid: "457e7610-3e59-49af-a55c-c5b8c6499620"
            },
            {
                created_at: "2021-01-25T20:49:54Z",
                uuid: "529E6772-DF2F-416B-9167-B432EDD59625"
            },
            {
                created_at: "2021-02-03T03:14:24Z",
                uuid: "BF955D71-6127-430A-B4F0-4EB1E4BF2B8C"
            },
            {
                created_at: "2021-02-09T18:03:50Z",
                uuid: "3d23bca6-2f1c-4600-8059-a9fe26bba5e2"
            },
            {
                created_at: "2021-01-24T22:56:24Z",
                uuid: "4a890958-b0f2-4e8f-9451-9cb403eba53d"
            },
            {
                created_at: "2021-01-23T18:52:05Z",
                uuid: "892D40C9-6EB5-4F10-84F1-61D3F086892E"
            },
            {
                created_at: "2021-02-12T15:06:02Z",
                uuid: "b82a58f6-1949-4c35-b3d9-e723f9318389"
            }
        ]
    }
}

export const lastPipelineRun = surveyUsersResponse.results.lastPipelineRun;
export const lastDetectedTrip = surveyUsersResponse.results.lastDetectedTrip;
export const uuids = surveyUsersResponse.results.uuids;
