/* 
 * src/views/participantMap/ParticipantMap.tsx
 */
import { useState, useEffect, useMemo, useRef} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { format } from 'date-fns';
import { LineString, MultiLineString } from 'geojson';
import L, {
    Layer,
    LatLng,
    LatLngExpression,
    Map as LeafletMap,
    Polyline as LeafletPolyline
} from 'leaflet';
import {
    CircleMarker,
    GeoJSON as GeoJSONLayer,
    LayerGroup,
    MapContainer,
    Polyline,
    TileLayer,
    Tooltip,
    ZoomControl
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

import ParticipantMapSelect from './ParticipantMapSelect';
import {
    fetchUserCoordinates,
    fetchUserCoordinatesUnbounded,
    fetchUuids,
    selectEndTime,
    selectIsFirstApiCall,
    selectStartTime,
    selectUserCanceledPrompts,
    selectUserCoordinates,
    selectUserPromptResponses,
    selectUuid,
} from './participantMapSlice';
import ParticipantMapStyle from './participantMapStyles';


const mapTileLayerURL = 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png';


function createUserCanceledPromptLayer(userCanceledPrompts: GeoJSON.FeatureCollection<GeoJSON.Point>) {
    const createMarker = (feature: GeoJSON.Feature, latLng: LatLng) => {
        return L.circleMarker(latLng, ParticipantMapStyle.featureStyles.prompts.canceled)
    };

    const uniqueKey = 'canceled-prompts-layer-' + Math.random().toString().substring(6);
    return (
        <GeoJSONLayer
            key={uniqueKey}
            pointToLayer={createMarker}
            data={userCanceledPrompts}
        />
    );
}

function createUserPromptLayer(userPromptResponses: GeoJSON.FeatureCollection<GeoJSON.Point>) {
    const createMarker = (feature: GeoJSON.Feature, latLng: LatLng) => {
        return L.circleMarker(latLng, ParticipantMapStyle.featureStyles.prompts.responses)
    };

    const showPromptInfo = (feature: GeoJSON.Feature, layer: Layer) => {
        let template = `<p><b>Displayed:</b> ${format(feature.properties?.displayedAt * 1000, 'yyyy-MM-dd HH:mm:ss (xx)')}</p</br>
                        <p><b>Recorded:</b> ${format(feature.properties?.recordedAt * 1000, 'yyyy-MM-dd HH:mm:ss (xx)')}</p</br>
                        <p><b>Responses:</b></p</br>`;
        
        feature.properties?.responses.forEach((response: string, i: number) => {
            const rowNum = i + 1,
                  responseLine = `<p><b>&emsp;&emsp;${rowNum}.</b> ${response}</p>`
            template = template + responseLine;
            layer.bindPopup(template);
        })
            
    }

    const uniqueKey = 'prompt-responses-layer-' + Math.random().toString().substring(6);
    return (
        <GeoJSONLayer
            key={uniqueKey}
            pointToLayer={createMarker}
            data={userPromptResponses}
            onEachFeature={showPromptInfo}
        />
    );
}

function createUserCoordinatesLinestringLayer(userCoordinates: GeoJSON.FeatureCollection<GeoJSON.LineString>, polylineRef: any) {
    const coordinates = userCoordinates.features[0].geometry.coordinates;
    const latLngCoordinates: LatLngExpression[] = coordinates.map(c => [c[1], c[0]]);

    const polyline = (
        <Polyline
            key={'gps-coordinates-layer'}
            positions={latLngCoordinates}
            ref={polylineRef}
            { ...ParticipantMapStyle.featureStyles.gpsPoints.polyline }
        />
    );
    return polyline;
}


function createUserCoordinatesPointsLayer(userCoordinates: GeoJSON.FeatureCollection<GeoJSON.LineString>) {
    const pointsFeature = userCoordinates.features[0];
    const modeColors = ParticipantMapStyle.featureStyles.gpsPoints.modeColors;

    const markerArray = pointsFeature.geometry.coordinates.map((lngLat, i) => {
        let fillColor = ParticipantMapStyle.featureStyles.gpsPoints.marker.fillColor;

        const pointMode = pointsFeature.properties?.modesDetected[i] as keyof typeof modeColors;
        if (ParticipantMapStyle.featureStyles.gpsPoints.modeColors.hasOwnProperty(pointMode)) {
            fillColor = ParticipantMapStyle.featureStyles.gpsPoints.modeColors[pointMode].color;
        }

        return (
            <CircleMarker key={"gps-point-" + i}
                          center={[lngLat[1], lngLat[0]]}
                          { ...ParticipantMapStyle.featureStyles.gpsPoints.marker }
                          fillColor={fillColor}>
                <Tooltip>
                    <p className={"tooltip-text"}>{format(pointsFeature.properties?.timestamps[i] * 1000, 'yyyy-MM-dd HH:mm:ss (xx)')}</p>
                    <p className={"tooltip-text"}>{pointsFeature.properties?.modesDetected[i]}</p>
                </Tooltip>
            </CircleMarker>
        )
    });

    var markerGroup;
    if (markerArray.length === 1) {
        markerGroup = markerArray[0];
    } else {
        const uniqueKey = 'coordinates-' + Math.random().toString().substring(6);
        markerGroup = (<LayerGroup key={uniqueKey}>{markerArray}</LayerGroup>);
    }
    return markerGroup;
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            position: 'relative',
        },
        map: {
            height: 'calc(100vh - 124px)'
        },
        participantSelect: {
            position: 'absolute',
            left: theme.spacing(1),
            width: '24em',
            zIndex: 1001,
        }
    })
);


const ParticipantMap = () => {
    const classes = useStyles();
    const [center] = useState<LatLngExpression>([45.5, -73.5]);
    const [zoom] = useState(13);
    const selectedUuid = useSelector(selectUuid);
    const startTime = useSelector(selectStartTime);
    const endTime = useSelector(selectEndTime);
    const isFirstApiCall = useSelector(selectIsFirstApiCall);
    const userCanceledPrompts = useSelector(selectUserCanceledPrompts);
    const userCoordinates = useSelector(selectUserCoordinates);
    const userPromptResponses = useSelector(selectUserPromptResponses);
    const mapRef = useRef<LeafletMap>();
    const polylineRef = useRef<LeafletPolyline<LineString | MultiLineString, any>>();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUuids());
    }, [dispatch]);

    // fetch new data
    useEffect(() => {
        if (selectedUuid) {
            dispatch(fetchUserCoordinatesUnbounded({
                userId: selectedUuid.uuid,
                tzBrowser: Intl.DateTimeFormat().resolvedOptions().timeZone,
            }));
        }
    }, [dispatch, selectedUuid]);

    useEffect(() => {
        if (selectedUuid && !isFirstApiCall) {
            dispatch(fetchUserCoordinates({
                userId: selectedUuid.uuid,
                startTime: Math.floor(startTime / 1000),
                endTime: Math.floor(endTime / 1000),
                tzBrowser: Intl.DateTimeFormat().resolvedOptions().timeZone,
            }));            
        }
        // Note: `selectedUuid`, `isFirstApiCall` intentionally excluded as dependencies
        // since this effect should only trigger when times are changed
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, startTime, endTime])

    // center map on new data
    useEffect(() => {
        const bounds = polylineRef.current?.getBounds();
        if (bounds && bounds.isValid()) mapRef.current?.fitBounds(bounds);
    }, [userCoordinates]);

    const layers = useMemo(() => {
        const layers: JSX.Element[] = [];
        if (userCanceledPrompts) layers.push(createUserCanceledPromptLayer(userCanceledPrompts));
        if (userPromptResponses) layers.push(createUserPromptLayer(userPromptResponses));
        if (userCoordinates) layers.push(createUserCoordinatesLinestringLayer(userCoordinates, polylineRef));
        if (userCoordinates) layers.push(createUserCoordinatesPointsLayer(userCoordinates));
        return layers;
    }, [userCanceledPrompts, userPromptResponses, userCoordinates]);

    return (
        <Box className={classes.root}>
            <ParticipantMapSelect
                className={classes.participantSelect}
            />

            <MapContainer
                className={classes.map}
                center={center}
                maxZoom={18}
                whenCreated={(mapInstance) => mapRef.current = mapInstance}
                zoom={zoom}
                zoomControl={false}
            >
                <TileLayer url={mapTileLayerURL} />
                { layers }
                <ZoomControl position='topright' />
            </MapContainer>
        </Box>
    );
};

export default ParticipantMap;
