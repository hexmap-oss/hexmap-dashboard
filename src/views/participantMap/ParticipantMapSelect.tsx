/* 
 * src/views/participantMap/ParticipantMapSelect.tsx
 */
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { alpha, Box, TextField, Typography } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Autocomplete } from '@material-ui/lab';

import {
    selectAllUuids,
    selectIsFetchingUuids,
    selectUuid,
    setUuid
} from './participantMapSlice';
import { ParticipantUUID } from './participantMapTypes';


const useStyles = makeStyles((theme: Theme) => ({
    inputRoot: {
        backgroundColor: alpha('#fff', 0.55),
        height: '2.6em',
        paddingLeft: 8,
        fontFamily: 'Roboto Mono',
        fontSize: 12,
    },
}));

const useTextStyles = makeStyles((theme: Theme) => ({
    inputLabelRoot: {
        fontFamily: 'Roboto Mono',
        fontSize: 12,
    },
}));


interface ParticipantMapSelectProps {
    className?: string,
}

const ParticipantMapSelect = (props: ParticipantMapSelectProps) => {
    const classes = useStyles();
    const textClasses = useTextStyles()
    const isRefreshing = useSelector(selectIsFetchingUuids);
    const selectedUuid = useSelector(selectUuid);
    const allUuids = useSelector(selectAllUuids);
    const dispatch = useDispatch();


    const handleChange = (event: any, value: ParticipantUUID | null) => {
        if (value) {
            dispatch(setUuid(value));
        }
    }

    return (
        <Box className={clsx(props.className)}>
            <Autocomplete
                id='map-participant-select'
                classes={classes}
                getOptionLabel={option => option.uuid}
                onChange={handleChange}
                options={allUuids}
                value={selectedUuid}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        margin='normal'
                    />
                )}
                renderOption={(option) => (
                    <Typography className={textClasses.inputLabelRoot}>{option.uuid}</Typography>
                )}
                loading={isRefreshing}
                disableListWrap
            />
        </Box>
    );
};

export default ParticipantMapSelect;
    
