/* 
 * src/views/participantMap/participantMapTypes.ts
 */

export type ParticipantUUID = {
    createdAt: number;
    uuid: string;
}
