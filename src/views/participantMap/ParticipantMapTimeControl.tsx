/* 
 * src/views/participantMap/ParticipantMapTimeControl.tsx
 */
import clsx from 'clsx';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

import DayByDaySelect from '../../components/widgets/DayByDaySelect';
import DateTimeSlider from '../../components/widgets/DateTimeSlider';
import { selectEndTime, selectStartTime, selectUserCollectionEndTime, selectUserCollectionStartTime, setEndTime, setStartTime } from './participantMapSlice';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 60,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative'
        },
        picker: {
            paddingTop: theme.spacing(1),
        },
        autoWidth: {
            width: '50%',
        },
        pickerSelect: {
            position: 'absolute',
            height: 60,
            left: 0,
            top: 0,
            display: 'flex',
            flexDirection: 'column',
            float: 'left',
            justifyContent: 'center',
        },
        pickerSelectButton: {
            fontSize: 12,
        }
    })
);

const ParticipantMapTimeControl = () => {
    const classes = useStyles();
    const [showDateTimeSlider, setShowDateTimeSlider] = useState(false);
    const collectionStartTime = useSelector(selectUserCollectionStartTime);
    const collectionEndTime = useSelector(selectUserCollectionEndTime);
    const startTime = useSelector(selectStartTime);
    const endTime = useSelector(selectEndTime);
    const dispatch = useDispatch();

    const handleShowDayByDayPicker = () => {
        setShowDateTimeSlider(false);
    }

    const handleShowTimeSlider = () => {
        setShowDateTimeSlider(true);
    }

    const handleChangeStartTime = (startTime: number) => {
        dispatch(setStartTime(startTime));
    }

    const handleChangeEndTime = (endTime: number) => {
        dispatch(setEndTime(endTime));
    }

    return (
        <Box className={classes.root}>
            <Box className={classes.pickerSelect}>
                <Button size='small' className={classes.pickerSelectButton} onClick={handleShowDayByDayPicker}>
                    Day-By-Day
                </Button>
                <Button size='small' className={classes.pickerSelectButton} onClick={handleShowTimeSlider}>
                    Time Range
                </Button>
            </Box>

            <Box className={clsx(classes.picker, showDateTimeSlider ? classes.autoWidth : undefined)}>
            {showDateTimeSlider
                ? <DateTimeSlider
                    collectionStartTime={collectionStartTime}
                    collectionEndTime={collectionEndTime}
                    sliderStartTime={startTime}
                    sliderEndTime={endTime}
                    onChangeStartTime={handleChangeStartTime}
                    onChangeEndTime={handleChangeEndTime}
                  />
                : <DayByDaySelect
                    collectionStartTime={collectionStartTime}
                    collectionEndTime={collectionEndTime}                
                    startTime={startTime}
                    onChangeStartTime={handleChangeStartTime}
                    onChangeEndTime={handleChangeEndTime}                
                />
            }
            </Box>
        </Box>
    );
}

export default ParticipantMapTimeControl;
