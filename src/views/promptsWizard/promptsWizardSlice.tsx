/* 
 * src/views/promptsWizard/promptsWizardSlice.ts
 */
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import update from 'immutability-helper';

import PromptsService, { PromptsSchemaParameters, PromptsSchemaResponse } from '../../api/prompts';
import { SurveyQuestionJSON } from '../../api/survey';
import {
        CheckboxOption,
        DropdownOption,
        JSONQuestionTypeLookup,
        MoveStackQuestion,
        QuestionTypes,
        StackQuestion
} from '../../components/questionBuilder/questionBuilderTypes';
import { isDebugMode } from '../../app/utils';


interface SurveyWizardState {
    questions: StackQuestion[];
    isActive: boolean;
};

const testInitialState: SurveyWizardState = {
    questions: [
        {
            id: 1,
            type: QuestionTypes.dropdown,
            title: 'Which flavor do you prefer?',
            fieldName: 'flavor',
            options: ['Chocolate', 'Vanilla', 'Cherry'] as DropdownOption[]
        },
        {
            id: 2,
            type: QuestionTypes.checkbox,
            title: 'What is your current mood?',
            fieldName: 'feelings',
            options: ['Happy', 'Mad', 'Silly'] as CheckboxOption[]            
        }
    ],
    isActive: false,
};

const initialState: SurveyWizardState = {
    questions: [
    ] as StackQuestion[],
    isActive: false,
};


export const fetchPromptsSchema = createAsyncThunk(
    'promptsWizard/fetchSchema',
    async(_, thunkAPI) => {
        try {
            const response = await PromptsService.fetchPromptsSchema();
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)

export const savePromptsSchema = createAsyncThunk(
    'promptsWizard/saveSchema',
    async(questions: StackQuestion[], thunkAPI) => {
        try {
            const jsonQuestions: SurveyQuestionJSON[] = questions.map(question => {
                const fields = question.options
                    ? { choices: question.options }
                    : {}
                const questionTypeId = Object.values(QuestionTypes).indexOf(question.type) + 1;

                return {
                    id: questionTypeId,
                    prompt: question.title,
                    colName: question.fieldName,
                    fields: fields,
                    answerRequired: true
                }
            });
            const schemaParams: PromptsSchemaParameters = {
                prompts: jsonQuestions,
            };
            const response = await PromptsService.savePromptsSchema(schemaParams);
            return response.data.results;
        }
        catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.message});
        }
    }
)


export const promptsWizardSlice = createSlice({
    name: 'promptsWizard',
    initialState: isDebugMode() ? testInitialState : initialState,
    reducers: {
        addQuestion: (state, action: PayloadAction<StackQuestion>) => {
            const newQuestions = [...state.questions, action.payload];
            state.questions = newQuestions;
        },
        deleteQuestion: (state, action: PayloadAction<number>) => {
            // const newQuestions = [...state.questions];
            // newQuestions.splice(action.payload.stackIndex, 1);
            // state.questions = newQuestions;
        },
        moveQuestion: (state, action: PayloadAction<MoveStackQuestion>) => {
            const question = state.questions[action.payload.fromIndex];
            state.questions = update(state.questions, {
                $splice: [
                    [action.payload.fromIndex, 1],
                    [action.payload.toIndex, 0, question],
                ]
            });
        },        
        saveQuestion: (state, action: PayloadAction<StackQuestion>) => {
            console.log(action.payload);
            const newQuestion = action.payload;
            state.questions = state.questions.map(existingQuestion => {
                if (existingQuestion.id === action.payload.id) {
                    return newQuestion;
                }
                return existingQuestion;
            });            
        }
    },
    extraReducers: builder => builder
        .addCase(fetchPromptsSchema.pending, (state, action) => {})
        .addCase(fetchPromptsSchema.fulfilled, (state, action: PayloadAction<PromptsSchemaResponse>) => {
            state.questions = action.payload.prompts.map((question: SurveyQuestionJSON, index: number) => {
                // map the string value to enum key (if exists) or use an integer
                // to lookup up enum key from intermediary JSON map object (deprecated)
                const questionType = question.type
                    ? QuestionTypes[question.type as keyof typeof QuestionTypes]
                    : JSONQuestionTypeLookup[question.id];

                const optionsTypes = [QuestionTypes.checkbox, QuestionTypes.dropdown];
                return  {
                    id: index + 1,
                    type: questionType,
                    title: question.prompt,
                    fieldName: question.colName,
                    options: optionsTypes.includes(questionType)
                        ? question.fields.choices
                        : undefined,
                };
            });

            state.isActive = action.payload.started;
        })
});

export const { addQuestion, deleteQuestion, moveQuestion, saveQuestion } = promptsWizardSlice.actions;

export const selectQuestions = (state: RootState) => state.promptsWizard.questions;
export const selectIsActive = (state: RootState) => state.surveyWizard.isActive;

export default promptsWizardSlice.reducer;
