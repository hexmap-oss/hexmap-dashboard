/* 
 * src/view/promptsWizard/PromptsWizardHeader.tsx
 */
import { useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import CommentOutlinedIcon from '@material-ui/icons/CommentOutlined';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import { selectIsActive } from './promptsWizardSlice';
import DraggableButton from '../../components/questionBuilder/DraggableButton';
import { FormTypes } from '../../components/questionBuilder/questionBuilderTypes';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 16,
        },
        buttonRow: {
            display: 'flex',
        },
        button: {
            margin: 5,
            paddingRight: 12,
            paddingLeft: 12,
            fontSize: 13,
            fontWeight: 'bold',

            '&:hover': {
                backgroundColor: theme.palette.primary.dark
            },

            '&:disabled': {
                backgroundColor: theme.palette.primary.light
            }
        },
        buttonIcon: {
            fontSize: 13,
            marginRight: 5,
        }
    }),
);


const PromptsWizardHeader = () => {
    const classes = useStyles();
    const isActive = useSelector(selectIsActive);

    return (
        <Paper elevation={1} className={classes.root}>
            <Typography variant='body1'>
                Enter prompts to query users about their current location or activities below.
            </Typography>

            <Box className={classes.buttonRow}>
                <DraggableButton
                    classes={classes}
                    text={'Dropdown'}
                    Icon={KeyboardArrowDownIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'dropdown'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}             
                    text={'Checkboxes'}
                    Icon={CheckBoxOutlinedIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'checkbox'
                    }}
                    isDisabled={isActive}
                />
                <DraggableButton
                    classes={classes}                    
                    text={'Text Box'}
                    Icon={CommentOutlinedIcon}
                    draggedItem={{
                        type: FormTypes.BUTTON,
                        question: 'textBox'
                    }}
                    isDisabled={isActive}               
                />
            </Box>                                                           
        </Paper>
    );
}

export default PromptsWizardHeader;
