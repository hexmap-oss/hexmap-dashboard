/* 
 * src/views/promptsWizard/PromptsWizardForm.tsx
 */
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';

import { fetchPromptsSchema, savePromptsSchema, selectIsActive, selectQuestions } from './promptsWizardSlice';
import QuestionStack from '../../components/questionBuilder/QuestionStack';
import {
    addQuestion,
    deleteQuestion,
    moveQuestion,
    saveQuestion
} from './promptsWizardSlice';
import { useEffect } from 'react';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: 'auto',
            paddingLeft: '5%',
            paddingRight: '5%',
            maxWidth: 760,
        },
        saveButton: {
            textAlign: 'center',
            paddingBottom: 48,
            paddingTop: 48,
        },
        saveButtonHelpText: {
            marginTop: 8,
            color: theme.palette.grey[700],
            fontSize: 14
        }
    }),
);

const PromptsWizardForm = () => {
    const classes = useStyles();
    const questions = useSelector(selectQuestions);
    const isActive = useSelector(selectIsActive);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPromptsSchema());
    }, [dispatch]);

    const handleSavePromptsSchema = () => {
        dispatch(savePromptsSchema(questions));
    }

    return (
        <Box className={classes.root}>
            <Box flexGrow={1} p={1}>
                <QuestionStack
                    questions={questions}
                    addQuestion={addQuestion}
                    moveQuestion={moveQuestion}
                    deleteQuestion={deleteQuestion}
                    saveQuestion={saveQuestion}
                    disabled={isActive}
                />

                <Box className={classes.saveButton}>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={handleSavePromptsSchema}
                        startIcon={<SaveIcon />}
                        disabled={isActive}
                    >
                        Save Prompts
                    </Button>

                    { isActive
                        ? <Typography className={classes.saveButtonHelpText}>Save disabled, the survey is active.</Typography>
                        : undefined
                    }                    
                </Box>
            </Box>
        </Box>
    );
}

export default PromptsWizardForm;