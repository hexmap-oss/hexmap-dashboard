/* 
 * src/views/tutorial/TutorialCard.tsx
 */
import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { SvgIconTypeMap } from '@material-ui/core/SvgIcon';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 32,
            margin: 16,
        },
        iconContent: {
            display: 'flex',
            justifyContent: 'center'
        },
        icon: {
            color: theme.palette.grey[500],
            fontSize: 70,
        },
        title: {
            fontSize: 18,
        },
        pos: {
            marginBottom: 12,
        }
    })
);


type TutorialCardProps = {
    title: string,
    text: string,
    Icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>,
    reversed?: boolean,
};


const TutorialCard = ({title, text, Icon, reversed = false}: TutorialCardProps) => {
    const classes = useStyles();

    const textContent = (
        <CardContent>
            <Typography className={classes.title} color='textSecondary'>{ title }</Typography>

            <Typography variant='body2' align='justify' component='p'>{ text }</Typography>
        </CardContent>
    );

    const iconContent = (
        <CardContent className={classes.iconContent}>
            <Icon className={classes.icon} />
        </CardContent>
    );

    return (
        <Card className={classes.root} variant='outlined'>
            <Grid container alignItems='center' spacing={0}>
                <Grid item xs={6}>
                    { reversed === false ? textContent : iconContent }
                </Grid>
                <Grid item xs={6}>
                    { reversed === false ? iconContent : textContent }
                </Grid>
            </Grid>
        </Card>
    );
}


export default TutorialCard;
