import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import sidebarReducer from '../components/sidebar/sidebarSlice';
import dataManagementReducer from '../views/dataManagement/dataManagementSlice';
import loginReducer from '../views/login/loginSlice';
import metricsReducer from '../views/metrics/metricsSlice';
import odMapControlReducer from '../views/odMap/ODMapControlSlice';
import odMapReducer from '../views/odMap/ODMapSlice';
import organizationUsersCodeReducer from '../views/organizationUsers/organizationUsersCodeSlice';
import organizationUsersTableReducer from '../views/organizationUsers/organizationUsersTableSlice';
import organizationSettingsReducer from '../views/organizationSettings/organizationSettingsSlice';
import participantMapReducer from '../views/participantMap/participantMapSlice';
import participantTableReducer from '../views/participantTable/participantTableSlice';
import promptsWizardReducer from '../views/promptsWizard/promptsWizardSlice';
import resetPasswordReducer from '../views/login/resetPasswordSlice';
import settingsReducer from '../views/surveySettings/surveySettingsSlice';
import surveyWizardReducer from '../views/surveyWizard/surveyWizardSlice';

export const store = configureStore({
    reducer: {
        sidebar: sidebarReducer,
        dataManagement: dataManagementReducer,
        login: loginReducer,
        metrics: metricsReducer,
        odMapControl: odMapControlReducer,
        odMap: odMapReducer,
        organizationUsersCode: organizationUsersCodeReducer,
        organizationUsersTable: organizationUsersTableReducer,
        organizationSettings: organizationSettingsReducer,
        participantMap: participantMapReducer,
        participantTable: participantTableReducer,
        promptsWizard: promptsWizardReducer,
        resetPassword: resetPasswordReducer,
        settings: settingsReducer,
        surveyWizard: surveyWizardReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;
