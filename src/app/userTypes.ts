/* 
 * src/app/userTypes.ts
 */

export type User = {
    AccessToken: string,
    UserLevel: number,
    SurveyName: string,
    RefreshToken?: string,    
};
