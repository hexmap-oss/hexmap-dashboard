/* 
 * src/app/browserStorage.ts
 */
import { User } from './commonTypes';


export const setCachedUser = (user: User) => {
    sessionStorage.setItem('accessToken', user.AccessToken);
    sessionStorage.setItem('userLevel', user.UserLevel.toString());
    sessionStorage.setItem('surveyName', user.SurveyName);

    localStorage.setItem('hmat-v1', user.AccessToken);
    if (user.RefreshToken && user.RefreshToken !== 'undefined') {
        localStorage.setItem('hmrt-v1', user.RefreshToken);
    }
}

export const clearCachedUser = () => {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('userLevel');
    sessionStorage.removeItem('surveyName');

    localStorage.removeItem('hmat-v1');
    localStorage.removeItem('hmrt-v1');
}

export const getCachedUser = (): User => {
    const accessToken = localStorage.getItem('hmat-v1')
        ? localStorage.getItem('hmat-v1')
        : sessionStorage.getItem('accessToken');
    const userLevel = sessionStorage.getItem('userLevel') ? parseInt(sessionStorage.getItem('userLevel')!) : null;
    const surveyName = sessionStorage.getItem('surveyName');

    if (accessToken === null) {
        throw Error(`missing cached access token: ${accessToken}`);
    }

    return {
        // TODO: Use sessionStorage for token cache or get a new one with a JWT refresh token (hmrt-v1)
        AccessToken: accessToken,
        UserLevel: userLevel!,
        SurveyName: surveyName!,
        // RefreshToken: accessToken, // Long-term HexMap refresh token
    };
}
