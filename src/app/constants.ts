/* 
 * src/app/constants.ts
*/
export const API_URL = String(process.env.REACT_APP_PUBLIC_URL);
export const API_WS_URL = String(process.env.REACT_APP_PUBLIC_WS_URL);
