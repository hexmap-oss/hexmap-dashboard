/* 
 * src/app/utils.ts
 */

export function makeDashedId(str: string) {
    return str.replace(/\s+/g, '-').toLowerCase();    
}

export function isStringEmpty(str: string | null | undefined) {
    return typeof str === 'undefined' ||
        str === null ||
        str.trim().length === 0;
}

export function capitalize(str: string) {
    return str.charAt(0).toLocaleUpperCase() + str.slice(1);
}

export function isDebugMode() {
    // check whether the production mode has been manually overriden
    // from .env file
    if (process.env.REACT_APP_DEVELOPMENT_MODE?.toLowerCase() === "false") {
        return false;
    }
    // otherwise, return the mode set by NODE_ENV
    if (process.env.NODE_ENV === 'development') {
        return true;
    }
    return false;
}


export function parseUnixTime(unixTime: number) {
    return new Date(unixTime * 1000);
}

export function dateToUnixTime(d: Date) {
    return d.getTime() / 1000;
}

export function stringArraysEqual(arr1: string[], arr2: string[]) {
    return JSON.stringify(Object.assign([], arr1).sort()) === JSON.stringify(Object.assign([], arr2).sort());
}
