/* 
 * src/app/routes.tsx
 */
import { Route, Redirect, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { selectIsAuthorized } from '../views/login/loginSlice';


interface PrivateRouteProps {
    children?: React.ReactNode;
    exact?: boolean;
    path: string;
};

export const PrivateRoute = (props: PrivateRouteProps) => {
    const loginIsAuthorized = useSelector(selectIsAuthorized);
    const location = useLocation();

    return (
        <Route
            render={() => {
                // TODO: this should show an intermediary component while login is being performed
                return loginIsAuthorized
                    ? props.children
                    : <Redirect to={{
                          pathname: '/login',
                          state: {
                              referrer: location.pathname
                          }
                        }}
                      />
            }}
            path={props.path}
            exact={props.exact}
        />
    );
};
