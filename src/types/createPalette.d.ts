import * as createPalette from '@material-ui/core/styles/createPalette';

declare module '@material-ui/core/styles/createPalette' {
    interface PaletteOptions {    
        blueGrey?: PaletteColorOptions;
    }

    interface Palette {    
        blueGrey: PaletteColor;
    }
}
