import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import BaseApp from './BaseApp';
import { store } from './app/store';
import { Provider } from 'react-redux';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import * as serviceWorker from './serviceWorker';
import ThemeProvider from './themes/ThemeProvider';

ReactDOM.render(
    <React.StrictMode>
        <DndProvider backend={HTML5Backend}>
            <ThemeProvider>
                <Provider store={store}>
                    <BaseApp />
                </Provider>
            </ThemeProvider>
        </DndProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
