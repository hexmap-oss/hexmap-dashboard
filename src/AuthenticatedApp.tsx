/* 
 * src/AuthenticatedApp.tsx
 */
import React from 'react';
import clsx from 'clsx';
import { Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { PrivateRoute } from './app/routes';
import BaseAppTheme from './themes/base';
import CustomAppBar from './components/appBar/CustomAppBar';
import Sidebar from './components/sidebar/Sidebar';
import DataManagementPanel from './layouts/dataManagement/DataManagementPanel';
import MetricsPanel from './layouts/metrics/MetricsPanel';
import ParticipantMapPanel from './layouts/participantMap/ParticipantMapPanel';
import ParticipantUsersPanel from './layouts/participantUsers/ParticipantUsersPanel';
import PromptsWizardPanel from './layouts/promptsWizard/PromptsWizardPanel';
import ODMapPanel from './layouts/odMap/ODMapPanel';
import OrganizationSettingsPanel from './layouts/organizationSettings/OrganizationSettingsPanel';
import OrganizationUsersPanel from './layouts/organizationUsers/OrganizationUsersPanel';
import SurveySettingsPanel from './layouts/surveySettings/SurveySettingsPanel';
import SurveyMapPanel from './layouts/surveyMap/SurveyMapPanel';
import SurveyWizardPanel from './layouts/surveyWizard/SurveyWizardPanel';
import TutorialPanel from './layouts/tutorial/TutorialPanel';
import UserSettingsPanel from './layouts/userSettings/UserSettingsPanel';


const useStyles = makeStyles((theme) => BaseAppTheme(theme));


const AuthenticatedApp = () => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    }

    const handleDrawerClose = () => {
        setOpen(false);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />

            {/* Top Navigation Bar */}
            <CustomAppBar
                drawerIsOpen={open}
                onDrawerOpen={handleDrawerOpen}
            />
            
            {/* Left-hand sidebar */}
            <Sidebar
                classes={classes}
                handleClose={handleDrawerClose}
                isOpen={open} />

            {/* Inner panel containers */}
            <main className={clsx(classes.content, {
                [classes.contentShift]: open
            })}>
                <Switch>
                    <PrivateRoute exact path='/'>
                        <TutorialPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/dashboard-users'>
                        <OrganizationUsersPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/survey-wizard'>
                        <SurveyWizardPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/prompts-wizard'>
                        <PromptsWizardPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/metrics'>
                        <MetricsPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/od-map'>
                        <ODMapPanel />
                    </PrivateRoute>            
                    <PrivateRoute path='/survey-map'>
                        <SurveyMapPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/participant-users'>
                        <ParticipantUsersPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/participant-map'>
                        <ParticipantMapPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/data-management'>
                        <DataManagementPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/survey-settings'>
                        <SurveySettingsPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/organization-settings'>
                        <OrganizationSettingsPanel />
                    </PrivateRoute>
                    <PrivateRoute path='/user-settings'>
                        <UserSettingsPanel />
                    </PrivateRoute>
                    <PrivateRoute path='*'>
                        <TutorialPanel />
                    </PrivateRoute>						
                </Switch>
            </main>
        </div>
    );
}

export default AuthenticatedApp;
