/* 
 * src/api/dataManagement.ts
 */
import * as Constants from '../app/constants';


class WebsocketService {
    socket?: WebSocket;

    connect() {
        if (!this.socket) {
            this.socket = new WebSocket(Constants.API_WS_URL + '/socket/v2/export');
        }
        return this.socket;
    }
}

export default new WebsocketService();
