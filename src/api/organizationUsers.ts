/* 
 * src/api/organizationUsers.ts
 */
import API from './api';


class OrganizationUsersService {
    fetchInviteCode() {
        return API
            .get('/webusers/signup/code')
            .then((response) => {
                return response;
            })
    }

    refreshInviteCode() {
        return API
            .post('/webusers/signup/code')
            .then((response) => {
                return response;
            })
    }

    fetchOrganizationUsers() {
        return API
            .get('/webusers/table', {
                params: {
                    pageIndex: 1,
                    itemsPerPage: 8,
                    // sorting: 'asc',
                }
            })
            .then((response) => {
                return response;
            })
    }
}

export default new OrganizationUsersService();
