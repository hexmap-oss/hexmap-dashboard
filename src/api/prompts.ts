/* 
 * src/api/prompts.ts
 */
import API from './api';
import { SurveyQuestionJSON } from './survey';


export type PromptsSchemaParameters = {}

export type PromptsSchemaResponse = {
    prompts: SurveyQuestionJSON[],
    started: boolean,
};


class PromptsService {
    fetchPromptsSchema() {
        return API
            .get('/promptswizard/edit')
            .then((response) => {
                return response;
            })
    }

    savePromptsSchema(params: PromptsSchemaParameters) {
        return API
            .post('/promptswizard/edit', params)
            .then((response) => {
                return response;
            })
    }
}


export default new PromptsService();
