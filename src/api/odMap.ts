/* 
 * src/api/odMap.ts
 */
import API from './api';


export type TripsMatrixParameters = {
    boundaryId: number
    startTime: number
    endTime: number
    mapType: string
    transportMode: string
    tripPurpose: string
}


class ODMapService {
    fetchRegions() {
        return API
            .get('/maps/od/regions')
            .then((response) => {
                return response;
            })
    }

    fetchBoundaries(region: string) {
        return API
            .get('/maps/od/boundaries', {
                params: { region }
            })
            .then((response) => {
                return response;
            })
    }

    fetchTripsMatrix(params: TripsMatrixParameters) {
        return API
            .get('/maps/od/matrix', { params })
            .then((response) => {
                return response;
            })
    }
}

export default new ODMapService();
