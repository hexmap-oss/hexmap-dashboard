/* 
 * src/api/auth.ts
 */
// import Keycloak, { KeycloakAdapter } from 'keycloak-js';
import API, { setAxiosJWT } from './api';
import { setCachedUser, getCachedUser } from '../app/browserStorage';
import { User } from '../app/commonTypes';
import { isDebugMode } from '../app/utils';



// REAL AUTH
class AuthService {
    init() {
        const _refreshJWT = this.refreshJWT.bind(this);

        // attach interceptors to each API request
        API.interceptors.response.use(response => {
            return response;
        }, async function (this: AuthService, error) {
            const originalRequest = error.config;
            if (error.response.status === 403 && !originalRequest._retry) {
                originalRequest._retry = true;
                _refreshJWT(user.AccessToken);
                return API(originalRequest);
            }
            return Promise.reject(error);
        });

        // set user in app state from cache and refresh jwt or redirect to login
        const user = this.getCurrentUser();
        try {
            _refreshJWT(user.AccessToken);
            return user;
        }
        catch (error) {
            return Promise.reject(error);
        }
    }

    login(username: string, password: string) {
        return API
            .post('/auth', { email: username, password })
            .then((response) => {
                const user: User = {
                    AccessToken: response.data.accessToken,
                    UserLevel: response.data.userLevel,
                    SurveyName: response.data.surveyName,
                };

                // cache user to browser storage and set JWT auth header
                if (user.AccessToken) {
                    setCachedUser(user);
                    setAxiosJWT(user.AccessToken);
                    return user;
                }
                return Promise.reject('access token not available');
            })
    }

    refreshJWT(token: string) {
        return API
            .post('/auth/refresh')
            .then((response) => {
                const user: User = {
                    AccessToken: response.data.accessToken,
                    UserLevel: response.data.userLevel,
                    SurveyName: response.data.surveyName,
                };

                // cache user to browser storage and set JWT auth header
                setCachedUser(user);
                setAxiosJWT(user.AccessToken);
                return user;
            })
            .catch((e) => {
                this.logout();
                throw e;
            });
    }

    logout() {
        sessionStorage.removeItem('accessToken');
        sessionStorage.removeItem('surveyName');
        sessionStorage.removeItem('userLevel');
        sessionStorage.removeItem('hmrt-v1');
        localStorage.removeItem('hmat-v1');
    }

    getCurrentUser() {
        const user = getCachedUser();
        return user;
    }

    resetPassword(baseURL: String, email: String) {
        return API
            .post('/auth/password/reset', { baseURL, email })
            .then((response) => {
                return response;
            })
            .catch(function(err) {
                console.log(err);
            })
    }

    updatePassword(email: String, password: String, token: String) {
        return API
            .put('/auth/password/reset', { email, password, token })
            .then((response) => {
                return response;
            })
    }
};


// KEYCLOAK VERSION OF EXISTING AUTH SERVICE
// class KeycloakAuthService {
//     init() {
//         const keycloak = new Keycloak();
//         keycloak.init()
//             .then(function(authenticated: boolean) {
//                 alert(authenticated ? 'authenticated' : 'not authenticated');
//             })
//             .catch(function() {
//                 alert('failed to initialize');
//             });

//     }

//     login(username: string, password: string) {
//         return false;
//     }

//     logout() {
//         sessionStorage.removeItem('surveyName');
//         sessionStorage.removeItem('userLevel');
//     }
// }
// const MyCustomAdapter: KeycloakAdapter = {
//     login(options) {

//     }
// };
// const keycloak = new Keycloak();
// keycloak.init({
//     adapter: MyCustomAdapterm,
// });


// OFFLINE DEBUG AUTH
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const FAKE_USER = { AccessToken: 'jwt-access-abc', UserLevel: 0, SurveyName: 'OfflineTest' } as User;

const fakeAuthenticate = async(username: string, password: string) => {
    await sleep(250);
    if (username === 'abc' && password === '123') {
        return FAKE_USER;
    }
    throw new Error('Username or password not recognized');
}

const fakeRefresh = async(user: User) => {
    await sleep(100);
    return user;
}

class FakeAuthService {
    init() {
        // set user in app state
        const user = this.getCurrentUser();
        setAxiosJWT(user.AccessToken)
        return user;        
    }

    login(username: string, password: string) {
        return fakeAuthenticate(username, password)
            .then((user) => {
                setCachedUser(user);
                return user;
            })
    }

    refreshJWT(token: string) {
        setCachedUser(FAKE_USER);
        return fakeRefresh(FAKE_USER);
    }    

    logout() {
        sessionStorage.removeItem('accessToken');
        sessionStorage.removeItem('surveyName');
        sessionStorage.removeItem('userLevel');
        sessionStorage.removeItem('hmrt-v1');
        localStorage.removeItem('hmat-v1');
    }

    getCurrentUser() {
        return getCachedUser();
    }

    resetPassword(email: String) {
        return {};
    }

    updatePassword(email: String, password: String, token: String) {
        return {};
    }    
};

export default isDebugMode() ? new FakeAuthService() : new AuthService();
