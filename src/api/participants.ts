/* 
 * src/api/participants.ts
 */
import API from './api';


export type ParticipantTableParameters = {
    itemsPerPage: number;
    pageIndex: number;
    searchString?: string;
    sorting?: string;
}


class ParticipantsService {
    fetchParticipantTable(params: ParticipantTableParameters) {
        return API
            .get('/itinerum/users/table', { params })
            .then((response) => {
                return response;
            })
    }
}

export default new ParticipantsService();
