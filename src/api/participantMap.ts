/* 
 * src/api/participantMap.ts
 */
import API from './api';
import msgpack from 'msgpack-lite';
import { ParticipantUUID } from '../views/participantMap/participantMapTypes';


export type ParticipantUuidsResponse = {
    uuids: ParticipantUUID[];
    lastPipelineRun: number;
    lastDetectedTrip: number;  
};

export type ParticipantCoordinatesParameters = {
    userId: string
    startTime?: number
    endTime?: number
    tzBrowser?: string
}

export type ParticipantCoordinatesResponse = {
    collectionStart: number,
    collectionEnd: number,
    searchStart: number,
    searchEnd: number,
    cancelledPrompts: GeoJSON.FeatureCollection<GeoJSON.Point>,
    points: GeoJSON.FeatureCollection<GeoJSON.LineString>,
    promptResponses: GeoJSON.FeatureCollection<GeoJSON.Point>
}


class ParticipantMapService {
    fetchParticipantUuids() {
        return API
            .get('/itinerum/users')
            .then((response) => {
                return response.data.results as ParticipantUuidsResponse;
            })
    }

    fetchParticipantCoordinates(params: ParticipantCoordinatesParameters) {
        return API
            .get(`/itinerum/users/${params.userId}/points`, {
                headers: {
                    'Content-Type': 'application/msgpack',
                    'Accept': 'application/msgpack'
                },
                responseType: 'arraybuffer',
                params: {
                    'startTime': params.startTime ? params.startTime : undefined,
                    'endTime': params.endTime ? params.endTime : undefined,
                    'tzBrowser': params.tzBrowser ? params.tzBrowser : undefined,
                }
            })
            .then((msgpackResponse) => {
                const response = msgpack.decode(Buffer.from(msgpackResponse.data));
                return response as ParticipantCoordinatesResponse;
            })
    }
}

export default new ParticipantMapService();

