/* 
 * src/api/dataManagement.ts
 */
import API from './api';


export type ExportDataParameters = {
    connectionId: string
    startTime: number
    endTime: number
    timezone: string
}


class DataManagementService {
    fetchSurveyStatus() {
        return API
            .get('/data/status')
            .then((response) => {
                return response;
            })
    }

    initiatiateExportSurveyData(params: ExportDataParameters) {
        return API
            .get('/data/export/raw/events', { params })
            .then((response) => {
                return response;
            })
    }

    initiatiateExportTripsData(params: ExportDataParameters) {
        return API
            .get('/data/export/trips/events', { params })
            .then((response) => {
                return response;
            })    
    }
}

export default new DataManagementService();
