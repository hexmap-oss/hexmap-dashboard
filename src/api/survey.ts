/* 
 * src/api/survey.ts
 */
import API from './api';

export type SurveyQuestionFieldsJSON = {
    choices?: string[];
}

export type SurveyQuestionJSON = {
    id: number
    colName: string
    prompt: string
    fields: SurveyQuestionFieldsJSON
    answerRequired: boolean
    type?: string
}

export type SurveySchemaParameters = {
    language: string
    aboutText: string
    termsOfService: string
    questions: SurveyQuestionJSON[]
}


export type SurveySchemaResponse = {
    // uuids: ParticipantUUID[];
    // lastPipelineRun: number;
    // lastDetectedTrip: number;
    survey: SurveyQuestionJSON[],
    started: boolean,
};


class SurveyService {
    fetchSurveySchema() {
        return API
            .get('/surveywizard/edit')
            .then((response) => {
                return response;
            })
    }

    saveSurveySchema(params: SurveySchemaParameters) {
        return API
            .post('/surveywizard/edit', params)
            .then((response) => {
                return response;
            })
    }
}


export default new SurveyService();
