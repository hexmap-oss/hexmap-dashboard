/* 
 * src/api/metrics.ts
 */
import API from './api';
import {
    DetectedTripDatesCalendarItem,
    InstallationsBarChartItem,
    InstallationsPieChartItem,
    UserActivityLineChartSeries,
    PromptResponsesBarItem,
    QuickStatsRow,
} from '../views/metrics/metricsTypes';


export type ChartsParameters = {
    start: number
    end: number
    countsTable: boolean
    promptNum: number
}

export type ChartsResponse = {
    overview: QuickStatsRow[],
    installationsBarChart: InstallationsBarChartItem[],
    installationsPieChart: InstallationsPieChartItem[],
    activeUsersLineChart: UserActivityLineChartSeries[],
    detectedTripDatesCalendar: DetectedTripDatesCalendarItem[],
    promptLabels: string[],
    promptResponsesBarChart: PromptResponsesBarItem[],
    promptResponsesBarLabels: string[],    
}


class MetricsService {
    fetchChartsData(params: ChartsParameters) {
        return API
            .get('/itinerum/metrics', { params })
            .then((response) => {
                return response;
            })
    }
}

export default new MetricsService();
