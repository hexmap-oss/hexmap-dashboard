/* 
 * src/api/auth.ts
 */
import axios, { AxiosRequestConfig } from 'axios';

import * as Constants from '../app/constants';
import { isDebugMode } from '../app/utils';


// set the token as an axios interceptor for future JWT requests
export const setAxiosJWT = (token: string) => {
    API.interceptors.request.use((config: AxiosRequestConfig) => {
        config.headers.Authorization = 'JWT ' + token;
        return config;
    });
}


// live server API
const WebAPI = axios.create({
    baseURL: `${Constants.API_URL}/dashboard/v2`,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
});

// testing api to blackhole webrequests
const FakeAPI = axios.create({
    baseURL: `http://localhost:65535`,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
});

const API = isDebugMode() ? FakeAPI : WebAPI;


export default API;
