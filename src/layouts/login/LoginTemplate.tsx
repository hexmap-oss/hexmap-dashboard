/* 
 * src/layouts/login/LoginTemplate.tsx
 */
import clsx from 'clsx';
import React from 'react';
import { Link, useHistory, Redirect } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { selectIsAuthorized } from '../../views/login/loginSlice';
import LogoSVG from './logo.svg';
import { useSelector } from 'react-redux';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.palette.primary.dark,
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            minHeight: '100vh',
        },
        languagesController: {
            alignItems: 'center',
            display: 'flex',
            position: 'absolute',
            top: 0,
            right: 0,
            marginTop: 15,
            marginRight: 15,
        },
        languageSelectContainer: {
            display: 'flex',
            width: '100%',
        },
        languageSelect: {
            alignItems: 'center',
            cursor: 'pointer',
            border: '2px solid white',
            borderRadius: '100px',
            display: 'flex',
            height: 26,
            justifyContent: 'center',
            marginLeft: 5,
            width: 26,
        },
        languageSelectActive: {
            backgroundColor: theme.palette.common.white,
        },
        languageText: {
            color: theme.palette.common.white,
            fontFamily: 'Arial',
            fontSize: 14,
            marginTop: -2
        },
        languageTextActive: {
            color: theme.palette.primary.dark,
        },
        loginFormContainer: {
            textAlign: 'center',
            flexGrow: 2
        },
        logo: {
            width: 250,
            height: 73
        },
        spacer: {
            flexGrow: 1
        }
    }),
);

interface LoginTemplateProps {
    children?: React.ReactNode;
};

const LoginTemplate = ({children}: LoginTemplateProps) => {
    const classes = useStyles();
    const history = useHistory<{ referrer?: string }>();
    const loginIsAuthorized = useSelector(selectIsAuthorized);

    if (loginIsAuthorized) {
        const redirectPath = history.location.state && history.location.state.referrer
            ? history.location.state.referrer
            : '/';
        return <Redirect to={redirectPath} />
    }

    return (
        <Box className={classes.root}>
            <Box className={classes.languagesController}>
                <Box className={classes.languageSelectContainer}>
                    <Box className={clsx(classes.languageSelect, classes.languageSelectActive)}>
                        <Typography className={clsx(classes.languageText, classes.languageTextActive)}>en</Typography>
                    </Box>
                    <Box className={classes.languageSelect}>
                        <Typography className={classes.languageText}>fr</Typography>
                    </Box>
                </Box>
            </Box>

            <Box className={classes.spacer}></Box>

            <Box className={classes.loginFormContainer}>
                <Link to={'/login'}>
                    <img className={classes.logo} src={LogoSVG} alt='hexmap logo' />
                </Link>

                {children}     
            </Box>

            <Box className={classes.spacer}></Box>
        </Box>
    );
};


export default LoginTemplate;
