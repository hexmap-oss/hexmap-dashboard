/* 
 * src/layouts/login/LoginPanel.tsx
 */
import { Route, Switch } from 'react-router-dom';

import LoginTemplate from './LoginTemplate';
import LoginForm from '../../views/login/LoginForm';
import EmailResetPasswordForm from '../../views/login/EmailPasswordResetForm';
import ResetPasswordForm from '../../views/login/ResetPasswordForm';


const LoginPanel = () => {
    return (
        <LoginTemplate>
            <Switch>
                <Route path={'/login'} exact component={LoginForm} />
                <Route path={'/login/reset-password'} exact component={EmailResetPasswordForm} />
                <Route path={'/login/reset-password/update'} component={ResetPasswordForm} />
            </Switch>
        </LoginTemplate>
    );
};

export default LoginPanel;
