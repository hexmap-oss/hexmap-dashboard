/* 
 * layouts/login/LoggingInJWT.tsx
 */
import clsx from 'clsx';
import { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useEffect } from 'react';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(255, 255, 255, 0.06)',
            display: 'flex',
            height: '100vh',
        },
        fadeIn: {
            transition: '1000ms all ease',
            opacity: 0,
            willChange: 'opacity',
        },
        show: {
            opacity: 1,
        },
        progressCircle: {
            marginRight: 10,
        }   
    })
);

const LoggingInJWT = () => {
    const classes = useStyles();
    const [showStatus, setShowStatus] = useState(false);

    useEffect(() => {
        const timeout = setTimeout(() => { setShowStatus(true) }, 250)
        return () => {
            clearTimeout(timeout);
        }
    }, []);

    return (
        <Box className={clsx(classes.container, classes.fadeIn, showStatus ? classes.show : undefined)}>
            <CircularProgress
                className={classes.progressCircle}
                size={20}
                thickness={10}
            />
            <Box>Checking existing login...</Box>
        </Box>
    );
}

export default LoggingInJWT;
