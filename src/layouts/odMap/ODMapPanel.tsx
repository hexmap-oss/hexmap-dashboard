/* 
 * src/layouts/odMap/ODMap.tsx
 */
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import ColorLegend from '../../components/widgets/ColorLegend';
import ODArcMap from '../../views/odMap/ODArcMap';
import ODMapControl from '../../views/odMap/ODMapControl';
import ODMapTable from '../../views/odMap/ODMapTable';


const ODMapPanel = () => {
    return (
        <Box>
            <Grid container>
                <ODMapControl />
            </Grid>
            <Grid container>
                <Grid item xs={8}>
                    <ODArcMap />
                </Grid>
                <Grid item xs={4}>
                    <ODMapTable />
                </Grid>
            </Grid>
            {/*
            <Grid container className={classes.root}>
                <Grid item xs={9}>
                    <Paper className={classes.legendPaper}>
                        <ColorLegend />
                    </Paper>
                </Grid>
            </Grid>
            */}
        </Box>
    );
};

export default ODMapPanel;
