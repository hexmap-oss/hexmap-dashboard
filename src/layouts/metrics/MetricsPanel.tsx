/* 
 * src/layouts/metrics/Metrics.tsx
 */
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import MetricsTimeSelector from '../../views/metrics/MetricsTimeSelector';
import DetectedTrips from '../../views/metrics/DetectedTrips';
import Installations from '../../views/metrics/Installations';
import PromptResponses from '../../views/metrics/PromptResponses';
import QuickStats from '../../views/metrics/QuickStats';
import UserActivity from '../../views/metrics/UserActivity';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: '8px 16px',
        }
    })
);

const MetricsPanel = () => {
    const classes = useStyles();

    return (
        <Box>
            <Grid container spacing={3} className={classes.root}>
                <Grid item sm={12}>
                    <MetricsTimeSelector />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={3}>
                        <Grid item xs={12} md={4}>
                            <QuickStats />
                        </Grid>                
                        <Grid item xs={12} md={8}>
                            <Installations />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            
            <Grid container spacing={3} className={classes.root}>
                <Grid item xs={6}>
                    <UserActivity />
                </Grid>
                <Grid item xs={6}>
                    <PromptResponses />
                </Grid>                
            </Grid>

            <Grid container spacing={3} className={classes.root}>
                <Grid item xs={12}>
                    <DetectedTrips />
                </Grid>
            </Grid>
        </Box>
    );
};

export default MetricsPanel;
