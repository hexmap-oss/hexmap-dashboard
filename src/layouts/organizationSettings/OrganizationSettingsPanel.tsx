/* 
 * src/layouts/organizationSettings/OrganizationSettingsPanel.tsx
 */
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import OrganizationSettingsCard from '../../views/organizationSettings/OrganizationSettingsCard';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingTop: theme.spacing(2),
            paddingLeft: theme.spacing(4),
            paddingRight: theme.spacing(4),
        },
        rootContainer: {
            padding: theme.spacing(3)
        },
        title: {
            paddingLeft: theme.spacing(1),
            borderBottomWidth: 1,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.grey[300],
            marginBottom: 10,            
            fontWeight: 600,
            paddingBottom: 10,
        },
        subtext: {
            color: theme.palette.grey[800],
        },        
    })
);


const OrganizationSettingsPanel = () => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Paper className={classes.rootContainer}>
                {/* Heading text */}
                <Typography className={classes.title} variant='h5'>Organization Settings</Typography>
                <Typography className={classes.subtext} variant='subtitle1'>Adjust settings for your organization.</Typography>                
                <Grid container>
                    <Grid item sm={12}>
                        <OrganizationSettingsCard />
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    );
};

export default OrganizationSettingsPanel;
