/* 
 * src/views/tutorial/TutorialPanel.tsx
 */
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import BallotIcon from '@material-ui/icons/Ballot';
import BarChartIcon from '@material-ui/icons/BarChart';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import FaceIcon from '@material-ui/icons/Face';
import FlagIcon from '@material-ui/icons/Flag';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import PublicIcon from '@material-ui/icons/Public';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

import TutorialCard from '../../views/tutorial/TutorialCard';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        title: {
            paddingTop: 32,
            paddingBottom: 32,
        },
        titleText: {
            fontWeight: 'bold',
        },
    }),
);


const TutorialPanel = () => {
    const classes = useStyles();

    return (
        <Container maxWidth='md'>
            {/* Page title */}
            <Box className={classes.title}>
                <Typography className={classes.titleText} variant='h4' align='center'>
                    HexMap Dashboard Guide
                </Typography>
            </Box>

            {/* Permissions card */}
            <TutorialCard
                title={'Permissions'}
                text={'Allows administrators to control access to web dashboard. A sign-up code can be provided to additional researchers to view survey data.'}
                Icon={SupervisorAccountIcon} />

            {/* Survey Wizard card */}
            <TutorialCard
                title={'Survey Wizard'}
                text={'A drag-and-drop survey building interface. Drag field types from the right to central survey stack to add questions to the survey. Question can be repositioned after initial drop. Click “edit” to change the prompt question and the field’s column name in answers database table.'}
                Icon={BallotIcon}
                reversed={true} />            

            {/* Prompts card */}
            <TutorialCard
                title={'Prompts'}
                text={'A drag-and-drop survey building interface for asking end-of-trip questions to users.'}
                Icon={FlagIcon} />            

            {/* Metrics card */}
            <TutorialCard
                title={'Metrics'}
                text={'Provides dashboard overview charts and metrics for the ongoing survey.'}
                Icon={BarChartIcon}
                reversed={true} />            

            {/* Survey map card */}
            <TutorialCard
                title={'Participants'}
                text={'A searchable table view of mobile users and their survey responses. Clicking on a user within the table will select their UUID within the GPS Points view.'}
                Icon={PublicIcon} />            

            {/* Participants card */}
            <TutorialCard
                title={'Data Management'}
                text={'Provides downloads for survey data, uploading of subway stations and resetting of an active survey.'}
                Icon={FaceIcon}
                reversed={true} />            

            {/* GPS Points card */}
            <TutorialCard
                title={'Permissions'}
                text={'Allows administrators to control access to web dashboard. A sign-up code can be provided to additional researchers to view survey data.'}
                Icon={GpsFixedIcon} />            

            {/* Data Management card */}
            <TutorialCard
                title={'Permissions'}
                text={'Allows administrators to control access to web dashboard. A sign-up code can be provided to additional researchers to view survey data.'}
                Icon={CloudDownloadIcon}
                reversed={true} />            
        </Container>
    );
}

export default TutorialPanel;
