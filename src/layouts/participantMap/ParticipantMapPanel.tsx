/* 
 * src/layouts/participantMap/participantMapPanel.tsx
 */
import Grid from '@material-ui/core/Grid';

import ParticipantMap from '../../views/participantMap/ParticipantMap';
import ParticipantMapTimeControl from '../../views/participantMap/ParticipantMapTimeControl';


const ParticipantMapPanel = () => {
    return (
        <Grid container direction='column'>
            <Grid item sm={12}>
                <ParticipantMapTimeControl />
            </Grid>
            <Grid item sm={12}>
                <ParticipantMap />
            </Grid>
        </Grid>
    );
};

export default ParticipantMapPanel;
