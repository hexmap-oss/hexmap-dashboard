/* 
 * src/layouts/participantUsers/ParticipantUsersPanel.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import ParticipantUsersTable from '../../views/participantTable/ParticipantTable';
import ParticipantUsersSearchbar from '../../views/participantTable/ParticipantTableSearchbar';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '90%',
            margin: 'auto',
            paddingTop: theme.spacing(5)
        },
        title: {
            paddingLeft: theme.spacing(4),
            paddingTop: theme.spacing(2),
        }
    })
);

const ParticipantUsersPanel = () => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.title} elevation={0}>
                        <Typography variant='h5'><b>Mobile Users</b></Typography>
                    </Paper>
                </Grid>

                <Grid item xs={12}>
                    <ParticipantUsersSearchbar />
                </Grid>

                <Grid item xs={12}>
                    <ParticipantUsersTable />
                </Grid>
            </Grid>

        </Box>
    );
};

export default ParticipantUsersPanel;
