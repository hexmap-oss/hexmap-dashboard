/* 
 * src/layouts/dataManagement/DataManagementPanel.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import DataDownloader from '../../views/dataManagement/DataDownloader';
import UploadSubwayStations from '../../views/dataManagement/UploadSubwayStations';


const useStyles = makeStyles((theme: Theme) => {
    return createStyles({
        root: {
            paddingTop: theme.spacing(2),
            paddingLeft: theme.spacing(4),
            paddingRight: theme.spacing(4),
        }
    })
}
);

const DataManagementPanel = () => {
    const classes = useStyles();

    return (
        <Grid className={classes.root} container spacing={3}>
            <Grid item sm={12}>
                <DataDownloader />
            </Grid>

            <Grid item sm={12}>
                <UploadSubwayStations />
            </Grid>            
        </Grid>
    );
};

export default DataManagementPanel;
