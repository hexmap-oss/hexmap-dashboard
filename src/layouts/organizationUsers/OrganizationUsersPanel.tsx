/* 
 * src/layouts/organizationUsers/OrganizationUsersPanel.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import OrganizationUserInviteCard from '../../views/organizationUsers/OrganizationUsersInviteCard';
import OrganizationUserPermissionsCard from '../../views/organizationUsers/OrganizationUsersPermissionsCard';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '90%',
            margin: 'auto',
            paddingTop: theme.spacing(4),
        }
    })
);


const OrganizationUsersPanel = () => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <OrganizationUserInviteCard />
                </Grid>

                <Grid item xs={12}>
                    <OrganizationUserPermissionsCard />
                </Grid>            
            </Grid>
        </Box>
    );
};

export default OrganizationUsersPanel;