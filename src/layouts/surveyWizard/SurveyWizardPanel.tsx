/* 
 * src/views/surveyWizard/SurveyWizardPanel.tsx
 */
import Grid from '@material-ui/core/Grid';
import SurveyWizardForm from '../../views/surveyWizard/SurveyWizardForm';
import SurveyWizardHeader from '../../views/surveyWizard/SurveyWizardHeader';

const SurveyWizardPanel = () => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <SurveyWizardHeader />
            </Grid>
            <Grid item xs={12}>
                <SurveyWizardForm />
            </Grid>
        </Grid>
    );
}

export default SurveyWizardPanel;
