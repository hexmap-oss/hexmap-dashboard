/* 
 * src/layouts/surveySettings/SurveySettingsPanel.tsx
 */
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import PromptSettings from '../../views/surveySettings/PromptSettings';
import SurveySettings from '../../views/surveySettings/SurveySettings';
import TripbreakerSettings from '../../views/surveySettings/TripbreakerSettings';
import ResetActiveSurvey from '../../views/surveySettings/ResetActiveSurvey';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingTop: theme.spacing(2),
            paddingLeft: theme.spacing(4),
            paddingRight: theme.spacing(4),
        }
    })
);

const SurveySettingsPanel = () => {
    const classes = useStyles();

    return (
        <Grid className={classes.root} container>
            <Grid item sm={12}>
                <SurveySettings />
            </Grid>

            <Grid item sm={12}>
                <PromptSettings />
            </Grid>

            <Grid item sm={12}>
                <TripbreakerSettings />
            </Grid>

            <Grid item sm={12}>
                <ResetActiveSurvey />
            </Grid>            
        </Grid>
    );
};

export default SurveySettingsPanel;
