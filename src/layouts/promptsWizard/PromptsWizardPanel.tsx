/* 
 * src/layouts/promptsWizard/PromptsWizardPanel.tsx
 */
import Grid from '@material-ui/core/Grid';
import PromptsWizardForm from '../../views/promptsWizard/PromptsWizardForm';
import PromptsWizardHeader from '../../views/promptsWizard/PromptsWizardHeader';


const PromptsWizardPanel = () => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <PromptsWizardHeader />
            </Grid>
            <Grid item xs={12}>
                <PromptsWizardForm />
            </Grid>
        </Grid>
    );
};

export default PromptsWizardPanel;
