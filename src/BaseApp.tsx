/* 
 * src/BaseApp.tsx
 */
import { useEffect } from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import AuthenticatedApp from './AuthenticatedApp';
import LoginPanel from './layouts/login/LoginPanel';
import LoggingInJWT from './layouts/login/LoggingInJWT';
import { 
    initJWT,
    selectIsLoadingJWT
} from './views/login/loginSlice';


export default function BaseApp() {
    const isLoadingJWT = useSelector(selectIsLoadingJWT);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(initJWT());
    }, [dispatch]);

    if (isLoadingJWT) {
        return <LoggingInJWT />
    }

    return (
        <Router>
            <Switch>
                <Route path={'/login'} component={LoginPanel} />
                <Route path={'/'} component={AuthenticatedApp} />
            </Switch>
        </Router>
    );
};